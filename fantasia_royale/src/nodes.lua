local S = minetest.get_translator("fantasia_royale")
local modname = "fantasia_royale"


-- given here is a killer airlike node that kills players on contact

-- of course, if you want to keep these useful nodes for your minigame, change the mod name



minetest.register_node(modname .. ":killer",{
        description = "Storm Minigame Killer",
        inventory_image = "no_texture_airlike.png",
        drawtype = "airlike",
        paramtype = "light",
        sunlight_propagates = true,
        walkable     = false,
        pointable    = true,
        diggable     = false,
        buildable_to = false,
        drop = "",
        damage_per_second = 2000,
        groups = {},
})



minetest.register_node(modname .. ":class_selector", {
	drawtype = "mesh",
	mesh = "froyale_class_selector.obj",
	tiles = {"froyale_class_selector.png"},
	visual_scale = 1.4,
	selection_box = {
		type = "fixed",
		fixed  = {-0.3, -0.5, -0.3, 0.3, 0.12, 0.3}
	},
	use_texture_alpha = "clip",
	groups = {crumbly=1, soil=1, oddly_breakable_by_hand=1},
	paramtype2 = "facedir",
	description = S("Fantasia Royale Class Selector"),
	on_punch = function (pos, node, puncher)
		fantasia.GUI_show_class_selector(puncher:get_player_name())
	end,
	on_rightclick = function (pos, node, clicker)
		fantasia.GUI_show_class_selector(clicker:get_player_name())
	end
})


local particle_colors = {
    ["green"] = "#71aa34",
    ["gold"] = "#f47e1b",
    ["purple"] = "#564064"
}

for _, dropbox_color in ipairs({"purple", "gold", "green"}) do

	minetest.register_node(modname .. ":dropbox_" .. dropbox_color, {
		description = "Storm Minigame Dropbox",
		tiles = {
			"froyale_dropbox_top_"..dropbox_color..".png",    -- top
			"froyale_dropbox_bottom.png",  -- bottom
			"froyale_dropbox_side_"..dropbox_color..".png",    -- side
		},
		groups = {cracky = 3, stone = 1},
		on_construct = function(pos)
			-- Initialize the node timer
			minetest.get_node_timer(pos):start(1.0)  -- Adjust the interval as needed
		end,
		drop = "",
		on_dig = function(pos, node, digger)
			local arena = arena_lib.get_arena_by_pos(pos, "fantasia_royale")
			if not arena then minetest.remove_node(pos) return end
			if not arena.in_game then minetest.remove_node(pos) return end
			if arena.in_celebration then minetest.remove_node(pos) return end

			minetest.remove_node(pos)

			local dropboxes = arena.permanent_dropboxes
			local key = minetest.pos_to_string(pos,0)
			if dropboxes[key] then
				local treasure = dropboxes[key].treasure

				-- add the treasure as an item in the world. Yes, theft is possible.
				local obj = minetest.add_item({x = pos.x, y = pos.y + 1, z = pos.z}, treasure)

				if obj then
					obj:add_velocity({x = 0, y = 1, z = 0})
				end

				arena.permanent_dropboxes[key] = nil
			end
		end,
		on_timer = function(pos, elapsed)
			-- Spawn particles
			minetest.add_particlespawner({
				amount = 10,
				time = 1,  -- Emission duration
				minpos = {x = pos.x - 0.5, y = pos.y - 0.5, z = pos.z - 0.5},
				maxpos = {x = pos.x + 0.5, y = pos.y + 0.5, z = pos.z + 0.5},
				minvel = {x = -1, y = -1, z = -1},
				maxvel = {x = 1, y = 1, z = 1},
				minacc = {x = 0, y = 0, z = 0},
				maxacc = {x = 0, y = 0, z = 0},
				minexptime = 1,
				maxexptime = 2,
				minsize = 0.5,
				maxsize = 1.0,
				texture = "froyale_dropbox_effect_particle.png^[colorize:" .. particle_colors[dropbox_color],
				glow = 10,
			})
			-- Restart the timer
			return true  -- Restart the timer
		end,
	})
end