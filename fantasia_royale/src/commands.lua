local S = minetest.get_translator("fantasia_royale")

local function get_valid_arena() end
local function get_looking_node_pos() end
local function from_chest_to_string() end
local function from_treasure_to_string() end
local function get_wielded_item() end



ChatCmdBuilder.new("froyale", function(cmd)
	cmd:sub("unittests", function(sender)
		if arena_lib.is_player_in_arena(sender) then
			fantasia.print_error(sender, S("Can't run unit tests when queuing or playing!"))
			return
		end

		if not fantasia.settings.are_unit_tests_over then
			fantasia.print_error(sender, S("Unit tests are already running, wait for them to finish!"))
			return
		end

		fantasia.unittest_HUD_log(minetest.get_player_by_name(sender))
	end)

	--------------------
	-- ! CHEST CMDS ! --
	--------------------

	cmd:sub("addtreasure :arena :treasure :count:int :preciousness:int :rarity:number",
	function(sender, arena_name, treasure_name, count, preciousness, rarity)
		local arena, arena_name = get_valid_arena(arena_name, sender, true)

		if not arena then
			return
		elseif count < 1 then
			fantasia.print_error(sender, S("Count has to be greater than 0!"))
			return
		elseif rarity < 1 or rarity > 10 then
			fantasia.print_error(sender, S("Rarity has to be between 1 and 10!"))
			return
		elseif ItemStack(treasure_name):is_known() == false then
			fantasia.print_error(sender, S("@1 doesn't exist!", treasure_name))
			return
		end

		local item_id = fantasia.generate_treasure_id(arena)
		local treasure = {
			name = treasure_name,
			rarity = rarity,
			count = count,
			preciousness = preciousness,
			id = item_id
		}
		table.insert(arena.treasures, treasure)

		fantasia.print_msg(sender, "+ " .. from_treasure_to_string(treasure))
		fantasia.reorder_treasures(arena)
	end)


	cmd:sub("addtreasure hand :arena :rarity:number :preciousness:int",
	function(sender, arena_name, preciousness, rarity)
		local arena, arena_name = get_valid_arena(arena_name, sender, true)
		local wielded_itemstack = get_wielded_item(sender)
		local treasure = {}

		if not arena or not wielded_itemstack then
			return
		elseif rarity < 1 or rarity > 10 then
			fantasia.print_error(sender, S("Rarity has to be between 1 and 10!"))
			return
		end

		local item_id = fantasia.generate_treasure_id(arena)
		treasure = {
			name = wielded_itemstack.name,
			rarity = rarity,
			count = wielded_itemstack.count,
			preciousness = preciousness,
			id = item_id
		}
		table.insert(arena.treasures, treasure)

		fantasia.print_msg(sender, "+ " .. from_treasure_to_string(treasure))
		fantasia.reorder_treasures(arena)
	end)


	cmd:sub("removetreasure hand :arena",
	function(sender, arena_name)
		local arena, arena_name = get_valid_arena(arena_name, sender, true)
		local found = {true, false} -- the first is used to repeat the for until nothing is found
		local wielded_itemstack = get_wielded_item(sender)

		if not arena or not wielded_itemstack then
			return
		end

		-- Removing all the treasures with that name.
		while found[1] do
			found[1] = false
			for i, treasure in pairs(arena.treasures) do
					if treasure.name == wielded_itemstack.name then
						table.remove(arena.treasures, i)
						found[1] = true
						found[2] = true
					end
			end
		end

		if found[2] then fantasia.print_msg(sender, S("@1 removed from @2!", wielded_itemstack.name, arena.name))
		else fantasia.print_error(sender, S("Treasure not found!")) end

		fantasia.reorder_treasures(arena)
	end)


	cmd:sub("removetreasure :arena :treasure",
	function(sender, arena_name, treasure_name)
		local arena, arena_name = get_valid_arena(arena_name, sender, true)
		local found = {true, false} -- the first is used to repeat the for until nothing is found

		if not arena then
			return
		end

		-- Removing all the treasures with that name.
		while found[1] do
			found[1] = false
			for i, treasure in pairs(arena.treasures) do
				if treasure.name == treasure_name then
					table.remove(arena.treasures, i)
					i = i-1
					found[1] = true
					found[2] = true
				end
			end
		end

		if found[2] then fantasia.print_msg(sender, S("@1 removed from @2!", treasure_name, arena_name))
		else fantasia.print_error(sender, S("Treasure not found!")) end

		fantasia.reorder_treasures(arena)
	end)


	cmd:sub("removetreasure id :arena :id:int",
	function(sender, arena_name, treasure_id)
		local arena, arena_name = get_valid_arena(arena_name, sender, true)
		local treasure_name = ""

		if not arena then
			return
		end

		for i=1, #arena.treasures do
			if arena.treasures[i].id == treasure_id then
				treasure_name = arena.treasures[i].name
				table.remove(arena.treasures, i)
				break
			end
		end

		if treasure_name ~= "" then fantasia.print_msg(sender, S("@1 removed from @2!", treasure_name, arena_name))
		else fantasia.print_error(sender, S("Treasure not found!")) end

		fantasia.reorder_treasures(arena)
	end)


	cmd:sub("copytreasures :fromarena :toarena",
	function(sender, from_name, to_name)
		local from_arena, from_name = get_valid_arena(from_name, sender)
		local to_arena, to_name = get_valid_arena(to_name, sender, true)

		if not to_arena or not from_arena then
			return
		elseif from_arena == to_arena then
			fantasia.print_error(sender, S("The arenas must be different!"))
			return
		end

		to_arena.treasures = table.copy(from_arena.treasures)

		fantasia.print_msg(sender, S("@1 treasures have been copied to @2!", from_name, to_name))
	end)


	cmd:sub("gettreasures :arena",
	function(sender, arena_name)
		local arena = get_valid_arena(arena_name, sender)

		if not arena then
			return
		end

		fantasia.print_msg(sender, S("Treasures list:"))
		for i=1, #arena.treasures do
			fantasia.print_msg(sender, from_treasure_to_string(arena.treasures[i]) .. "\n\n")
		end
	end)


	cmd:sub("searchtreasure :arena :treasure",
	function(sender, arena_name, treasure_name)
		local arena, arena_name = get_valid_arena(arena_name, sender)

		if not arena then
			return
		end

		fantasia.print_msg(sender, S("Treasures list:"))
		for i=1, #arena.treasures do
			local treasure = arena.treasures[i]
			if treasure.name:find(treasure_name) then
				fantasia.print_msg(sender, from_treasure_to_string(treasure)  .. "\n\n")
			end
		end
	end)


	cmd:sub("filter treasures rarity :arena :rarity:number",
	function(sender, arena_name, rarity)
		local arena, arena_name = get_valid_arena(arena_name, sender)

		if not arena then
			return
		end

		fantasia.print_msg(sender, S("Treasures list:"))
		for i=1, #arena.treasures do
			local treasure = arena.treasures[i]
			if treasure.rarity == rarity then
				fantasia.print_msg(sender, from_treasure_to_string(treasure)  .. "\n\n")
			end
		end
	end)


	cmd:sub("filter treasures preciousness :arena :preciousness:number",
	function(sender, arena_name, preciousness)
		local arena, arena_name = get_valid_arena(arena_name, sender)

		if not arena then
			return
		end

		fantasia.print_msg(sender, S("Treasures list:"))
		for i=1, #arena.treasures do
			local treasure = arena.treasures[i]
			if treasure.preciousness == preciousness then
				fantasia.print_msg(sender, from_treasure_to_string(treasure)  .. "\n\n")
			end
		end
	end)


	cmd:sub("filter treasures preciousness+rarity :arena :preciousness:number :rarity:number",
	function(sender, arena_name, preciousness, rarity)
		local arena, _ = get_valid_arena(arena_name, sender)

		if not arena then
			return
		end

		fantasia.print_msg(sender, S("Treasures list:"))
		for i=1, #arena.treasures do
			local treasure = arena.treasures[i]
			if treasure.preciousness == preciousness and treasure.rarity == rarity then
				fantasia.print_msg(sender, from_treasure_to_string(treasure)  .. "\n\n")
			end
		end
	end)


	cmd:sub("addchest :minpreciousness:int :maxpreciousness:int :tmin:int :tmax:int",
	function(sender, min_preciousness, max_preciousness, min_treasures, max_treasures)
		local arena, _ = get_valid_arena("@", sender, true)
		local pos = get_looking_node_pos(sender)
		local exists = false

		if not pos then
			return
		elseif not arena then
			return
		end

		if min_treasures <= 0 or max_treasures <= 0 then
			fantasia.print_error(sender, S("The minimum or maximum amount of treasures has to be greater than 0!"))
			return
		end

		local chest_id = fantasia.generate_chest_id(arena)
		local chest =
		{
			pos = pos,
			min_preciousness = min_preciousness,
			max_preciousness = max_preciousness,
			min_treasures = min_treasures,
			max_treasures = max_treasures,
			id = chest_id
		}

		for i=1, #arena.chests do
			if vector.equals(arena.chests[i].pos, pos) then
				exists = true
				break
			end
		end

		if exists then
			fantasia.print_error(sender, S("The chest already exists!"))
			return
		end
		fantasia.print_msg(sender, "+ " .. from_chest_to_string(chest))

		table.insert(arena.chests, chest)
	end)


	cmd:sub("getchests :arena",
	function(sender, arena_name)
		local arena = get_valid_arena(arena_name, sender)

		if not arena then
			return
		end

		fantasia.print_msg(sender, S("Chest list:"))
		for i=1, #arena.chests do
			fantasia.print_msg(sender, from_chest_to_string(arena.chests[i]) .. "\n\n")
		end
	end)


	cmd:sub("removechest",
	function(sender)
		local arena, _ = get_valid_arena("@", sender, true)
		local found = false
		local pos = get_looking_node_pos(sender)

		if not pos then
			return
		elseif not arena then
			return
		end

		for i=1, #arena.chests do
			if vector.equals(arena.chests[i].pos, pos) then
				table.remove(arena.chests, i)
				found = true
				break
			end
		end

		if found then
			fantasia.print_msg(sender, S("Chest removed!"))
		else
			fantasia.print_error(sender, S("Chest not found!"))
		end
	end)


	cmd:sub("inspect",
	function(sender)
		local arena, _ = get_valid_arena("@", sender)
		local found = false
		local pos = get_looking_node_pos(sender)

		if not pos then
			return
		elseif not arena then
			return
		end

		for i=1, #arena.chests do
			local chest = arena.chests[i]
			if vector.equals(chest.pos, pos) then
				fantasia.print_msg(sender, from_chest_to_string(chest) .. "\n\n")
				found = true
				break
			end
		end

		if not found then
			fantasia.print_error(sender, S("Chest not found!"))
		end
	end)



	cmd:sub("removechest id :arena :id:int",
	function(sender, arena_name, chest_id)
		local arena, _ = get_valid_arena(arena_name, sender, true)
		local found = false

		if not arena then
			return
		end

		for i=1, #arena.chests do
			if arena.chests[i].id == chest_id then
				table.remove(arena.chests, i)
				found = true
				break
			end
		end

		if found then
			fantasia.print_msg(sender, S("Chest removed!"))
		else
			fantasia.print_error(sender, S("Chest not found!"))
		end
	end)


end, {

	description = [[

		ADMIN COMMANDS
		(Use /help storm to read it all)


		TREASURES:

		- addtreasure <arena name> <item> <count> <preciousness>
			<rarity (min 1.0, max 10.0)>
		- addtreasure hand <arena name> <preciousness>
			<rarity (min 1.0, max 10.0)>
		- gettreasures <arena name>
		- searchtreasure <arena name> <treasure name>: shows all the treasures with that name
		- filter treasures rarity <arena name> <rarity>
		- filter treasures preciousness <arena name> <preciousness>
		- filter treasures preciousness+rarity <arena name> <preciousness> <rarity>
		- removetreasure <arena name> <treasure name>: remove all treasures with that name
		- removetreasure hand <arena name>
		- removetreasure id <arena name> <treasure id>
		- copytreasures <(from) arena name> <(to) arena name>


		CHESTS:

		- addchest <min_preciousness> <max_preciousness> <min_treasures_amount (min. 1)>
			<max_treasures_amount>
		- getchests <arena name>
		- inspect: gives you information about the chest you're looking at
		- removechest
		- removechest id <arena name> <id>

		UNIT TESTS:

		- unittests: runs unit tests on the sender. Can't be in a game

		]],
	privs = { froyale_admin = true }
})



minetest.register_privilege("froyale_admin", {
	description = "With this you can use /froyale"
})





----------------------------------------------
---------------LOCAL FUNCTIONS----------------
----------------------------------------------

function get_valid_arena(arena_name, sender, property_is_changing)
	local arena = nil

	if arena_name == "@" then
		local player_pos = minetest.get_player_by_name(sender):get_pos()
		arena = arena_lib.get_arena_by_pos(player_pos, "fantasia_royale")
		if arena then arena_name = arena.name end
	else
		local _, arena_ = arena_lib.get_arena_by_name("fantasia_royale", arena_name)
		arena = arena_
	end

	if not arena then
		fantasia.print_error(sender, S("@1 doesn't exist!", arena_name))
		return nil
	elseif arena.enabled and property_is_changing then
		arena_lib.disable_arena(sender, "fantasia_royale", arena_name)
		local couldnt_disable = arena.enabled

		if couldnt_disable then
			fantasia.print_error(sender, S("@1 must be disabled!", arena_name))
			return nil
		end
	end

	return arena, arena_name
end



function get_looking_node_pos(pl_name)
	local player = minetest.get_player_by_name(pl_name)
	local look_dir = player:get_look_dir()
	local pos_head = vector.add(player:get_pos(), {x=0, y=1.5, z=0})
	local result, pos = minetest.line_of_sight(
		vector.add(pos_head, vector.divide(look_dir, 4)),
		vector.add(pos_head, vector.multiply(look_dir, 10))
	)

	if result then
		fantasia.print_error(pl_name, S("You're not looking at anything!"))
		return nil
	end

	return pos
end



function from_chest_to_string(chest)
	local chest_pos = minetest.pos_to_string(chest.pos, 0)
	return S(
		"ID: @1, position: @2, preciousness: @3-@4, treasures amount: @5-@6",
		chest.id, chest_pos, chest.min_preciousness, chest.max_preciousness,
		chest.min_treasures, chest.max_treasures
	)
end



function from_treasure_to_string(treasure)
	return S(
		"ID: @1, name: @2, rarity: @3, preciousness: @4, count: @5",
		treasure.id, treasure.name, treasure.rarity, treasure.preciousness, treasure.count
	)
end



function get_wielded_item(player)
	local item_name = minetest.get_player_by_name(player):get_wielded_item():get_name()
	local item_count = minetest.get_player_by_name(player):get_wielded_item():get_count()
	local itemstack = {}

	if ItemStack(item_name):is_known() == false then
		fantasia.print_error(player, S("@1 doesn't exist!", item_name))
		return nil
	elseif item_name == "" then
		fantasia.print_error(player, S("Your hand is empty!"))
		return nil
	end

	itemstack.name = item_name
	itemstack.count = item_count

	return itemstack
end
