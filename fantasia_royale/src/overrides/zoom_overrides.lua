fantasia.saved_fovs = {}

arena_lib.register_on_load(function(mod, arena)
    if mod == "fantasia_royale" then
        for pl_name, stats in pairs(arena.players) do
            local player = minetest.get_player_by_name(pl_name)
            if player then
                local props = player:get_properties()
                fantasia.saved_fovs[pl_name] = props.zoom_fov or 0
                player:set_properties({zoom_fov = 0})
            end
        end
    end
end)

arena_lib.register_on_quit(function(mod, arena, p_name, is_spectator, reason)
    if mod == "fantasia_royale" then
        local player = minetest.get_player_by_name(p_name)
        if player then
            player:set_properties({zoom_fov = fantasia.saved_fovs[p_name] or 0})
        end
    end
end)

if minetest.get_modpath("binoculars") then
    minetest.debug("binoculars overrides loaded")
    local old = binoculars.update_player_property
    function binoculars.update_player_property(player)
        local p_name = player:get_player_name()
        if arena_lib.is_player_in_arena(p_name, "fantasia_royale") then return end
        old(player)
    end
end

