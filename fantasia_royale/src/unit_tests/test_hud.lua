fantasia.settings.are_unit_tests_over = true -- to expand if more are added in the future



function fantasia.unittest_HUD_log(player)
    fantasia.settings.are_unit_tests_over = false

    local dummy_arena = {players = {}}
    local p_name = player:get_player_name()

    fantasia.HUD_log_create(p_name)
    dummy_arena.players[p_name] = true

    minetest.log("action", "[FANTASIA ROYALE] Unit test 'Log Panel': started")
    minetest.chat_send_player(p_name, "Log panel: created")

    fantasia.HUD_log_update(dummy_arena, "froyale_log_death.png", "Ananas", "Fra@@1@@@@@@")
    fantasia.HUD_log_update(dummy_arena, "froyale_log_death.png", "Tu", "i")

    minetest.after(1, function()
        fantasia.HUD_log_update(dummy_arena, "froyale_log_death.png", nil, "_@_@_XXXX")

        minetest.after(1, function()
            fantasia.HUD_log_update(dummy_arena, "froyale_log_death.png", "Somethingfoo Bar", "BRIE")

            minetest.after(3, function()
                minetest.chat_send_player(p_name, "Log panel: emptied")
                fantasia.HUD_log_clear(dummy_arena)

                minetest.after(1, function()
                    minetest.chat_send_player(p_name, "Log panel: removed")
                    panel_lib.get_panel(p_name, "froyale_log"):remove()

                    minetest.log("action", "[FANTASIA ROYALE] Unit test 'Log Panel': completed")
                    fantasia.settings.are_unit_tests_over = true
                end)
            end)
        end)
    end)
end