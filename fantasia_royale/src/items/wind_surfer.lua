local function dash() end

local JUMP_HEIGHT = 16
local JUMP_DELAY = 5
local DASH_FACTOR = 22
local DASH_DELAY = 3



minetest.register_tool("fantasia_royale:wind_surfer", {
    description = "Surf the wind!",
    inventory_image = "froyale_item_windsurfer_icon.png",
    groups = {oddly_breakable_by_hand = "2"},
    on_drop = function() end,

    on_use = function(itemstack, user, pointed_thing)
        local p_meta = user:get_meta()

        if p_meta:get_int("fr_surfer_jump") == 1 then return end

        user:add_velocity(vector.new(0, JUMP_HEIGHT, 0))
        --minetest.sound_play("TODO", user:get_player_by_name())
        p_meta:set_int("fr_surfer_jump", 1)

        local p_name = user:get_player_name()
        local i_meta = itemstack:get_meta()

        if p_meta:get_int("fr_surfer_dash") == 1 then
            i_meta:set_string("inventory_image", "froyale_item_windsurfer_icon_noboth.png")
        else
            i_meta:set_string("inventory_image", "froyale_item_windsurfer_icon_nojump.png")
        end

        minetest.after(JUMP_DELAY, function()
            if not minetest.get_player_by_name(p_name) then return end
            p_meta:set_int("fr_surfer_jump", 0)

            if not arena_lib.is_player_playing(p_name, "fantasia_royale") then return end

            if p_meta:get_int("fr_surfer_dash") == 1 then
                i_meta:set_string("inventory_image", "froyale_item_windsurfer_icon_nodash.png")
            else
                i_meta:set_string("inventory_image", "froyale_item_windsurfer_icon.png")
            end

            user:get_inventory():set_stack("main", 1, itemstack)
        end)

        return itemstack
    end,

    on_secondary_use = function(itemstack, user, pointed_thing)
      return dash(user, itemstack)
    end,

    on_place = function(itemstack, user, pointed_thing)
      return dash(user, itemstack)
    end
})



----------------------------------------------
---------------LOCAL FUNCTIONS----------------
----------------------------------------------

function dash(player, itemstack)
    local p_meta = player:get_meta()

    if p_meta:get_int("fr_surfer_dash") == 1 then return end

    player:add_velocity(vector.multiply(player:get_look_dir(), DASH_FACTOR))
    --minetest.sound_play("TODO", player:get_player_by_name())
    p_meta:set_int("fr_surfer_dash", 1)

    local p_name = player:get_player_name()
    local i_meta = itemstack:get_meta()

    if p_meta:get_int("fr_surfer_jump") == 1 then
        i_meta:set_string("inventory_image", "froyale_item_windsurfer_icon_noboth.png")
    else
        i_meta:set_string("inventory_image", "froyale_item_windsurfer_icon_nodash.png")
    end

    minetest.after(DASH_DELAY, function()
        if not minetest.get_player_by_name(p_name) then return end
        p_meta:set_int("fr_surfer_dash", 0)

        if not arena_lib.is_player_playing(p_name, "fantasia_royale") then return end

        if p_meta:get_int("fr_surfer_jump") == 1 then
            i_meta:set_string("inventory_image", "froyale_item_windsurfer_icon_nodash.png")
        else
            i_meta:set_string("inventory_image", "froyale_item_windsurfer_icon.png")
        end

        player:get_inventory():set_stack("main", 1, itemstack)
    end)

    return itemstack
end