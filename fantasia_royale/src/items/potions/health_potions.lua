local S = minetest.get_translator("fantasia_royale")



minetest.register_craftitem("fantasia_royale:health_potion_small", {
	description = S("Heals @1 hearts", 4),
	inventory_image = "froyale_potion_health_small.png",
	sound = {
		eat = {name = "froyale_drinking_potion"}
	},
	on_use = minetest.item_eat(2*4),
})


minetest.register_craftitem("fantasia_royale:health_potion_medium", {
	description = S("Heals @1 hearts", 6),
	inventory_image = "froyale_potion_health_medium.png",
	sound = {
		eat = {name = "froyale_drinking_potion"}
	},
	on_use = minetest.item_eat(2*6),
})


minetest.register_craftitem("fantasia_royale:health_potion_large", {
	description = S("Heals @1 hearts", 10),
	inventory_image = "froyale_potion_health_large.png",
	sound = {
		eat = {name = "froyale_drinking_potion"}
	},
	on_use = minetest.item_eat(2*10),
})