local S = minetest.get_translator("fantasia_royale")



--[[
	name: vial name, without mod prefix,
	def: {
		texture = "txt_name.png",
		recharged_mana_bars = 2,
		recharge_sound = "gluglu_1"
	}
]]
function fantasia.register_mana_vial(name, def)
	minetest.register_craftitem("fantasia_royale:mana_vial_"..name, {
		description = S("Recharges @1 mana bars", def.recharged_mana_bars),
		inventory_image = def.texture or ("froyale_mana_vial_%s.png"):format(name),
		_recharged_mana_bars = def.recharged_mana_bars,
		_recharge_sound = def.recharge_sound,
		_is_vial = true,

		on_pickup = function(itemstack, picker, pointed_thing, time_from_last_punch)
			picker:get_player_name():add_mana_reserve(def.recharged_mana_bars * fantasia.MAX_MANA * itemstack:get_count())
			minetest.sound_play({name = def.recharge_sound or "froyale_drinking_potion"}, {to_player = picker:get_player_name()}, true)
			itemstack:set_count(0)
			return itemstack
		end
	})
end



minetest.register_on_player_inventory_action(function(player, action, inventory, inv_info)
	local pl_name = player:get_player_name()

	if not arena_lib.is_player_in_arena(pl_name, "fantasia_royale") then
		return
	end

	if action == "put" then
		local vial_def = minetest.registered_craftitems[inv_info.stack:get_name()]
		if vial_def and vial_def._is_vial then
			inventory:remove_item(inv_info.listname, inv_info.stack)
			pl_name:add_mana_reserve(vial_def._recharged_mana_bars * fantasia.MAX_MANA * inv_info.stack:get_count())
			minetest.sound_play({name = vial_def.recharge_sound or "froyale_drinking_potion"}, {to_player = pl_name}, true)
		end
	end
end)