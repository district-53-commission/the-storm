-- default texture: froyale_mana_vial_<name>.png (e.g. "froyale_mana_vial_minor.png")
-- default recharge_sound: froyale_drinking_potion


fantasia.register_mana_vial("small", {
	recharged_mana_bars = 2,
})

fantasia.register_mana_vial("medium", {
	recharged_mana_bars = 4,
})

fantasia.register_mana_vial("large", {
	recharged_mana_bars = 6,
})