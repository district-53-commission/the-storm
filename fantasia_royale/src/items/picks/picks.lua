
-- digging times: hardest, medium, softest
local lvl1_times = {6,3,2}
local lvl2_times = {2.5,2,1}
local lvl3_times = {.75,.5,.25}

local lvl1crackytimes = {lvl1_times[1]*1.5,lvl1_times[2]*1.5,lvl1_times[3]*1.5}
local lvl2crackytimes = {lvl2_times[1]*1.5,lvl2_times[2]*1.5,lvl2_times[3]*1.5}
local lvl3crackytimes = {lvl3_times[1]*1.5,lvl3_times[2]*1.5,lvl3_times[3]*1.5}

minetest.register_craftitem("fantasia_royale:pickaxe_1",{
    stack_max = 1,
    description = "Magic Pickaxe\nLvl 1",
    inventory_image = "froyale_pick_lvl1_icon.png",
    wield_image = "froyale_pick_lvl1_icon.png",
    is_froyale_pick = true,
    pick_lvl = 1,
    tool_capabilities = {
        full_punch_interval = 1.0,
        max_drop_level = 0,
        groupcaps = {
            crumbly = {times = lvl1_times, maxlevel = 3},
            cracky = {times = lvl1crackytimes, maxlevel = 3},
            snappy = {times = {.3,.3,.3}, maxlevel = 3},
            choppy = {times = lvl1_times, maxlevel = 3},
        },
    },
    groups = {froyale_pickaxe = 1},
    on_drop = function(itemstack) return itemstack end,
})

minetest.register_craftitem("fantasia_royale:pickaxe_2",{
    stack_max = 1,
    description = "Magic Pickaxe\nLvl 2",
    inventory_image = "froyale_pick_lvl2_icon.png",
    wield_image = "froyale_pick_lvl2_icon.png",
    is_froyale_pick = true,
    pick_lvl = 2,
    tool_capabilities = {
        full_punch_interval = 1.0,
        max_drop_level = 0,
        groupcaps = {
            crumbly = {times = lvl2_times, maxlevel = 3},
            cracky = {times = lvl2crackytimes, maxlevel = 3},
            snappy = {times = {.2,.2,.2}, maxlevel = 3},
            choppy = {times = lvl2_times, maxlevel = 3},
        },
    },
    groups = {froyale_pickaxe = 2},
    on_drop = function(itemstack) return itemstack end,
})


minetest.register_craftitem("fantasia_royale:pickaxe_3",{
    stack_max = 1,
    description = "Magic Pickaxe\nLvl 3",
    inventory_image = "froyale_pick_lvl3_icon.png",
    wield_image = "froyale_pick_lvl3_icon.png",
    is_froyale_pick = true,
    pick_lvl = 3,
    tool_capabilities = {
        full_punch_interval = 1.0,
        max_drop_level = 0,
        groupcaps = {
            crumbly = {times = lvl3_times, maxlevel = 3},
            cracky = {times = lvl3crackytimes, maxlevel = 3},
            snappy = {times = {.1,.1,.1}, maxlevel = 3},
            choppy = {times = lvl3_times, maxlevel = 3},
        },
    },
    groups = {froyale_pickaxe = 3},
    on_drop = function(itemstack) return itemstack end,
})
