local minimap_huds = {}
-- local test_huds = {}
local get_minimap_texture = function(p_name, player, arena) end
local append_table_to_table = function(t1,t2) end


local mini_map_size = 350*.75
local mini_map_top_right_corner_offset = 10
local half_minimap_size = math.floor(mini_map_size/2)
local center_offset = half_minimap_size + mini_map_top_right_corner_offset

fantasia.minimap_screenspace = mini_map_top_right_corner_offset + mini_map_size + mini_map_top_right_corner_offset

function fantasia.HUD_minimap_create(p_name, arena)
    local player = minetest.get_player_by_name(p_name)

    if not player then return end
    if minimap_huds[p_name] then fantasia.HUD_minimap_remove(p_name) end


    local type
    if minetest.features.hud_def_type_field then
        type = "type"
    else
        type = "hud_elem_type"
    end
    


    minimap_huds[p_name] = {
        
        [1] = player:hud_add({
            [type] = "compass",
            size = { x = mini_map_size, y = mini_map_size },
            position = { x = 1, y = 0 },
            alignment = { x = -1, y = 1 },
            offset = { x = -mini_map_top_right_corner_offset, y = mini_map_top_right_corner_offset },
            direction = 1,
            text = "blank.png",
        }),

        -- [2] = player:hud_add({
        --     [type] = "image",
        --     scale = { x = 1, y = 1 },
        --     position = { x = 1, y = 0 },
        --     alignment = { x = -1, y = 1 },
        --     offset = { x = -center_offset, y = center_offset },
        --     text = "froyale_hud_minimap_player.png",
        -- }),
    }
end

-- spectated_name is optional, indicates that the player is spectating another player
function fantasia.HUD_minimap_update(p_name, arena, spectated_name)

    if not minimap_huds[p_name] then return end
    
    local player = minetest.get_player_by_name(p_name)

    if not player then return end

    local texture

    if spectated_name then
        texture = get_minimap_texture(spectated_name, minetest.get_player_by_name(spectated_name), arena)
    else
        texture = get_minimap_texture(p_name, player, arena)
    end

    player:hud_change(minimap_huds[p_name][1], "text", texture)
    -- player:hud_change(test_huds[p_name], "text", texture)

    -- also get any specators of the player
    if not spectated_name then
        local spectators = arena_lib.get_target_spectators("fantasia_royale", arena.name, "player", p_name)
        for spec_name, bool in pairs(spectators) do
            local sp_player = minetest.get_player_by_name(spec_name)
            if sp_player then
                player:hud_change(minimap_huds[spec_name][1], "text", texture)
            end
        end
    end
end



fantasia.register_on_time_tick("minimap_huds", function(arena)
    for pl_name, stats in pairs(arena.players) do
        fantasia.HUD_minimap_update(pl_name, arena)
        local spectators = arena_lib.get_target_spectators("fantasia_royale", arena.name, "player", pl_name)
        for spec_name, bool in pairs(spectators) do
            fantasia.HUD_minimap_update(spec_name, arena, pl_name)
        end
    end
end)


function fantasia.HUD_minimap_remove(p_name)

    if not minimap_huds[p_name] then return end

    local player = minetest.get_player_by_name(p_name)

    if not player then return end

    player:hud_remove(minimap_huds[p_name][1])
    -- player:hud_remove(minimap_huds[p_name][2])

    minimap_huds[p_name] = nil
end

function get_minimap_texture(p_name, player, arena)

    local arena_top_left_x = math.min(arena.pos1.x, arena.pos2.x)
    local arena_top_left_z = math.max(arena.pos1.z, arena.pos2.z)
    
    local player_pos = player:get_pos()
    local center_x = math.floor(player_pos.x)
    local center_z = math.floor(player_pos.z)

    local view_size = 350 -- hardcoded so that the image manipulation is easier

    local player_view_top_left_x = center_x - view_size / 2
    local player_view_top_left_z = center_z + view_size / 2

    local x_offset = math.floor(arena_top_left_x - player_view_top_left_x)
    local z_offset = math.floor(player_view_top_left_z - arena_top_left_z)

    local minimap_texture = "minimap_" .. arena.name .. ".png" .."\\^[transformFY\\^[transformFX"
    -- the minimap is always made in the same orientation, with x and z flipped,
    -- and so now the minimap has been fixed to be North up and East right.

    -- overlay the storm on the minimap
    
    -- the size of the minimap texture is the same as the size of the arena plus 1
    -- we need to scale the storm texture to the size of the storm.

    
    local arena_top_left = vector.apply(vector.new(math.min(arena.pos1.x, arena.pos2.x), 0, math.max(arena.pos1.z, arena.pos2.z)) , math.floor)
    local arena_bottom_left = vector.apply(vector.new(math.min(arena.pos1.x, arena.pos2.x), 0, math.min(arena.pos1.z, arena.pos2.z)) , math.floor)
    local arena_top_right = vector.apply(vector.new(math.max(arena.pos1.x, arena.pos2.x), 0, math.max(arena.pos1.z, arena.pos2.z)) , math.floor)
    local arena_bottom_right = vector.apply(vector.new(math.max(arena.pos1.x, arena.pos2.x), 0, math.min(arena.pos1.z, arena.pos2.z)) , math.floor)
    local arena_size = arena_top_right - arena_bottom_left


    local storm_radius = arena.storm_radius

    local storm_center = vector.apply(vector.new(arena.storm_center.x, 0, arena.storm_center.z), math.floor)

    local storm_top_left = vector.apply(storm_center + vector.new( -arena.storm_radius, 0, arena.storm_radius), math.floor)
    local storm_bottom_left = vector.apply(storm_center + vector.new( -arena.storm_radius, 0, -arena.storm_radius), math.floor)
    local storm_top_right = vector.apply(storm_center + vector.new( arena.storm_radius, 0, arena.storm_radius), math.floor)
    local storm_bottom_right = vector.apply(storm_center + vector.new( arena.storm_radius, 0, -arena.storm_radius), math.floor)
    
    local draw_top_fill = storm_top_left.z < arena_top_left.z
    local draw_bottom_fill = storm_bottom_left.z > arena_bottom_left.z
    local draw_left_fill = storm_top_left.x > arena_top_left.x
    local draw_right_fill = storm_top_right.x < arena_top_right.x

    local function world_pos_to_minimap_pos(world_pos)
        local minimap_pos = world_pos - arena_top_left
        minimap_pos.z = - minimap_pos.z
        return vector.apply(minimap_pos, math.floor)
    end

    local top_fill_height = math.abs(storm_top_left.z - arena_top_left.z)
    local top_fill_width = arena_top_right.x - arena_top_left.x
    local top_fill_top_corner = arena_top_left
    local mm_top_fill_top_corner = world_pos_to_minimap_pos(top_fill_top_corner)

    local bottom_fill_height = math.abs(storm_bottom_left.z - arena_bottom_left.z)
    local bottom_fill_width = arena_top_right.x - arena_top_left.x
    local bottom_fill_top_corner = vector.new(arena_top_left.x, 0, storm_bottom_left.z)
    local mm_bottom_fill_top_corner = world_pos_to_minimap_pos(bottom_fill_top_corner)
    
    local left_fill_height = math.abs(storm_top_left.z - storm_bottom_left.z)
    local left_fill_width = math.abs(storm_top_left.x - arena_top_left.x)
    local left_fill_top_corner = vector.new(arena_top_left.x, 0, storm_top_left.z)
    local mm_left_fill_top_corner = world_pos_to_minimap_pos(left_fill_top_corner)

    local right_fill_height = math.abs(storm_top_right.z - storm_bottom_right.z)
    local right_fill_width = math.abs(storm_top_right.x - arena_top_right.x)
    local right_fill_top_corner = storm_top_right
    local mm_right_fill_top_corner = world_pos_to_minimap_pos(right_fill_top_corner)


    local storm_offset = storm_top_left - arena_top_left

    local storm_size = vector.apply(vector.new(arena.storm_radius * 2, 0, arena.storm_radius * 2), math.floor)
    local storm_texture = "(froyale_minimap_storm.png\\\\^[resize\\\\:" .. storm_size.x .. "x" .. storm_size.z .. ")"

    -- build the texture
    local combine = {
        "[combine\\:",
        arena_size.x, "x", arena_size.z, "\\:",
        0, ",", 0, "=", "(" .. minimap_texture .. ")", 
        "\\:",
        storm_offset.x, ",", -storm_offset.z, "=", storm_texture,
    }

    local function overlay_storm_fill(bool,w,h,x,z)
        if bool then
            local texture = "(froyale_minimap_storm_fill.png\\\\^[resize\\\\:" .. w .. "x" .. h .. ")"
            combine = append_table_to_table(combine, {
                "\\:",
                x, ",", z, "=", texture,
            })
        end
    end

    overlay_storm_fill(draw_top_fill, top_fill_width, top_fill_height, mm_top_fill_top_corner.x, mm_top_fill_top_corner.z)
    overlay_storm_fill(draw_bottom_fill, bottom_fill_width, bottom_fill_height, mm_bottom_fill_top_corner.x, mm_bottom_fill_top_corner.z)
    overlay_storm_fill(draw_left_fill, left_fill_width, left_fill_height, mm_left_fill_top_corner.x, mm_left_fill_top_corner.z)
    overlay_storm_fill(draw_right_fill, right_fill_width, right_fill_height, mm_right_fill_top_corner.x, mm_right_fill_top_corner.z)

    -- next, we will show the storm target and second target, if the player is allowed to see them

    if arena.rounds_complete >= 1 and arena.players[p_name].can_view_storm_target_1 then
        local target_1_center = vector.apply(vector.new(arena.next_center.x, 0, arena.next_center.z), math.floor)
        local target_1_top_left = vector.apply(target_1_center + vector.new( -arena.next_radius, 0, arena.next_radius), math.floor)    
        local target_1_offset = target_1_top_left - arena_top_left    
        local target_1_size = vector.apply(vector.new(arena.next_radius * 2, 0, arena.next_radius * 2), math.floor)    
        local target_1_texture = "(froyale_minimap_storm_target_1.png\\\\^[resize\\\\:" .. target_1_size.x .. "x" .. target_1_size.z .. ")"    


        combine = append_table_to_table(combine, {
            "\\:",
            target_1_offset.x, ",", -target_1_offset.z, "=", target_1_texture,
        })

        if arena.players[p_name].can_view_storm_target_2 then
            local target_2_center = vector.apply(vector.new(arena.next_center2.x, 0, arena.next_center2.z), math.floor)
            local target_2_top_left = vector.apply(target_2_center + vector.new( -arena.next_radius2, 0, arena.next_radius2), math.floor)
            local target_2_offset = target_2_top_left - arena_top_left
            local target_2_size = vector.apply(vector.new(arena.next_radius2 * 2, 0, arena.next_radius2 * 2), math.floor)    
            local target_2_texture = "(froyale_minimap_storm_target_2.png\\\\^[resize\\\\:" .. target_2_size.x .. "x" .. target_2_size.z .. ")"
    
    
            combine = append_table_to_table(combine, {
                "\\:",
                target_2_offset.x, ",", -target_2_offset.z, "=", target_2_texture,
            })
        end
    end

    -- next, we will show any POIs that are on the minimap. If they are off the
    -- circle defined by the view range then we will show them as a dot on the
    -- edge of the circle.

    for color, pos in pairs(arena.players[p_name].pois) do
        local poi_center = vector.apply(vector.new(pos.x, 0, pos.z), math.floor)
        local view_center = vector.new(center_x, 0, center_z)
        local dist = vector.distance(view_center, poi_center)
        
        if dist < view_size / 2 then
            -- POI is within view range
            local poi_top_left = poi_center + vector.new(-8, 0, 8)
            local poi_offset = poi_top_left - arena_top_left
            combine = append_table_to_table(combine, {
                "\\:",
                poi_offset.x, ",", -poi_offset.z, "=", fantasia.poi_colors[color].poi_dot_texture,
            })
        else
            -- POI is outside view range, place it on the edge
            local vector_from_center_to_poi = vector.subtract(poi_center, view_center)
            local normal_offset = vector.normalize(vector_from_center_to_poi)
            
            -- Place the dot at the edge of the view range
            local dot_center = vector.multiply(normal_offset, (view_size / 2) - 6)
            dot_center = vector.add(dot_center, view_center)
            local dot_top_left = dot_center + vector.new(-8, 0, 8)
            local dot_offset = dot_top_left - arena_top_left
            
            combine = append_table_to_table(combine, {
                "\\:",
                dot_offset.x, ",", -dot_offset.z, "=", fantasia.poi_colors[color].poi_trans_dot_texture,
            })
        end
    end
    


    minimap_texture = table.concat(combine)



    -- cut it down to view size

    local texture = {
        "[combine\\:", 
        tostring(view_size), "x", tostring(view_size),
        "\\:", 
        tostring(x_offset), ",",tostring(z_offset),"=", minimap_texture,
    }

    -- handle placing dots on the edge of the minimap
    
    -- for color, pos in pairs(arena.players[p_name].pois) do

    --     local poi_center = vector.apply(vector.new(pos.x, 0, pos.z), math.floor)
    --     -- next, see if it is in the view range.
    --     local view_center = vector.new(center_x, 0, center_z)
    --     local dist = vector.distance(view_center, poi_center)
    --     if dist >= view_size / 2 then
    --         -- get the direction of the poi from the player
    --         local poi_offset_from_player = poi_center - view_center
    --         poi_offset_from_player.z = -poi_offset_from_player.z
    --         local normal_offset = vector.normalize(poi_offset_from_player)

    --         local dot_center = vector.apply(vector.multiply(normal_offset, (view_size / 2) - 10), math.floor)
    --         local dot_top_left = dot_center + vector.new(-8, 0, 8)


    --         texture = append_table_to_table(texture, {
    --             "\\:",
    --             dot_top_left.x, ",", dot_top_left.z, "=", fantasia.poi_colors[color].poi_dot_texture,
    --         })
    --     end

    -- end



    texture = table.concat(texture)

    -- texture = "(" .. texture .. ")"
    texture = texture .. "^" .. "(froyale_hud_minimap_mask.png)" 
    texture = texture .. "^[makealpha:000,000,000"

    return texture
end


function append_table_to_table(t1,t2)
    for k,v in pairs(t2) do
        table.insert(t1, v)
    end
    return t1
end