local poi_huds = {}

function fantasia.HUD_poi_create(p_name, arena, color, pos)

    if not arena then return end
    if not arena.in_game then return end

    local poi_texture = fantasia.poi_colors[color].poi_texture
    local player = minetest.get_player_by_name(p_name)

    if not player then return end

    if not poi_huds[p_name] then poi_huds[p_name] = {} end

    if poi_huds[p_name][color] then
        player:hud_remove(poi_huds[p_name][color])
        poi_huds[p_name][color] = nil
    end

    poi_huds[p_name][color] = player:hud_add({
        hud_elem_type = "image_waypoint",
        world_pos = pos,
        text = poi_texture,
        scale = {x=2, y=2}
    })
end

function fantasia.HUD_poi_remove(p_name, color)

    local player = minetest.get_player_by_name(p_name)
    if not player then return end

    if not poi_huds[p_name] then return end
    if not poi_huds[p_name][color] then return end

    player:hud_remove(poi_huds[p_name][color])
    poi_huds[p_name][color] = nil

end

function fantasia.HUD_poi_remove_all(p_name)

    local player = minetest.get_player_by_name(p_name)
    if not player then return end

    if not poi_huds[p_name] then return end

    for k, v in pairs(poi_huds[p_name]) do
        player:hud_remove(v)
    end
    poi_huds[p_name] = nil

end


arena_lib.register_on_eliminate(function(mod, arena, p_name)
    if mod == "fantasia_royale" then
        fantasia.HUD_poi_remove_all(p_name)
    end
end)