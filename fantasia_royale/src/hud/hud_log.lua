local function get_row_length() end
local function calc_action_offset() end



local ROW1_HEIGHT_TXT = 24
local ROW2_HEIGHT_TXT = 76
local ROW3_HEIGHT_TXT = 128
local ROW1_HEIGHT_IMG = ROW1_HEIGHT_TXT -14
local ROW2_HEIGHT_IMG = ROW2_HEIGHT_TXT -14
local ROW3_HEIGHT_IMG = ROW3_HEIGHT_TXT -14
local ROW1_OFFSET_BG = ROW1_HEIGHT_TXT -7
local ROW2_OFFSET_BG = ROW2_HEIGHT_TXT -7
local ROW3_OFFSET_BG = ROW3_HEIGHT_TXT -7
local BG = "froyale_util_blacksquare.png^[opacity:180]"


function fantasia.HUD_log_create(p_name)
  local icon_scale = { x = 3, y = 3}
  local bg_scale = { x = 10, y = 2}

  Panel:new("froyale_log", {
    player = p_name,
    bg = "",
    position = { x = 1, y = 0.55 },
    alignment = { x = -1, y = 1 },

    sub_img_elems = {
      action_1_bg = {
        alignment = {x = -1, y = 1},
        offset = { x = 0, y = ROW1_OFFSET_BG},
        scale = bg_scale,
        z_index = -1
      },
      action_2_bg = {
        alignment = {x = -1, y = 1},
        offset = { x = 0, y = ROW2_OFFSET_BG},
        scale = bg_scale,
        z_index = -1,
      },
      action_3_bg = {
        alignment = {x = -1, y = 1},
        offset = { x = 0, y = ROW3_OFFSET_BG},
        scale = bg_scale,
        z_index = -1,
      },
      action_1 = {
        alignment = {x = -1, y = 1},
        scale = icon_scale
      },
      action_2 = {
        alignment = {x = -1, y = 1},
        scale = icon_scale
      },
      action_3 = {
        alignment = {x = -1, y = 1},
        scale = icon_scale
      },
    },
    sub_txt_elems = {
      executor_1 = {
        alignment = {x = -1, y = 1},
        style = 4
      },
      executor_2 = {
        alignment = {x = -1, y = 1},
        offset = { x = -20, y = ROW2_HEIGHT_TXT },
        style = 4
      },
      executor_3 = {
        alignment = {x = -1, y = 1},
        offset = { x = -20, y = ROW3_HEIGHT_TXT },
        style = 4
      },
      receiver_1 = {
        alignment = {x = -1, y = 1},
        offset = { x = -20, y = ROW1_HEIGHT_TXT },
        style = 4
      },
      receiver_2 = {
        alignment = {x = -1, y = 1},
        offset = { x = -20, y = ROW2_HEIGHT_TXT },
        style = 4
      },
      receiver_3 = {
        alignment = {x = -1, y = 1},
        offset = { x = -20, y = ROW3_HEIGHT_TXT },
        style = 4
      },
    }
  })
end



function fantasia.HUD_log_update(arena, action_img, executor, receiver, assister)
  for psp_name, _ in pairs(arena.players_and_spectators) do
    local panel = panel_lib.get_panel(psp_name, "froyale_log")
    local executor_color = psp_name == executor and 0xffffff or 0xff5a3f
    local receiver_color = psp_name == receiver and 0xffffff or 0xff5a3f
    local rec1_offset = calc_action_offset(panel.receiver_2.text)
    local rec2_offset = calc_action_offset(panel.receiver_3.text)
    local rec3_offset = calc_action_offset(receiver)

    -- TODO 5.9: assister should always be red if we want to feature assists.
    -- Featuring more colours in HUD will be possible starting from MT 5.9 https://github.com/minetest/minetest/pull/14558
    --local executors = assister and (assister .. " + " .. executor) or executor
    local executors = executor or ""

    panel:update(nil,
    -- bg & icons
    {
      action_1_bg = {
        scale = { x = get_row_length(psp_name, panel.receiver_2.text, panel.executor_2.text) / 14, y = 2},
        text = panel.action_2.text ~= "" and BG or ""
      },
      action_2_bg = {
        scale = { x = get_row_length(psp_name, panel.receiver_3.text, panel.executor_3.text) / 14, y = 2},
        text = panel.action_3.text ~= "" and BG or ""
      },
      action_3_bg = {
        scale = { x = get_row_length(psp_name, receiver, executor) / 14, y = 2},
        text = BG
      },
      action_1 = {
        offset = { x = rec1_offset, y = ROW1_HEIGHT_IMG },
        text = panel.action_2.text
      },
      action_2 = {
        offset = { x = rec2_offset, y = ROW2_HEIGHT_IMG },
        text = panel.action_3.text
      },
      action_3 = {
        offset = { x = rec3_offset, y = ROW3_HEIGHT_IMG },
        text = action_img
      }
    },

    -- txt
    {
      executor_1 = {
        offset = { x = rec1_offset - 60, y = ROW1_HEIGHT_TXT },
        number = panel.executor_2.number,
        text = panel.executor_2.text
      },
      executor_2 = {
        offset = { x = rec2_offset - 60, y = ROW2_HEIGHT_TXT },
        number = panel.executor_3.number,
        text = panel.executor_3.text
      },
      executor_3 = {
        offset = { x = rec3_offset - 60, y = ROW3_HEIGHT_TXT },
        number = executor_color,
        text = executors
      },
      receiver_1 = {
        number = panel.receiver_2.number,
        text = panel.receiver_2.text
      },
      receiver_2 = {
        number = panel.receiver_3.number,
        text = panel.receiver_3.text
      },
      receiver_3 = {
        number = receiver_color,
        text = receiver
      }
    })

  end
end



function fantasia.HUD_log_clear(arena)
  for psp_name, _ in pairs(arena.players_and_spectators) do
    local panel = panel_lib.get_panel(psp_name, "froyale_log")

    panel:update(nil,
    -- icons
    {
      action_1_bg = {text = ""},
      action_2_bg = {text = ""},
      action_3_bg = {text = ""},
      action_1 = {text = ""},
      action_2 = {text = ""},
      action_3 = {text = ""}
    },

    -- txt
    {
      executor_1 = {text = ""},
      executor_2 = {text = ""},
      executor_3 = {text = ""},
      receiver_1 = {text = ""},
      receiver_2 = {text = ""},
      receiver_3 = {text = ""}
    })
  end
end





----------------------------------------------
---------------LOCAL FUNCTIONS----------------
----------------------------------------------

function get_row_length(p_name, receiver, executor)
  local icon_size = 32
  local padding = executor and 48 or 35
  local char_amount = string.len(receiver) + string.len(executor or "")
  local distance = 7.3
  local window_info = minetest.get_player_window_information(p_name)
  local window_size_x = window_info and window_info.size.x or 1920 -- clients <5.7 won't return window_info, avoid crash
  local screen_x_ratio = 1920 / window_size_x -- text isn't affected by screen size, scale is. This fixes it

  return (-icon_size - padding - (distance * char_amount)) * screen_x_ratio
end



function calc_action_offset(receiver)
  return -27 - (10 * string.len(receiver))
end
