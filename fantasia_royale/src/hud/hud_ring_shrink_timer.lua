-- the minimap HUD script ought to be run before this one.

local y_offset = fantasia.minimap_screenspace

function fantasia.HUD_ring_shrink_timer_create(p_name)
    Panel:new("froyale_ring_shrink_timer", {
        player = p_name,
        bg = "froyale_hud_ring_shrink_bg.png",
        title = "",
        position = { x = 1, y = 0 },
        alignment = { x = -1, y = 1 },
        offset = { x = 0, y = y_offset},
        bg_scale = { x = 2, y = 2},

        sub_txt_elems = {
            title = {
                offset = {x = 0, y = 8},
                text = "Ring Shrinks in:" .. "  ",
                alignment = { x = -1, y = 1 },
                size = {x=1},
                number = 0xa93b3b
            },
            timer = {
                offset = {x = -70, y = 42},
                alignment = { x = 0, y = 0 },
                text = "5",
                size = {x=2},
                number = 0xe6482e
            }
        }
    })
end

function fantasia.HUD_ring_shrink_timer_create_update_all(arena)
    for pl_name, _ in pairs(arena.players_and_spectators) do
        local panel = panel_lib.get_panel(pl_name, "froyale_ring_shrink_timer")
        if not panel then -- create the panel if it doesn't exist
            fantasia.HUD_ring_shrink_timer_create(pl_name)
            panel = panel_lib.get_panel(pl_name, "froyale_ring_shrink_timer")
        end
        panel:update(nil,{
            timer = {
                text = tostring(arena.round_timer)
            }
        })
    end
end


function fantasia.HUD_ring_shrink_remove(p_name)
    local panel = panel_lib.get_panel(p_name, "froyale_ring_shrink_timer")
    if panel then
        panel:remove()
    end
end

function fantasia.HUD_ring_shrink_remove_all(arena)
    for pl_name, stats in pairs(arena.players_and_spectators) do
        fantasia.HUD_ring_shrink_remove(pl_name)
        -- play a heads up effect
        minetest.sound_play("froyale_alarm", {to_player = pl_name}, true)
    end
end