function fantasia.hud_remove_all(p_name)
	panel_lib.get_panel(p_name, "froyale_mana"):remove()
	panel_lib.get_panel(p_name, "storm_players_counter"):remove()
	panel_lib.get_panel(p_name, "froyale_log"):remove()
	-- these have built-in nil checks
	fantasia.HUD_poi_remove_all(p_name)
	fantasia.HUD_storm_overlay_remove(p_name)
	fantasia.HUD_ring_shrink_remove(p_name)
	fantasia.HUD_minimap_remove(p_name)
end