function fantasia.HUD_players_counter_create(p_name)
	local arena = arena_lib.get_arena_by_player(p_name)
	local past_present_pls_amount = 0

	for _, _ in pairs(arena.past_present_players) do
		past_present_pls_amount = past_present_pls_amount + 1
	end

	Panel:new("storm_players_counter", {
		player = p_name,
		bg = "froyale_hud_pl_counter.png",
		bg_scale = { x = 3.5, y = 3.5 },
		position = { x = 0.45, y = 0 },
		offset = {x = 0, y = 50},
		alignment = {x = 1.0},
		title_offset = {x=39, y=3},
		title = tostring(arena.players_amount) .. "/" .. tostring(past_present_pls_amount),
	})
end



function fantasia.HUD_players_counter_update(arena)
	for psp_name, _ in pairs(arena.players_and_spectators) do
		local panel = panel_lib.get_panel(psp_name, "storm_players_counter")

		local past_present_pls_amount = 0
		for _, _ in pairs(arena.past_present_players) do
			past_present_pls_amount = past_present_pls_amount + 1
		end

		panel:update({
			title = tostring(arena.players_amount) .. "/" .. tostring(past_present_pls_amount)
		})
	end
end
