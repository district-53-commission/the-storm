local overlays = {}


function fantasia.has_HUD_storm_overlay(p_name)
    return overlays[p_name] ~= nil
end


-- as_spectator = true if the function is called recursively by itself for a spectator
function fantasia.HUD_storm_overlay_create(p_name, as_spectator)
    local player = minetest.get_player_by_name(p_name)
    if not player then return end
    -- dont make a new overlay if it exists already
    if overlays[p_name] then return end

    overlays[p_name] = player:hud_add({
		hud_elem_type = "image",
		text = "froyale_ring.png",
		position = {x=0, y=0},
		scale = {x=-100, y=-100},
		alignment = {x=1, y=1},
		offset = {x=0, y=0}
	})


    -- get spectators
    if not as_spectator then
        local arena = arena_lib.get_arena_by_player(p_name)
        local spectators = arena_lib.get_target_spectators("fantasia_royale", arena.name, "player", p_name)
        for spec_name, bool in pairs(spectators) do

            fantasia.HUD_storm_overlay_create(spec_name, true)

        end
    end
end

-- as_spectator = true if the function is called recursively by itself for a spectator
function fantasia.HUD_storm_overlay_remove(p_name, as_spectator)

    local player = minetest.get_player_by_name(p_name)
    if not player then return end
    if not overlays[p_name] then return end
    player:hud_remove(overlays[p_name])
    overlays[p_name] = nil

    -- get spectators
    if not as_spectator then
        local arena = arena_lib.get_arena_by_player(p_name)
        if not arena then return end -- this happens if the target is the last player in the arena
        local spectators = arena_lib.get_target_spectators("fantasia_royale", arena.name, "player", p_name)
        for spec_name, bool in pairs(spectators) do
            local sp_player = minetest.get_player_by_name(spec_name)
            if sp_player then
                fantasia.HUD_storm_overlay_remove(spec_name, true)
            end
        end
    end
end


minetest.register_on_leaveplayer(function(player, timed_out)
    if overlays[player:get_player_name()] then
        overlays[player:get_player_name()] = nil
    end
end)


