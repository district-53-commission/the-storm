function fantasia.generate_controls_hud(p_name)
	local player = minetest.get_player_by_name(p_name)

	if not player or not arena_lib.is_player_in_arena(p_name, "fantasia_royale") then
		if player then hud_fs.close_hud(player, "fantasia_royale:controls") end
		return false
	end

	local class = classy.get_class_by_name(fantasia.get_class(p_name))
	local skill = p_name:get_skill(class.starting_skills[1])

	local function get_overlay(skill)
		local black_overlay = "froyale_hud_skill_dark_overlay.png"
		if skill.cooldown_timer <= 0 then
			return "blank.png"
		else
			return black_overlay
		end
	end

	local function get_key_overlay(skill)
		local dark_overlay = "^[colorize:#302c2e:175"
		local cd = skill.cooldown_timer
		if cd > 0 then
			return dark_overlay
		else
			return ""
		end
	end

	local function get_cooldown(skill)
		if skill.cooldown_timer > 0 then
			return math.floor(skill.cooldown_timer+0.5) .. "s"
		else
			return ""
		end
	end

	local formspec = [[
		formspec_version[3]
		size[2,2]
		position[0.08,0.78]
		no_prepend[]
		bgcolor[#FF00FF00]

		style[mid_label;border=false;font_size=*2]
		style[big_label;border=false;font_size=*3]
		style[small_label;border=false;font_size=*1]

		image[0.22,1.5;1.25,1.25;froyale_hud_skill_slot.png;] ]] ..
		"image[0.47,1.75;0.75,0.75;" .. skill._icon .. ";]"..
		"image[0.22,1.5;1.25,1.25;" .. get_overlay(skill) .. "]" ..
		"image[0,2.29;0.65,0.65;froyale_hud_btnZ.png".. get_key_overlay(skill) .. ";]" ..
		"image_button[0.4,1.7;1,1;blank.png;mid_label;" .. get_cooldown(skill) .. "]"


	hud_fs.show_hud(player, "fantasia_royale:controls", formspec)

	minetest.after(0.3, function ()
		fantasia.generate_controls_hud(p_name)
	end)
end