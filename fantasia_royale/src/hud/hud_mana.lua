function fantasia.HUD_mana_create(p_name)

  Panel:new("froyale_mana", {
    player = p_name,
    bg = "froyale_hud_mana_empty.png",  -- todo: make different bg filled with #31a2c3 for when they have mana reserves
    title = "",
    position = { x = 0.5, y = 1 },
    alignment = { x = 1, y = 0 },
    offset = { x = 20, y = -76},
    bg_scale = { x = 1.5, y = 1.5},
    sub_img_elems = {
      mana_indicator = {
        offset = {x = 5},
        scale = {x = 1.5, y= 1.5},
        text = "froyale_hud_mana.png"
      },
    },
    sub_txt_elems = {
      vials_amount = {
        offset = {x = 275, y = 1},
        text = "x 0",
        number = 0xdff6f5
      }
    }
  })
end



function fantasia.HUD_mana_update(p_name)
  local mana = p_name:get_mana()
  local mana_reserve = p_name:get_mana_reserve()
  local panel = panel_lib.get_panel(p_name, "froyale_mana")
  local remaining_vials = math.floor(mana_reserve/fantasia.MAX_MANA)
  local bg = mana_reserve > 0 and "froyale_hud_mana_reserve.png" or "froyale_hud_mana_empty.png"

  panel:update({bg=bg},
    {mana_indicator = {
      scale = {x = (mana / 10) * 1.5, y = 1.5}
    }},
    {vials_amount = {
      text = "x "..remaining_vials,
    }}
  )

  for sp_name, _ in pairs(arena_lib.get_player_spectators(p_name)) do
    local sp_panel = panel_lib.get_panel(sp_name, "froyale_mana")

    sp_panel:update({bg=bg},
    {mana_indicator = {
      scale = {x = (mana / 10) * 1.5, y = 1.5}
    }},
    {vials_amount = {
      text = "x "..remaining_vials,
    }}
  )
  end
end
