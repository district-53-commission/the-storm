arena_lib.on_start("fantasia_royale", function(arena)
    fantasia.fill_loot(arena)
    for p_name, _ in pairs(arena.players) do
        fantasia.generate_controls_hud(p_name)
    end
end)