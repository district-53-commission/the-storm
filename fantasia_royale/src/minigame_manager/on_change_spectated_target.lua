arena_lib.on_change_spectated_target("fantasia_royale", function(arena, sp_name, t_type, t_name, prev_type, prev_spectated, is_forced)
    
    -- minimap
    if t_type == "player" then
        -- calling create removes the old HUD
        fantasia.HUD_minimap_create(sp_name, arena)
        -- update the HUD for the minimap with the new target
        fantasia.HUD_minimap_update(sp_name, arena, t_name)
    else
        -- remove the HUD for the minimap
        fantasia.HUD_minimap_remove(sp_name)
    end


    -- storm overlay
    local player = minetest.get_player_by_name(sp_name)
    local pos
    if t_type == "player" then
        pos = minetest.get_player_by_name(t_name):get_pos()
    else
        pos = player:get_pos()
    end

    local is_in_storm = fantasia.is_pos_in_storm(arena, pos)
    if not is_in_storm and fantasia.has_HUD_storm_overlay(sp_name) then
        fantasia.HUD_storm_overlay_remove(sp_name)
    end
    if is_in_storm and not fantasia.has_HUD_storm_overlay(sp_name) then
        fantasia.HUD_storm_overlay_create(sp_name)
    end

end)
