local S = minetest.get_translator("fantasia_royale")

local adapt_radius = function(arena, pl_name) end

arena_lib.on_enable("fantasia_royale", function(arena, pl_name)
	if arena.treasures[1] == nil then
		fantasia.print_error(pl_name, S("You haven't set the treasures!"))
		return false
	elseif arena.chests[1] == nil then
		fantasia.print_error(pl_name, S("You haven't set the chests!"))
		return false
	end

	minetest.forceload_block(arena.storm_center_start, false, -1)

	-- check ground level; we need it to auto-set the storm center
	if arena.ground_level == -34000 then
		fantasia.print_error(pl_name, S("You haven't set the ground level! It's used to calculate the center!"))
		return false
	end

	-- check the minimap
	local path = minetest.get_worldpath() .. DIR_DELIM .. "fantasia_royale" .. DIR_DELIM .. "minimap" .. DIR_DELIM .. "minimap_" .. arena.name .. ".png"
	-- check if the file is there
	local f = io.open(path, "r")
	if not f then
		fantasia.print_error(pl_name, S("The minimap doesn't exist!"))
		return false
	end


	adapt_radius(arena, pl_name)

	-- check center integrity: the center must be within the arena.
	local p1_exists = (arena.pos1 ~= nil)
	local p2_exists = (arena.pos2 ~= nil)
	local center_exists = (arena.storm_center_start ~= nil)
	
	local center_is_in_arena = vector.in_area(arena.storm_center_start, arena.pos1, arena.pos2)
	if not p1_exists or not p2_exists or not center_exists then
		fantasia.print_error(pl_name, S("You haven't set the arena bounds and center!"))
	end

	if not center_is_in_arena then
		fantasia.print_error(pl_name, S("The center is not in the arena!"))
	end

	return true

end)


function adapt_radius(arena, pl_name)
	-- if arena.storm_center_start is (0,0,0) then we will automatically set it to the center of the arena.
	if arena.storm_center_start.x == 0 and arena.storm_center_start.y == 0 and arena.storm_center_start.z == 0 then
		local arena_center = vector.apply(vector.new(
			(arena.pos1.x + arena.pos2.x)/2,
			(arena.pos1.y + arena.pos2.y)/2,
			(arena.pos1.z + arena.pos2.z)/2), function(x) return math.floor(x) end)
		
		arena_center.y = arena.ground_level + 160 -- a good height for the storm above ground level

		-- arena_lib.change_arena_property(<sender>, mod, arena_name, property, new_value)
		arena_lib.change_arena_property(pl_name, "fantasia_royale", arena.name, "storm_center_start", arena_center)

		-- let the editor know that the arena center has changed
		fantasia.print_msg(pl_name, S("The storm_center_start has been set to @1!", vector.to_string(arena_center)))
	end


	-- if arena.starting_storm_radius == 0 then we will automatically set it to
	-- the largest possible given the arena size and storm_center_start. This
	-- assumes the entire play area is available for the first round by choosing
	-- the radius to be the maximum distance from the center to a corner.
	if arena.starting_storm_radius == 0 then
		local c = vector.new(arena.storm_center_start.x, 0, arena.storm_center_start.z)
		local a = vector.new(arena.pos1.x, 0, arena.pos1.z)
		local b = vector.new(arena.pos2.x, 0, arena.pos2.z)
		local new_radius = math.max(
			vector.distance(c, a),
			vector.distance(c, b)
		)
		arena_lib.change_arena_property(pl_name, "fantasia_royale", arena.name, "starting_storm_radius", new_radius)

		-- let the editor know that the arena radius has changed
		fantasia.print_msg(pl_name, S("The starting_storm_radius has been set to @1!", tostring(new_radius)))
	end
end