local lerp = function(a, b, t) end
local vector_lerp = function(a, b, t) end

minetest.register_globalstep(function(dtime)
    -- the globalstep needs to handle changing the position and size of the storm over time
    for _, arena in pairs(arena_lib.mods["fantasia_royale"].arenas) do
        if arena.in_game and arena.storm_loaded and arena.round_timer > arena.stop_time then

            -- lineraly interpolate the size and offset of the storm based on elapsed time since the start of the round
            local current_us_time = minetest.get_us_time()
            local elapsed_us_time = current_us_time - arena.round_start_timestamp
            local elapsed_seconds = elapsed_us_time / 1000000.0

            if elapsed_seconds < arena.stop_time then
                -- linearly interpolate the size and offset from arena.round_start_offset and arena.round_start_size to the targets
                local start_offset = arena.round_start_offset
                local start_size = arena.round_start_size
                local target_offset = arena.next_offset
                local target_size = arena.next_radius
                
                arena.storm_offset = vector_lerp(start_offset, target_offset, elapsed_seconds/arena.stop_time)
                arena.storm_radius = lerp(start_size, target_size, elapsed_seconds/arena.stop_time)

            else
                arena.storm_offset = arena.next_offset
                arena.storm_radius = arena.next_radius

            end

            fantasia.update_storm_properties(arena)
            fantasia.handle_stormsound_globalstep(arena)

        end
    end
end)


function lerp(a, b, t)
    return a + (b - a) * t
end

function vector_lerp(a, b, t)
    return vector.add(a, vector.multiply(vector.subtract(b, a), t))
end
