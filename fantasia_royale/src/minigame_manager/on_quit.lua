arena_lib.on_quit("fantasia_royale", function(arena, p_name, is_spectator, reason)
	local player = minetest.get_player_by_name(p_name)
	if player then
		fantasia.remove_glider_object(player, true) -- ignore if not found is true. Just do it.
	end
	fantasia.hud_remove_all(p_name)
	fantasia.restore_inventory(minetest.get_player_by_name(p_name))
	fantasia.end_storm_loop_sounds(p_name)
end)