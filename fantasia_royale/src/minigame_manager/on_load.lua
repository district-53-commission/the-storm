local function try_to_load_stormspawn(arena) end
local function reset_meta(p_name) end
local function create_hud(p_name) end
local function wait_until_loaded(pos,callback,iter) end
local function get_blockpos(pos) end

arena_lib.on_load("fantasia_royale", function(arena)
    -- generate the match unique identifier
    arena.storm_radius = arena.starting_storm_radius
    arena.next_radius = arena.starting_storm_radius
    arena.round_timer = arena.first_round_time
    arena.leaderboard = fantasia.retrieve_leaderboard_from_storage(arena) or {}
    arena.dropbox_rounds_left = arena.num_dropbox_rounds

    -- keep the entity at the center always loaded. We do offsets via bone positions
    try_to_load_stormspawn(arena)

    -- see vehicles/initial_vehicle.lua. This creates the initial vehicle and
    -- attaches players to it, and sets up the loop of tracking the initial
    -- vehicle and allowing players to drop off.
    fantasia.create_initial_vehicle(arena)

    for pl_name, _ in pairs(arena.players) do
        local player = minetest.get_player_by_name(pl_name)
        reset_meta(pl_name)
        classy.set_player_class(pl_name, fantasia.get_class(pl_name), true)
        -- hide the player now that setting class may have changed their visual size
        player:set_properties({visual_size = {x = 0, y = 0}})
        create_hud(pl_name)
        fantasia.set_inventory(player)
        fantasia.start_storm_loop_sounds(pl_name)
    end

    wait_until_loaded(arena.storm_center_start,function()
        if arena and arena.in_game then
            local obj = fantasia.create_storm_circle(arena)
            if obj then
                for pl_name,_ in pairs(arena.players) do
                    local player = minetest.get_player_by_name(pl_name)
                    if player then
                        player:send_mapblock(get_blockpos(arena.storm_center_start))
                    end
                end
                arena.storm_loaded = true
                return true
            end
            return false
        end
    end)


end)





----------------------------------------------
---------------LOCAL FUNCTIONS----------------
----------------------------------------------

function try_to_load_stormspawn(arena)
    local min = vector.subtract(arena.storm_center_start,vector.new(16,16,16))
    local max = vector.add(arena.storm_center_start,vector.new(16,16,16))
    minetest.load_area(min, max)
    minetest.emerge_area(min, max)
end



function reset_meta(p_name)
    local p_meta = minetest.get_player_by_name(p_name):get_meta()

    p_meta:set_int("fr_surfer_jump", 0)
    p_meta:set_int("fr_surfer_dash", 0)
end





-- callback() the function to run when the block is loaded. Return bool success
function wait_until_loaded(pos,callback,iter)
    iter = iter or 0
    iter = iter + 1
    -- try for 2 minutes. After that, its not happening.
    if iter > 10*60*2 then
        minetest.log("error","[fantasia_royale] Forceloading the spawn location of 'fantasia_royale:ring' entity took longer than expected and was abandoned (so the game will not run). Check the 'max_forceloaded_blocks' setting in minetest.conf")
        return
    end
    if minetest.compare_block_status(pos, "active") == true then
        local success = callback()
        if not success then
            wait_until_loaded(pos,callback,iter)
        end
    else
        minetest.after(.1,function ()
            wait_until_loaded(pos,callback,iter)
        end)
    end
end



function create_hud(p_name)
    fantasia.HUD_log_create(p_name)
    fantasia.HUD_players_counter_create(p_name)
    fantasia.HUD_mana_create(p_name)
    fantasia.HUD_minimap_create(p_name)
end



local BLOCKSIZE = core.MAP_BLOCKSIZE
function get_blockpos(pos)
    return vector.apply(vector.copy(pos)/BLOCKSIZE,math.floor)
end
