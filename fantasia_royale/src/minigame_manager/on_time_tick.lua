fantasia.on_time_ticks = {}
fantasia.register_on_time_tick = function(name, func)
    fantasia.on_time_ticks[name] = func
end

arena_lib.on_time_tick("fantasia_royale", function(arena)
    arena.time_since_start = arena.time_since_start + 1
    if arena.storm_loaded then
        fantasia.handle_storm_phase_change(arena)
    else
        arena_lib.HUD_send_msg_all("title", arena, "Waiting for storm to form ...", 1, nil, 0xe6482e)
    end

    -- check if players are in the storm. If they are, damage them and warn them. Only use 2d flat distance
    for pl_name,_ in pairs(arena.players) do
        if arena.time_since_start > 30 then -- we don't want players getting damage before they drop off the initial vehicle!
            if fantasia.is_player_in_storm(arena,pl_name) then
                arena_lib.HUD_send_msg("title", pl_name, "You are outside the Storm's eye! Move!", 1, nil, 0xe6482e)
                fantasia.HUD_storm_overlay_create(pl_name)
                fantasia.storm_damage(pl_name, arena)
            else
                fantasia.HUD_storm_overlay_remove(pl_name)
            end
        end
    end

    -- kill height
    for pl_name,stats in pairs(arena.players) do
        local player = minetest.get_player_by_name(pl_name)
        if player:get_pos().y < arena.kill_level then
            arena_lib.remove_player_from_arena(pl_name, 1, "")
        end
    end

    -- storm sound layers
    fantasia.handle_stormsound_timetick(arena)

    -- on time tick functions
    for name, func in pairs(fantasia.on_time_ticks) do
        func(arena)
    end
end)