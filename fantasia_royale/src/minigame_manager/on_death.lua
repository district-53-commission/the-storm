local function custom_death() end

-- This minigame is supposed to be installed alongside the beds mod (on a survival
-- server), we cannot use arena_lib's on_death, and we cannot allow the player to
-- die. The following prevents death and calls a fake death function
minetest.register_on_player_hpchange(function(player, hp_change, reason)
    local p_name = player:get_player_name()

    if arena_lib.is_player_in_arena(p_name, 'fantasia_royale') then
        local arena = arena_lib.get_arena_by_player(p_name)
        local hp = player:get_hp()

        --exclude some unwanted possibilities ... dont modify hp_change if the
        --player is not playing

        --protect winners from damage
        if arena.in_celebration then return 0 end
        -- protect spectators from damage
        if arena_lib.is_player_spectating(p_name) then return 0 end

        -- if the player is about to die..
        if hp + hp_change <= 0 then
            -- ..call on_death and don't kill them
            custom_death(arena, p_name, reason)
            return 0
        else
            -- the hp_change wouldn't kill them, so allow the change.
            return hp_change
        end
    else
        return hp_change
    end
end, true)





function custom_death(arena, p_name, reason)
    local xc_name = reason.object and reason.object:get_player_name()
    arena_lib.remove_player_from_arena(p_name, 1, xc_name)
end