arena_lib.on_end("fantasia_royale", function(arena, winners, is_forced)
    for pl_name, pl_data in pairs(arena.players) do
        if pl_data.is_glider_open then
            fantasia.remove_glider_object(minetest.get_player_by_name(pl_name))
        end
    end

    for psp_name, _ in pairs(arena.players_and_spectators) do
        fantasia.hud_remove_all(psp_name)
        fantasia.restore_inventory(minetest.get_player_by_name(psp_name))
        fantasia.end_storm_loop_sounds(psp_name)
        classy.remove_player_class(psp_name)
    end

    -- get rid of the storm object and free the forceloaded central block
    if arena.storm_obj_ref then
        arena.storm_obj_ref:remove()
    end

    -- `arena` is a copy. If we empty the copy, the real arena will keep the userdata of the obj_ref
    local _, real_arena = arena_lib.get_arena_by_name("fantasia_royale", arena.name)

    -- minetest.forceload_free_block(arena.storm_center_start, true)
    real_arena.storm_obj_ref = nil
end)