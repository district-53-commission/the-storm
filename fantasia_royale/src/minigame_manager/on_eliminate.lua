-- called when a player is eliminated. The sample minigame doest really have a use for this,
-- but if you had customized code that required cleanup, this would one place to do it. Also see on_kick and on_quit
-- on_prequit and on_disconnect callbacks if you need to do cleanup (in the api docs)


-- arena_lib automatically sends a chat message to other players and to spectators if a player is eliminated
arena_lib.on_eliminate("fantasia_royale", function(arena, p_name, xc_name, p_properties)
    -- TODO: drop items in-world. Not needed for armors, as they're automatically dropped

    fantasia.HUD_log_update(arena, "froyale_log_death.png", xc_name, p_name)

    if xc_name and minetest.get_player_by_name(xc_name) then
        fantasia.add_xp(arena,xc_name,10)
        minetest.chat_send_player(xc_name,"You've eliminated p_name! +10 xp")
    end

    -- give xp to winner here, it lets the second-to-last player see the final leaderboard
    -- // Give XP to winner //
    if arena.players_amount == 1 then
        for winner_name, _ in pairs(arena.players) do
            fantasia.add_xp(arena,winner_name,50)
		    arena_lib.HUD_send_msg("title",winner_name,"You win! +50 xp!",3)
        end
    end
    -- // END Give XP to Winner //


    if p_properties.is_glider_open then
        local player = minetest.get_player_by_name(p_name)
        fantasia.remove_glider_object(player)
    end

    fantasia.HUD_players_counter_update(arena)
    fantasia.show_leaderboard(arena,p_name)
    fantasia.end_storm_loop_sounds(p_name)
    arena_lib.HUD_hide("broadcast", p_name)
    arena_lib.HUD_hide("title", p_name)
end)