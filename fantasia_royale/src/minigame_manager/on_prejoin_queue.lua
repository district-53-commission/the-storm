arena_lib.on_prejoin_queue("fantasia_royale", function(arena, p_name)
    if fantasia.settings.DEBUG_MODE and not fantasia.settings.are_unit_tests_over then
        fantasia.print_error(p_name, "Unit tests are not over yet, try later!")
        return
    end

    return true
end)