local S = minetest.get_translator("fantasia_royale")

local function calc_inv() end
local function calc_weapon() end

local old_invs = {}                       -- KEY: p_name; VAL: old inv fs



function fantasia.set_inventory(player)
    old_invs[player:get_player_name()] = player:get_inventory_formspec()

    local p_inv = player:get_inventory()

    p_inv:set_size("weap1_attachments", 1)
    p_inv:set_size("weap2_attachments", 1)
    player:set_inventory_formspec(calc_inv(player))
end



function fantasia.update_inventory(player)
    player:set_inventory_formspec(calc_inv(player))
end



function fantasia.restore_inventory(player)
    local p_inv = player:get_inventory()

    p_inv:set_size("weap1_attachments", 0)
    p_inv:set_size("weap2_attachments", 0)
    player:set_inventory_formspec(old_invs[player:get_player_name()])
end





----------------------------------------------
---------------LOCAL FUNCTIONS----------------
----------------------------------------------

function calc_inv(player)
    local p_name = player:get_player_name()
    local p_inv = player:get_inventory()
    local weap1_fs = calc_weapon(p_inv, 1, p_name)
    local weap2_fs = calc_weapon(p_inv, 2, p_name)

    local fs = {
        "formspec_version[4]",
        "size[21,13,true]",
        "no_prepend[]",
        "bgcolor[;true]",
        "style_type[list;size=1.1;spacing=0.2]",
        "style[craft_no;fgimg=button9_2.png;textcolor=#aaaaaa]",
        "image[0,0;21,13;froyale_inv_bg.png]",
        "container[0,0]",
        weap1_fs,
        "container_end[]",
        "container[11.5,0]",
        weap2_fs,
        "container_end[]",
        "style_type[list;size=1.2;spacing=0.4]",
        "list[detached:" .. p_name .. "_armor;armor;0.5,6.5;1.4,4;0]",
        "container[1.22,6.5]",
        "list[player:" .. p_name .. ";main;0.89,0;8,1.5;0]",
        "list[player:" .. p_name .. ";main;5.67,1.58;4.5,3.5;9]",
        "model[0.85,1.65;4.45,4.39;chara;character.b3d;" .. player:get_properties().textures[1] .. ";0,-150;false;true]",
        "container_end[]",
        -- TODO crafting area
        --[["container[15.25,6.6]",
        "container[0,0]",
        "box[0,0;5.16,1.2;#a0938e]",
        "image[0.1,0.15;0.85,0.85;default_book_written.png]",
        "image[1.1,0.1;0.5,0.5;icon_sceptre.png]",
        "hypertext[1.65,0.13;1,0.5;audio_settings;<global valign=middle size=15>x 2]",
        "image[2.1,0.1;0.5,0.5;icon_staff.png]",
        "hypertext[2.65,0.13;1,0.5;audio_settings;<global valign=middle size=15>x 2]",
        "image[1.1,0.62;0.5,0.5;icon_sword.png]",
        "hypertext[1.65,0.65;1,0.5;audio_settings;<global valign=middle size=15>x 1]",
        "image_button[3.22,0.15;1.75,0.85;button9_2_on24.png;craft;Craft]",
        "container_end[]",
        "container[0,1.2]",
        "box[0,0;5.16,1.2;#6b6162]",
        "image[0.1,0.15;0.85,0.85;bl_sword.png^[multiply:#aaaaaa]",
        "image[1.1,0.35;0.5,0.5;icon_sceptre.png^[multiply:#aaaaaa]",
        "hypertext[1.65,0.39;1,0.5;audio_settings;<global valign=middle size=15 color=#aaaaaa>x 3]",
        "image[2.1,0.35;0.5,0.5;icon_staff.png^[multiply:#aaaaaa]",
        "hypertext[2.65,0.39;1,0.5;audio_settings;<global valign=middle size=15 color=#aaaaaa>x 3]",
        "image_button[3.22,0.15;1.75,0.85;;craft_no;Craft]",
        "container_end[]",
        "container_end[]"]]
    }

    return table.concat(fs, "")
end



function calc_weapon(p_inv, id, p_name)
    local weap = p_inv:get_stack("main", id + 1) -- slot 2 & 3

    if weapons_lib.is_weapon(weap:get_name()) then
        return table.concat({
            "list[player:" .. p_name .. ";weap" .. id .. "_attachments;0.28,3.2;3,1.5;0]",
            "hypertext[0.24,2.3;4.5,0.6;weap;<global size=22><b>" .. weap:get_description() .. "</b>]",
            --"hypertext[0.24,2.75;4.5,0.6;audio_settings;<global size=18 color=#28ccdf><b>Skin name</b>]",
            "image[6.3,1.4;1.5,1.5;" .. weap:get_definition().inventory_image .. "]",
            --"image[0.31,3.25;1,1;froyale_inv_vel.png]", <- icon overlay test
        }, "")
    else
        return ""
    end
end