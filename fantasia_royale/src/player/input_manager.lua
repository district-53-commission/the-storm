-- skills
controls.register_on_press(function(player, key)
    if key == "zoom" then
        local p_name = player:get_player_name()

        if not arena_lib.is_player_playing(p_name, "fantasia_royale") then return end

        local arena = arena_lib.get_arena_by_player(p_name)

        if not arena.players[p_name].has_dropped_off then return end -- TODO @Zughy: check whether has landed instead (add a player_property?)

        local class = classy.get_class_by_name(fantasia.get_class(p_name))

        local skill = p_name:get_skill(class.starting_skills[1])
        if skill.loop_params then skill:start()
        else skill:cast() end
    end
end)