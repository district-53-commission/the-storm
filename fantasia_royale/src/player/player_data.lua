-- player data is populated inside `database_manager.lua`

local unlocked_classes = {}                             -- KEY: p_name; VAL: {c_name = true}
local equipped_class = {}                               -- KEY: p_name; VAL: c_name

local players_data = {}                                 -- KEY: p_name; VAL: p_data

local default_data = {
    class = "fantasia_royale:elara",
    unlocked_classes = {"fantasia_royale:elara", "fantasia_royale:sol_arca", "fantasia_royale:oakblade"}
    -- possibly more in the future. We can just run checks in init_player_data to add the new parameter
}



function fantasia.init_player_data(p_name)
    local p_data = fantasia.load_player(p_name)

    -- first access/data being reset
    if not p_data then
        p_data = table.copy(default_data)
        fantasia.save_player(p_name, p_data)
    end

    -- TODO checks in case some classes have been removed in the meanwhile

    players_data[p_name] = p_data
end



function fantasia.unlock_class(p_name, c_name)
    if not classy.is_class(c_name) then return end

    players_data[p_name].unlocked_classes[c_name] = true
    fantasia.save_player(p_name, players_data[p_name])
end



function fantasia.remove_class(p_name, c_name)
    if not classy.is_class(c_name) then return end

    players_data[p_name].unlocked_classes[c_name] = nil
    fantasia.save_player(p_name, players_data[p_name])
end





----------------------------------------------
-----------------GETTERS----------------------
----------------------------------------------

function fantasia.get_class(p_name)
    return players_data[p_name].class
end



function fantasia.get_unlocked_classes(p_name)
    return table.copy(players_data[p_name].unlocked_classes)
end





----------------------------------------------
-----------------SETTERS----------------------
----------------------------------------------

function fantasia.set_class(p_name, c_name)
    if not classy.is_class(c_name) then return end

    players_data[p_name].class = c_name
    fantasia.save_player(p_name, players_data[p_name])
end





----------------------------------------------
--------------------UTILS---------------------
----------------------------------------------

function fantasia.is_class_unlocked(p_name, c_name)
    if not players_data[p_name] then return end

    for _, cl_name in ipairs(players_data[p_name].unlocked_classes) do
        if c_name == cl_name then
            return true
        end
    end
end