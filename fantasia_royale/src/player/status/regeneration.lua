local S = minetest.get_translator("fantasia_royale")



skills.register_skill("fantasia_royale:regeneration", {
	name = S("Regeneration"),
	description = S("Regenerates the player over time."),

	loop_params = {
		cast_rate = 1
	},

	sounds = {
		bgm = {name = "froyale_healing", to_player = true}
	},

	hud = {{
			name = "heal",
			hud_elem_type = "image",
			text = "froyale_healing_hud.png",
			scale = {x=3, y=3},
			position = {x=0.5, y=0.75},
	}},

	_stop_job = nil,

	on_start = function(self, hp_per_tick, duration)
		if duration then
			if self._stop_job then self._stop_job:cancel() end
			self._stop_job = minetest.after(duration, function() self:stop() end)
		end
	end,

	cast = function(self, hp_per_tick)
		self.player:set_hp(self.player:get_hp() + hp_per_tick)
	end,

	on_stop = function(self)
		self._stop_job = nil
	end
})