----------------------------------------------
-----------------WORLD FOLDER-----------------
----------------------------------------------
local function load_world_folder()
    local wrld_dir = minetest.get_worldpath() .. "/fantasia_royale"
    local content = minetest.get_dir_list(wrld_dir)

    if not next(content) then
        local modpath = minetest.get_modpath("fantasia_royale")
        local src_dir = modpath .. "/IGNOREME"

        minetest.cpdir(src_dir, wrld_dir)
        os.remove(wrld_dir .. "/README.md")
    end
end

load_world_folder()





----------------------------------------------
-------------------SETTINGS-------------------
----------------------------------------------
dofile(minetest.get_worldpath() .. "/fantasia_royale/SETTINGS.lua")


-- TODO Giov4 @Zughy: instead of add_new_parameter, first import the default settings.lua file
-- and then import the custom one, so that the latter's missing properties are imported from the 
-- first one

-- useful to add new parameters, so that we don't have to manually update the world
-- folder when new parameters are added. Rememember to update the IGNOREME equivalent though
local function add_new_parameter(name, desc)
    if not fantasia.settings[name] then
        local settings = io.open(minetest.get_worldpath() .. "/fantasia_royale/SETTINGS.lua", "r")
        local txt = settings:read("*all") .. "\n" .. table.concat(desc, "\n")

        settings:close()
        minetest.safe_file_write(minetest.get_worldpath() .. "/fantasia_royale/SETTINGS.lua", txt)

        minetest.log("action", "[FANTASIA ROYALE] Settings: `fantasia.settings. " .. name .. "` was missing. Successfully added at the end of the file")
    end
end

--[[add_new_parameter("name", {
    "--Explain the param",
    "--Yadda yadda",
    "fantasia.settings.name = <default_value>"
})]]





----------------------------------------------
--------------------TWEAKS--------------------
----------------------------------------------

-- we have to increase the active_object_send_range_blocks in order to let the
-- storm ring be visible from the entire map. Unfortunately, this can only be
-- done at run time. Also unfortunately, this affects the entire server, so
-- objects will be visible at this distance even in distance mapblocks which may
-- have sideeffects of lag and visual glitches. There's not much I can do about
-- that. The only remedy would be to use smaller arenas.
minetest.settings:set("active_object_send_range_blocks",tostring(fantasia.MAX_STORM_STARTING_RADIUS / 16 + 1))