local storage = minetest.get_mod_storage()



function fantasia.load_player(p_name)
	return minetest.deserialize(storage:get_string(p_name))
end



function fantasia.save_player(p_name, p_data)
	storage:set_string(p_name, minetest.serialize(p_data))
end



function fantasia.load_table(metadata_name)
	local metadata = minetest.deserialize(storage:get_string(metadata_name))

	if metadata == "" or metadata == nil then
		metadata = {}
	end

	return metadata
end





function fantasia.store_table(metadata_name, table)
	storage:set_string(metadata_name, minetest.serialize(table))
end

function fantasia.load_int(metadata_name)
	return storage:get_int(metadata_name)
end

function fantasia.store_int(metadata_name, value)
	storage:set_int(metadata_name, value)
end

function fantasia.remove_storage(metadata_name)
	storage:set_string(metadata_name, "")
end

-- Utility function to check if a storage key is set
function fantasia.is_storage_key_set(key)
    return storage:get_string(key) ~= ""
end

local json_file_path = minetest.get_worldpath() .. "/fantasia_royale/leaderboards"
minetest.mkdir(json_file_path)

function fantasia.store_json(metadata_name, table)
	minetest.safe_file_write(json_file_path.."/"..metadata_name..".json", minetest.write_json(table,true))
end