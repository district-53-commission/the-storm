-- Function to save the heightmap of an arena
fantasia.save_heightmap = function(arena)

    -- delete the hightmap if it already exists
    fantasia.delete_heightmap(arena.name)

    if not arena.pos1 or not arena.pos2 then
        return "Cannot save heightmap, arena region is not set!"
    end

    local min_pos = arena.pos1
    local max_pos = arena.pos2

    -- Save min_pos and max_pos
    local position_reference = {
        min_pos = min_pos,
        max_pos = max_pos
    }

    fantasia.store_table("arena_position_reference_" .. arena.name, position_reference)

    -- Get the content ID of air
    local air_cid = minetest.get_content_id("air")

    -- Define chunk size (adjust based on your server's memory capacity)
    local chunk_size = 16

    -- Function to process a chunk
    local function process_chunk(minp, maxp)
        local vm = minetest.get_voxel_manip()
        local e1, e2 = vm:read_from_map(minp, maxp)
        local area = VoxelArea:new({MinEdge = e1, MaxEdge = e2})
        local data = vm:get_data()

        for x = minp.x, maxp.x do
            for z = minp.z, maxp.z do
                local found_non_air = false
                for y = maxp.y, minp.y, -1 do
                    local index = area:index(x, y, z)
                    local node_id = data[index]
                    if node_id ~= air_cid then
                        -- Check if the position is within the arena bounds
                        if x >= min_pos.x and x <= max_pos.x and z >= min_pos.z and z <= max_pos.z then
                            local heightmap_name = "heightmap_" .. arena.name .. "_" .. x .. "_" .. z
                            fantasia.store_int(heightmap_name, y)
                            found_non_air = true
                        end
                        break
                    end
                end
                if not found_non_air then
                    local heightmap_name = "heightmap_" .. arena.name .. "_" .. x .. "_" .. z
                    fantasia.store_int(heightmap_name, -35000)
                end
            end
        end
    end

    -- Process the arena in chunks
    for x = min_pos.x, max_pos.x, chunk_size do
        for z = min_pos.z, max_pos.z, chunk_size do
            local chunk_minp = {x = x, y = min_pos.y, z = z}
            local chunk_maxp = {
                x = math.min(x + chunk_size - 1, max_pos.x),
                y = max_pos.y,
                z = math.min(z + chunk_size - 1, max_pos.z)
            }
            process_chunk(chunk_minp, chunk_maxp)
        end
    end

end

-- Function to delete the heightmap of an arena
fantasia.delete_heightmap = function(arena_name)
    -- Load the position reference table
    local position_reference = fantasia.load_table("arena_position_reference_" .. arena_name)
    if not position_reference or not position_reference.min_pos or not position_reference.max_pos then
        return "No heightmap found for arena " .. arena_name
    end

    local min_pos = position_reference.min_pos
    local max_pos = position_reference.max_pos

    -- Iterate through the positions and remove the storage entries
    for x = min_pos.x, max_pos.x do
        for z = min_pos.z, max_pos.z do
            local heightmap_name = "heightmap_" .. arena_name .. "_" .. x .. "_" .. z
            fantasia.remove_storage(heightmap_name)
        end
    end

    -- Remove the position reference table
    fantasia.remove_storage("arena_position_reference_" .. arena_name)

    return
end

-- can return -35000 if a height is not found (because there is only air)
fantasia.get_height = function(arena_name_or_arena, x, z)
    local arena_name
    if type(arena_name_or_arena) ~= "string" then
        arena_name = arena_name_or_arena.name
    else
        arena_name = arena_name_or_arena
    end
    local heightmap_name = "heightmap_" .. arena_name .. "_" .. x .. "_" .. z

    local height = fantasia.load_int(heightmap_name)

    -- Check if the height value exists in the storage
    -- Assuming fantasia.load_int returns 0 if the value is not set, we should assert existence differently.
    assert(height ~= 0 or (height == 0 and fantasia.is_storage_key_set(heightmap_name)), "Height for (" .. x .. ", " .. z .. ") not found! Ensure the heightmap is saved in the arena editor.")

    return height
end
