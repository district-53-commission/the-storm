--[[
	This manages the players' mana and their HUD.

	Each player has two associated statistics:
	- mana (number, 0 to 10): corresponds to the mana bar,
	  has a minimum value of 0 and a maximum value of 10;
	- reserve (number, >= 0): is the mana reserve added by the ether vials,
	  when the mana recharges it will consume this.
]]

local function clamp(v, min, max) end
local function get_stats(name) end

fantasia.MAX_MANA = 10

local MAX_MANA = fantasia.MAX_MANA
local SECONDS_TO_REFILL_MANA = 1

local string_metatable = getmetatable("")



function fantasia.add_mana(p_name, value)
	if not arena_lib.is_player_playing(p_name, "fantasia_royale") then
		return false
	end

	local stats = get_stats(p_name)
	local leftover = math.max(0, value - (MAX_MANA - stats.mana))

	stats.mana = clamp(stats.mana + value, 0, MAX_MANA)

	fantasia.HUD_mana_update(p_name)

	return leftover
end
string_metatable.__index["add_mana"] = fantasia.add_mana



function fantasia.deplete_mana(p_name, value)
	if not arena_lib.is_player_playing(p_name, "fantasia_royale") then
		return false
	end

	local stats = get_stats(p_name)
	local depleted_amount = math.min(value, stats.mana)

	stats.mana = stats.mana - depleted_amount

	fantasia.HUD_mana_update(p_name)

	return depleted_amount
end
string_metatable.__index["deplete_mana"] = fantasia.deplete_mana



function fantasia.deplete_mana_reserve(p_name, value)
	if not arena_lib.is_player_playing(p_name, "fantasia_royale") then
		return false
	end

	local stats = get_stats(p_name)
	local depleted_amount = math.min(value, stats.mana_reserve)

	stats.mana_reserve = stats.mana_reserve - depleted_amount

	fantasia.HUD_mana_update(p_name)

	return depleted_amount
end
string_metatable.__index["deplete_mana_reserve"] = fantasia.deplete_mana_reserve



function fantasia.add_mana_reserve(p_name, value)
	p_name:deplete_mana_reserve(-value)
end
string_metatable.__index["add_mana_reserve"] = fantasia.add_mana_reserve



-- Tries to consume the reserve first, and if it's empty then tries with the normal mana.
-- Returns false if the player hasn't enough mana + mana_reserve
function fantasia.use_mana(p_name, value)
	if not arena_lib.is_player_playing(p_name, "fantasia_royale") or p_name:get_mana() + p_name:get_mana_reserve() < value then
		return false
	elseif p_name:get_mana() < value  then
		p_name:recharge_mana()
	end

	p_name:deplete_mana(value)

	-- if they have equal or less then a mana bar remaining put the reserve on the bar
	if (p_name:get_mana() + p_name:get_mana_reserve()) <= fantasia.MAX_MANA then
		p_name:recharge_mana()
	end

	return true
end
string_metatable.__index["use_mana"] = fantasia.use_mana



function fantasia.recharge_mana(p_name)
	if not arena_lib.is_player_playing(p_name, "fantasia_royale") then
		return false
	end

	local stats = get_stats(p_name)

	if stats.mana < MAX_MANA and stats.mana_reserve > 0 then
		local mana_amount_per_second = MAX_MANA / SECONDS_TO_REFILL_MANA
		local depleted_from_mana_reserve = p_name:deplete_mana_reserve(mana_amount_per_second)
		local leftover = p_name:add_mana(depleted_from_mana_reserve)
		p_name:add_mana_reserve(leftover)
	end

	-- recharge over time disabled for now
	--minetest.after(1, fantasia.recharge_mana, p_name)
end
string_metatable.__index["recharge_mana"] = fantasia.recharge_mana





----------------------------------------------
-----------------GETTERS----------------------
----------------------------------------------

function fantasia.get_mana(p_name)
	return get_stats(p_name).mana
end
string_metatable.__index["get_mana"] = fantasia.get_mana



function fantasia.get_mana_reserve(p_name)
	return get_stats(p_name).mana_reserve
end
string_metatable.__index["get_mana_reserve"] = fantasia.get_mana_reserve





----------------------------------------------
--------------------UTILS---------------------
----------------------------------------------

function fantasia.has_enough_mana(p_name, amount)
	local arena = arena_lib.get_arena_by_player(p_name)

	if not arena then return end

	local p_data = arena.players[p_name]

	return p_data.mana + p_data.mana_reserve >= amount
end
string_metatable.__index["has_enough_mana"] = fantasia.has_enough_mana




----------------------------------------------
---------------LOCAL FUNCTIONS----------------
----------------------------------------------

function clamp(value, min, max)
	return math.max(min, math.min(value, max))
end



function get_stats(name)
	return arena_lib.get_arena_by_player(name).players[name]
end