-- ### Storm Damage ###

-- removes half a heart from the player
-- pass a player name
function fantasia.storm_damage(p_name, arena)
    local damage = 1 + math.floor(arena.rounds_complete/3)
    local player = minetest.get_player_by_name(p_name)
    if not player then return end
    local p_hp = player:get_hp()
    p_hp = p_hp - damage
    player:set_hp(p_hp)
end


-- returns true if player p_name is outside the storm ring
function fantasia.is_player_in_storm(arena,p_name)
    local player = minetest.get_player_by_name(p_name)
    local pos = player:get_pos()

    return fantasia.is_pos_in_storm(arena, pos)
end


function fantasia.is_pos_in_storm(arena, pos)
    pos.y=0
    local normal_center_pos = vector.copy(arena.storm_center_start)
    normal_center_pos.y=0
    local storm_offset = arena.storm_offset
    storm_offset.y = 0
    local actual_center_pos = vector.add(normal_center_pos,storm_offset)
    local pl_dist = vector.distance(pos,actual_center_pos)
    if pl_dist > arena.storm_radius then
        return true
    else
        return false
    end
end