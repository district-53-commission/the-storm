
local player_sound_loop_handles = {}
local function fade_loop_sounds() end


function fantasia.start_storm_loop_sounds(p_name)
    player_sound_loop_handles[p_name] = {}
    player_sound_loop_handles[p_name][1] = minetest.sound_play("froyale_rainloop", {
        gain = .001,
        to_player = p_name,
        loop = true,
    })
    player_sound_loop_handles[p_name][2] = minetest.sound_play("froyale_windloop", {
        gain = .001,
        to_player = p_name,
        loop = true,
    })
end

function fantasia.end_storm_loop_sounds(p_name)
    if player_sound_loop_handles[p_name] then
        minetest.sound_stop(player_sound_loop_handles[p_name][1])
        minetest.sound_stop(player_sound_loop_handles[p_name][2])
    end
end

-- the globalstep is a function that plays storm sound layers from locations inside the storm
function fantasia.handle_stormsound_globalstep(arena)
    for pl_name, stats in pairs(arena.players) do
        local player = minetest.get_player_by_name(pl_name)
        if player then
            -- first take a chance to spawn a sound
            if math.random(1,100) == 1 then
                -- then choose a location above the player and in a box around the player
                local pl_pos = player:get_pos()
                local sp_pos = vector.add(pl_pos, vector.new(math.random(-30,30),math.random(0,20), math.random(-30,30)))
                -- check if the location is outside the ring
                if vector.distance(arena.storm_center, sp_pos) > arena.storm_radius then
                    -- if it is outside play the sound at that location
                    minetest.sound_play("froyale_stormsound", {
                        pos = sp_pos,
                        to_player = pl_name,
                        max_hear_distance = 40,
                        loop = false,
                    })
                end
            end
        end
    end
end

local e_const = 2.71828
-- the time tick is a function that fades the storm loop based on proximity to the storm
function fantasia.handle_stormsound_timetick(arena)
    for pl_name, stats in pairs(arena.players) do
        local player = minetest.get_player_by_name(pl_name)
        if player then
            -- update loop sound loudness based on proximity to the storm.
            local pl_pos = player:get_pos()
            local dist_to_center = vector.distance(arena.storm_center, pl_pos)
            local dist_factor = dist_to_center/arena.storm_radius
            local loudness_curve = 1/(1+(e_const^(-35.4*(dist_factor-.88)))) --https://www.desmos.com/calculator/y5g7bvxbwn
            fade_loop_sounds(pl_name,1,loudness_curve)
        end
    end
end



function fade_loop_sounds(p_name,step,gain)
    if player_sound_loop_handles[p_name] then
        for i,handle in ipairs(player_sound_loop_handles[p_name]) do
            minetest.sound_fade(handle,step,gain)
        end
    end
end
