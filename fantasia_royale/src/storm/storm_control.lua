
-- ### Create Storm Circle ###

-- does what it says. Creates a storm circle for the game.

function fantasia.create_storm_circle(arena)
    local center = vector.copy(arena.storm_center_start)
    local obj = minetest.add_entity(center, "fantasia_royale:ring")
    if obj then
        return obj
    end
end

-- wrapper for the update properties function of the storm object. Re-makes the
-- storm object if its been randomly deleted, which Minetest seems to enjoy
-- doing

function fantasia.update_storm_properties(arena)
    local obj = arena.storm_obj_ref
    if not obj then
        obj = fantasia.create_storm_circle(arena)
        arena.storm_obj_ref = obj
    end
    local ent = obj:get_luaentity()
    ent:_update_properties(arena)
    arena.storm_center = vector.add(arena.storm_center_start,arena.storm_offset)
    if arena.rounds_complete >= 1 then
        arena.next_center = vector.add(arena.storm_center_start,arena.next_offset)
        arena.next_center2 = vector.add(arena.storm_center_start,arena.next_offset2)
    end
end

-- ## Storm Phase change ###

-- Handles changing the storm phase. Called every second. If its time: Halts the
-- storm if moving, moves the storm if stopped. Predicts the next storm phase.
-- Resets the storm timer.

function fantasia.handle_storm_phase_change(arena)
    arena.round_timer = arena.round_timer - 1

    -- create a storm shrinking hud for all players at T-60, and remove it at T0
    if arena.round_timer <= 60 then
        fantasia.HUD_ring_shrink_timer_create_update_all(arena)
    end
    if arena.round_timer == 0 then
        fantasia.HUD_ring_shrink_remove_all(arena)
    end




    if arena.round_timer == 0 then

        -- microsecond timestamp when the round started
        arena.round_start_timestamp = minetest.get_us_time()
        arena.round_start_offset = arena.storm_offset
        arena.round_start_size = arena.storm_radius


        if arena.rounds_complete == 0 then
            arena.next_offset2,arena.next_radius2 = fantasia.choose_next_offset_and_radius(arena,vector.zero(),arena.starting_storm_radius)
        end

        arena.next_radius = arena.next_radius2
        arena.next_offset = arena.next_offset2

        arena.next_offset2,arena.next_radius2 = fantasia.choose_next_offset_and_radius(arena,arena.next_offset2,arena.next_radius2)

        arena.rounds_complete = arena.rounds_complete + 1
        arena.round_timer = fantasia.choose_next_round_time(arena.rounds_complete) -- the time until the storm moves again

        arena.stop_time = math.floor(arena.round_timer/2) -- the time in which the storm will move

        -- handle dropboxes now
        if arena.dropbox_rounds_left > 0 then
            arena.dropbox_rounds_left = arena.dropbox_rounds_left - 1
            fantasia.choose_dropbox_spawns(arena)
            fantasia.place_dropboxes(arena)
        end

    end

end

-- pass centers are pos, radii are numbers. returns next center and next radius
function fantasia.choose_next_offset_and_radius(arena, current_offset, current_radius)
    local next_radius, next_offset, max_dist, dir_offset, vec_offset
    -- choose a new radius between .55 and .95 times as large as the current radius
    next_radius = current_radius * ((math.random() * .4) + .55)
    -- the max distance to travel is the difference of the old and new radii
    max_dist = (current_radius - next_radius)

    -- let the walls only shrink for the first 6 rounds. Then, the whole circle can move more, and faster after 8 rounds.
    if arena.rounds_complete <= 6 then
        max_dist = max_dist/2
    elseif arena.rounds_complete <=8 then
        max_dist = max_dist
    else
        max_dist = max_dist*2
    end

    local dist = max_dist*math.random()

    -- choose a random direction (radians)
    dir_offset = math.random() * math.pi * 2
    -- get a random 2d direction vector
    vec_offset = vector.new(dist*math.cos(dir_offset),0,dist*math.sin(dir_offset))
    -- add the vector to the original offset to get the new offset
    next_offset = current_offset + vec_offset


    return next_offset, next_radius
end

-- calculates the next round time based on the rounds complete

function fantasia.choose_next_round_time(rounds_complete)
    if rounds_complete <=2 then
        return 2*60
    elseif rounds_complete <= 6 then
        return 1.5*60
    elseif rounds_complete <= 8 then
        return 60
    elseif rounds_complete <= 10 then
        return 45
    else
        return 30
    end
end