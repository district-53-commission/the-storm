local S = minetest.get_translator("fantasia_royale")

local function override_size() end



weapons_lib.register_weapon("fantasia_royale:firestaff", {
  description = S("Fire staff"),
  groups = {["fantasia_royale:attchmt_projsize"] = 1},

  wield_scale = {x=1.3, y=1.3, z=1.3},
  wield_image = "froyale_weap_firestaff_icon.png",
  inventory_image = "froyale_weap_firestaff_icon.png",
  crosshair = "weaponslib_test1_crosshair.png", -- TODO

  weapon_type = "ranged",
  magazine = -1,

  can_use_weapon = function(player, action)
    return fantasia.util_weapon_use_check(player:get_player_name(), action)
  end,

  action1 = {
    type = "bullet",
    damage = 6,
    _mana_cost = 3,
    delay = 0.4,
    sound = "froyale_weapon_firestaff_attack",

    decrease_damage_with_distance = false,
    continuous_fire = true,

    bullet = {
      name = "fantasia_royale:fireball",

      mesh = "weaponslib_test_bullet.obj",
      textures = {"weaponslib_test1_trail2.png"},
      visual_size = {x=1, y=1, z=1},
      collisionbox = {-0.1, -0.1, -0.1, 0.1, 0.1, 0.1},

      speed = 30,
      lifetime = 2.2,

      explosion = { -- TODO: (weapons_lib comments, ignore) si può portare fuori, x tutte le armi (es. Cannonade); motivo per cui explode_on_contact rimane fuori, solo x proiettili
        range = 3,
        damage = 6,
        texture = "weaponslib_test1_trail2.png",
      },

      remove_on_contact = true,
      gravity = false,

      trail = {
        image = "froyale_weap_crossbow_trail.png",
        life = 1,
        size = 2,
        interval = 5, -- in step
        amount = 20,
      },
    },

    on_use = function(player, weapon, action)
      fantasia.use_mana(player:get_player_name(), action._mana_cost)
    end
  },
})