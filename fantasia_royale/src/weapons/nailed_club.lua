local S = minetest.get_translator("fantasia_royale")
local dmg1 = 6

weapons_lib.register_weapon("fantasia_royale:nailedclub", {
  description = S("Nailed club"),
  groups = {["fantasia_royale:attchmt_improved_hilt"] = 1},

  wield_scale = {x=1.3, y=1.3, z=1.3},
  wield_image = "froyale_weap_nailedclub_icon.png",
  inventory_image = "froyale_weap_nailedclub_icon.png",
  crosshair = "weaponslib_test2_crosshair.png", -- TODO, using weapons_lib test image

  weapon_type = "melee",

  can_use_weapon = function(player, action)
    return fantasia.util_weapon_use_check(player:get_player_name(), action)
  end,

  action1 = {
    type = "punch",
    damage = dmg1,
    delay = 1,
    sound = "weaponslib_test2_attack",
    -- physics_override = {speed = * 0.8} -- TODO: understand how to implement such physics modifiers
   },
})