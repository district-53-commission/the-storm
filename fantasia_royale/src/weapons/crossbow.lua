local S = minetest.get_translator("fantasia_royale")
local dmg = 8

weapons_lib.register_weapon("fantasia_royale:crossbow", {
  description = S("Enchanted crossbow"),
  groups = {["fantasia_royale:attchmt_zoom"] = 1},

  wield_scale = {x=1.3, y=1.3, z=1.3},
  wield_image = "froyale_weap_crossbow_icon.png",
  inventory_image = "froyale_weap_crossbow_icon.png",
  crosshair = "froyale_weap_crossbow_crosshair.png",

  weapon_type = "ranged",
  magazine = 1,
  reload_time = 1,

  can_use_weapon = function(player, action)
    return fantasia.util_weapon_use_check(player:get_player_name(), action)
  end,

  -- TODO: it should be a charged action on release but we need https://github.com/minetest/minetest/issues/13581
  action1 = {
    type = "raycast",
    damage = dmg,
    _mana_cost = 3,
    range = 300,
    delay = 0.9,
    --load_time = 2,

    --attack_on_release = true, -- TODO: ^
    pierce = true,

    sound = "froyale_weapon_crossbow_attack",
    trail = {
      image = "froyale_weap_crossbow_trail.png",
      amount = 20,
      size = 5,
    },

    on_use = function(player, weapon, action)
      fantasia.use_mana(player:get_player_name(), action._mana_cost)
    end
  },

  action2 = {
    type = "zoom",
    description = S("zoom"),
    fov = 25,
  }
})
