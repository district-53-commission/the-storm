local S = minetest.get_translator("fantasia_royale")



function fantasia.util_weapon_use_check(p_name, action)
    if not arena_lib.is_player_playing(p_name, "fantasia_royale") then return true end

    if action and action._mana_cost then
        if not fantasia.has_enough_mana(p_name, action._mana_cost) then
            local panel = panel_lib.get_panel(p_name, "froyale_mana")

            minetest.sound_play("froyale_mana_deny", {to_player = p_name, pitch = 1.15})
            panel:update({bg = "froyale_hud_mana_deny.png"})

            -- flash
            minetest.after(0.1, function()
                if not minetest.get_player_by_name(p_name) or not arena_lib.is_player_playing(p_name) or p_name:get_mana_reserve() > 0 then return end
                panel:update({bg = "froyale_hud_mana_empty.png"})

                minetest.after(0.1, function()
                    if not minetest.get_player_by_name(p_name) or not arena_lib.is_player_playing(p_name) or p_name:get_mana_reserve() > 0 then return end
                    panel:update({bg = "froyale_hud_mana_deny.png"})

                    minetest.after(0.1, function()
                        if not minetest.get_player_by_name(p_name) or not arena_lib.is_player_playing(p_name) or p_name:get_mana_reserve() > 0 then return end
                        panel:update({bg = "froyale_hud_mana_empty.png"})
                    end)
                end)
            end)

            return
        end
    end

    return true
end