minetest.register_tool("fantasia_royale:attchmt_zoom", {
    description = "[Attachment] Gunsight 2x\n\nZoom +20%",
    groups = {weapon_attachment = 1},
    inventory_image = "froyale_attchmt_zoom.png",

    _on_equip = function(inv, weapon, slot)
        local weap_def = weapon:get_definition()

        weapon:get_meta():set_float("action2@fov", weap_def.action2.fov * 0.80) -- TODO @Zughy: explain it in the item description (+20% zoom)
        inv:set_stack("main", slot, weapon)
    end,

    _on_remove = function(inv, weapon, slot)
        weapon:get_meta():set_string("action2@fov", "")
        inv:set_stack("main", slot, weapon)
    end
})



minetest.register_tool("fantasia_royale:attchmt_projsize", {
    description = "[Attachment] Bigger projectiles\n\nProjectiles 100% bigger",
    groups = {weapon_attachment = 1},
    inventory_image = "froyale_attchmt_projsize.png",

    _on_equip = function(inv, weapon, slot)
        weapon:get_meta():set_float("action1@bullet@size", 2)
        inv:set_stack("main", slot, weapon)
    end,

    _on_remove = function(inv, weapon, slot)
        weapon:get_meta():set_string("action1@bullet@size", "")
        inv:set_stack("main", slot, weapon)
    end
})



minetest.register_tool("fantasia_royale:attchmt_incendyo", {
    description = "[Attachment] Incendyo",
    groups = {weapon_attachment = 1},
    inventory_image = "froyale_attchmt_incendyo.png"
})



minetest.register_tool("fantasia_royale:attchmt_hawkeye", {
    description = "[Attachment] Hawkeye",
    groups = {weapon_attachment = 1},
    inventory_image = "froyale_attchmt_hawkeye.png"
})