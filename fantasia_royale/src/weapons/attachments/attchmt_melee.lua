minetest.register_tool("fantasia_royale:attchmt_improved_hilt", {
    description = "[Attachment] Improved Hilt\n\nAttack 20% faster",
    groups = {weapon_attachment = 1},
    inventory_image = "froyale_attchmt_improvedhilt.png",

    _on_equip = function(inv, weapon, slot)
        local weap_def = weapon:get_definition()
        local w_meta = weapon:get_meta()

        -- in case there are other factors already changing the delay
        if w_meta:get("action1@delay") then
            w_meta:set_float("action1@delay", tonumber(w_meta:get("action1@delay")) * 0.80)
        else
            w_meta:set_float("action1@delay", weap_def.action1.delay * 0.80) -- TODO @Zughy: explain it in the item description (-20% delay)
        end

        inv:set_stack("main", slot, weapon)
    end,

    _on_remove = function(inv, weapon, slot)
        weapon:get_meta():set_string("action1@delay", "")
        inv:set_stack("main", slot, weapon)
    end
})

minetest.register_tool("fantasia_royale:attchmt_sharpness", {
    description = "[Attachment] Sharpness",
    groups = {weapon_attachment = 1},
    inventory_image = "froyale_attchmt_sharpness.png"
})