minetest.register_on_joinplayer(function(player, last_login)
    fantasia.init_player_data(player:get_player_name())
end)



minetest.register_on_player_hpchange(function(player, hp_change, reason)
    local p_name = player:get_player_name()
    if not arena_lib.is_player_spectating(p_name, "fantasia_royale") then return hp_change end

    local arena = arena_lib.get_arena_by_player(p_name)

    -- can't get damage whilst on the initial vehicle...  ... check for existence of player in arena.players because this is an engine callback and the player could have been just eliminated, which causes a nil crash when arena.players[p_name] = nil

    if not(arena.players[p_name]) then
        return hp_change -- for when the player has just got eliminated; don't mess up the hp_change of resetting the player after the match
    elseif not arena.players[p_name].has_dropped_off then
        return 0
    end

    -- ..nor inflict it
    if reason.type == "punch" then
        local puncher = reason.object
        if puncher:is_player() and not arena.players[puncher:get_player_name()].has_dropped_off then return 0 end
    end

    return hp_change
end, true)