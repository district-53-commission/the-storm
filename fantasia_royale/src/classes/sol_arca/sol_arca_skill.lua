local S = minetest.get_translator("fantasia_royale")



skills.register_skill("fantasia_royale:companion", {
	name = S("Companion"),
	description = S("Casts a healing orb that heals you or your teammates over time. Lasts 10s"),
	_icon = "froyale_skills_companion_icon.png",
	cooldown = 25,

	loop_params = {
		duration = 10,
		cast_rate = 1
	},

	_orb = nil,

	on_start = function(self)
		local spawn_pos = vector.add(self.player:get_pos(), vector.new(0, 2.5, 0))
		self._orb = self:add_entity(spawn_pos, "fantasia_royale:companion")
	end,
})
