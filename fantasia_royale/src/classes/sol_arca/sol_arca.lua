local folder = fantasia.srcpath .. "/classes/sol_arca/"
dofile(folder .. "sol_arca_entity.lua")
dofile(folder .. "sol_arca_skill.lua")



local S = minetest.get_translator("fantasia_royale")



classy.register_class("fantasia_royale:sol_arca", {
    name = S("Sol Arca"),
    description = S("Healer"),
    starting_skills = {"fantasia_royale:companion"},
    _splash_art = "froyale_classes_solarca_splash.png",
    _thumbnail = "froyale_classes_solarca_thumbnail.png"
})