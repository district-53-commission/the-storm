local companion = {
	initial_properties = {
		 hp_max = 999,
		 physical = false,
		 visual_size = {x = 0.55, y = 0.55},
		 textures = {"froyale_skills_companion_icon.png"},
		 pointable = false,
		 visual = "sprite",
		 glow = 1,
	},
	_speed = 5,
	_max_distance = 10,
	_radius = 6,
	_hp_per_second = 1
}


local function vec_interpolate(v1, v2, factor)
	return {
			x = v1.x + (v2.x - v1.x) * factor,
			y = v1.y + (v2.y - v1.y) * factor,
			z = v1.z + (v2.z - v1.z) * factor
	}
end


function companion:on_step(dtime_s)
	local target_pos = vector.add(self.player:get_pos(), vector.new(0, 2.5, 0))
	local dist_from_pl = vector.distance(target_pos, self.object:get_pos())

	-- MOVEMENT
	if dist_from_pl > 0.15 and dist_from_pl < self._max_distance then
		local dir_to_pl = vector.direction(self.object:get_pos(), target_pos)
		local desired_velocity = dir_to_pl * self._speed
		local actual_velocity = self.object:get_velocity()
		self.object:set_velocity(vec_interpolate(actual_velocity, desired_velocity, dtime_s * 3))
	else
		local actual_velocity = self.object:get_velocity()
		self.object:set_velocity(vec_interpolate(actual_velocity, {x = 0, y = 0, z = 0}, dtime_s * 3))
	end

	-- HEALING
	local entities = minetest.get_objects_inside_radius(self.object:get_pos(), self._radius)

	for _, entity in ipairs(entities) do
		if entity.get_luaentity and entity:is_player() then
			local p_name = entity:get_player_name()
			local arena = arena_lib.get_arena_by_player(p_name)

			if p_name == self.pl_name or arena_lib.is_player_in_same_team(arena, self.pl_name, p_name) then
				entity:get_player_name():unlock_skill("fantasia_royale:regeneration")
				entity:get_player_name():start_skill("fantasia_royale:regeneration", self._hp_per_second, 2)
			end
		end
	end
end



skills.register_expiring_entity("fantasia_royale:companion", companion)
