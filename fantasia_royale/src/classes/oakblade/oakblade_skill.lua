local function find_teleport_pos(pos, pl_pos) end
local S = minetest.get_translator("fantasia_royale")



skills.register_skill("fantasia_royale:void_leap", {
	name = S("Void leap"),
	description = S("Teleport forwards becoming invisible for 5 seconds"),
	_icon = "froyale_skills_voidleap_icon.png",
	cooldown = 25,

	loop_params = {
		duration = 5,
		cast_rate = 1
	},

	sounds = {
		start = {name="froyale_oakblade_teleport", object = false, max_hear_distance = 8},
		stop = {name="froyale_oakblade_appear", object = false, max_hear_distance = 8}
	},

	_pl_properties_backup = {},
	_old_fov = 1,
	_old_saturation = 1,
	_fov = 1.5,
	_saturation = 0.3,

	teleport = function (self)
		local pl_pos = self.player:get_pos()
		local particles = {
			amount = 60,
			time = 0.1,
			pos = {
				min = pl_pos+vector.new(0.8,2,0.8), max = pl_pos+vector.new(-0.8,0,-0.8)
			},
			attract = {
				kind = "point",
				strength = 3,
				origin = pl_pos+vector.new(0,1,0),
				die_on_contact = true
			},
			minsize = 2,
			maxsize = 8,
			glow = 12,
			texture = {
				name = "froyale_oakblade_leap_particle.png",
				alpha_tween = {1, 0.15},
				scale_tween = {
					{x = 1, y = 1},
					{x = 0, y = 0},
				}
			}
		}

		local ray = fantasia.look_raycast(self.player, 30, false, true)
		for pointed_thing in ray do
			if pointed_thing.type == "node" then
				self.player:set_pos(find_teleport_pos(pointed_thing.above, self.player:get_pos()))
				break
			end
		end

		minetest.add_particlespawner(particles)
	end,

	on_start = function(self)
		self._pl_properties_backup = table.copy(self.player:get_properties())
		self._old_saturation = self.player:get_lighting().saturation
		self.player:set_lighting({saturation = self._saturation})
		self._old_fov = self.player:get_fov()
		self.player:set_fov(self._fov, true, 0.1)

		self.player:set_properties({
			makes_footstep_sound = false,
			pointable = false,
			nametag = "",
			visual_size = {x=0,y=0,z=0}
		})

		self:teleport()
	end,

	on_stop = function (self)
		self.player:set_properties(self._pl_properties_backup)
		self.player:set_lighting({saturation = self._old_saturation})
		self.player:set_fov(self._old_fov, false, 0.1)
		self._pl_properties_backup = nil

		local pl_pos = self.player:get_pos()
		local particles = {
			amount = 60,
			time = 0.1,
			pos = {
				min = pl_pos+vector.new(0.8,2,0.8), max = pl_pos+vector.new(-0.8,0,-0.8)
			},
			attract = {
				kind = "point",
				strength = -1.5,
				origin = pl_pos+vector.new(0,1,0),
				die_on_contact = false
			},
			minsize = 2,
			maxsize = 8,
			glow = 12,
			texture = {
				name = "froyale_oakblade_leap_particle.png",
				alpha_tween = {1, 0.15},
				scale_tween = {
					{x = 1, y = 1},
					{x = 0, y = 0},
				}
			}
		}

		minetest.add_particlespawner(particles)
	end
})




function find_teleport_pos(pos, pl_pos)
  local dir = vector.direction(pos, pl_pos)
	local tries = {
		vector.normalize(vector.new(dir.x, 0.7, dir.z)),
		dir*2,
		dir*3,
		dir*4,
    dir*10  -- prevents getting stuck in a block teleporting you, at most, 10 nodes away from it
	}

	for _, d in ipairs(tries) do
		local pos = vector.add(pos, d)
    local head_pos = vector.add(pos, vector.new(0,1.5,0))
		local node = minetest.get_node_or_nil(pos)
    local head_node = minetest.get_node_or_nil(head_pos)

		if node and head_node then
			local def = minetest.registered_nodes[node.name]
      local head_def = minetest.registered_nodes[head_node.name]

			if (def and not def.walkable) and (head_def and not head_def.walkable) then
				return pos
			end
		end
	end
	return pos
end
