local folder = fantasia.srcpath .. "/classes/oakblade/"
dofile(folder .. "oakblade_skill.lua")



local S = minetest.get_translator("fantasia_royale")

local x = 0.92
local y = 0.95
local z = 0.92



classy.register_class("fantasia_royale:oakblade", {
    name = S("Oakblade"),
    description = S("Assassin"),
    aspect = {visual_size = {x = x, y = y, z = z}},
    collisionbox = {-0.3 * x, 0.0, -0.3 * z, 0.3 * x, 1.7 * y, 0.3 * z},
    starting_skills = {"fantasia_royale:void_leap"},
    _splash_art = "froyale_classes_oakblade_splash.png",
    _thumbnail = "froyale_classes_oakblade_thumbnail.png"
})