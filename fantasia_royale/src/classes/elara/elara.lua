local folder = fantasia.srcpath .. "/classes/elara/"
dofile(folder .. "elara_skill.lua")

local S = minetest.get_translator("fantasia_royale")

local x = 1.15
local y = 1
local z = 1.1




classy.register_class("fantasia_royale:elara", {
    name = S("Lady-of-Arms Elara"),
    description = "Off-tank",
    aspect = {visual_size = {x = x, y = y, z = z}},
    collisionbox = {-0.3 * x, 0.0, -0.3 * z, 0.3 * x, 1.7 * y, 0.3 * z},
    starting_skills = {"fantasia_royale:warcry"},
    _splash_art = "froyale_classes_elara_splash.png",
    _thumbnail = "froyale_classes_elara_thumbnail.png"
})