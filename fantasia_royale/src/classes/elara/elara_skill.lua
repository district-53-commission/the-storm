local S = minetest.get_translator("fantasia_royale")



skills.register_skill("fantasia_royale:warcry", {
	name = S("Warcry"),
	description = S("Doubles your HPs and deals twice the damage for 5s. Can't move for 3 after it ends"),
	_icon = "froyale_skills_warcry_icon.png",
	cooldown = 25,

	_status = {SKIP_FIRST_CAST = 0, RAGE = 1, RESTING = 2},
	_current_status = 0,

	loop_params = {
		duration = 8,
		cast_rate = 5
	},

	sounds = {
		start = {name="froyale_elara_scream"}
	},

	hud = {{
		name = "vignette",
		hud_elem_type = "image",
		position = {x = 0.5, y = 0.5},
		scale = {
			x = -100,
			y = -100
		},
		text =  "froyale_rage_vignette.png",
		z_index = 99
	}},

	-- TODO: HUD when resting and entity on the player's head (like iron skin on fbrawl)

	attachments = {
		particles = {{
			amount = 5*30,
			time = 5,
			minpos = {x = -0.3, y = 0, z = -0.5},
			maxpos = {x = 0.3, y = 1.5, z = 0.5},
			minvel = {x = 0, y = 0.2, z = 0},
			maxvel = {x = 0, y = 1.5, z = 0},
			minsize = 3,
			maxsize = 6,
			glow = 12,
			texture = {
				name = "froyale_rage_particle.png",
				alpha_tween = {1, 0},
				scale_tween = {
					{x = 1, y = 1},
					{x = 0, y = 0},
				},
				animation = {
					type = "vertical_frames",
					aspect_w = 16, aspect_h = 16,
					length = 0.3,
				},
			},
			minexptime = 1.5,
			maxexptime = 2,
		}}
	},

	on_start = function(self)
		local hp = self.player:get_hp() * 2
		local new_hp_max = self.player:get_properties().hp_max * 2

		self.player:set_properties({hp_max = new_hp_max})
		self.player:set_hp(hp)
		self._current_status = self._status.SKIP_FIRST_CAST
	end,

	cast = function(self)
		if self._current_status == self._status.SKIP_FIRST_CAST then
			self._current_status = self._status.RAGE

		elseif self._current_status == self._status.RAGE then
			minetest.add_entity(self.player:get_pos(), "fantasia_royale:player_anchor", self.pl_name)

			self.player:hud_change(self._hud.vignette, "text", "blank.png")

			self._original_saturation = self.player:get_lighting().saturation
			self.player:set_lighting({saturation = 0.6})

			self:on_stop()
		end
	end,

	on_stop = function(self)
		if self._current_status == self._status.RAGE then -- from rage -> resting
			local hp = self.player:get_hp() / 2
			local new_hp_max = self.player:get_properties().hp_max / 2

			self.player:set_properties({hp_max = new_hp_max})
			self.player:set_hp(hp)

			self._current_status = self._status.RESTING
			minetest.sound_play("froyale_elara_breathing", {to_player = self.player:get_player_name()})
		else -- from resting -> stop
			self.player:set_lighting({saturation = self._original_saturation})
		end
	end
})



minetest.register_on_player_hpchange(function(player, hp_change, reason)
	if hp_change < 0 and reason.type == "punch" and minetest.is_player(reason.object) then
		local hitter_name = reason.object:get_player_name()
		local elara_skill = hitter_name:get_skill("fantasia_royale:warcry")

		if elara_skill and elara_skill._current_status == elara_skill._status.RAGE then
			return hp_change*2
		end
	end

	return hp_change
end, true)