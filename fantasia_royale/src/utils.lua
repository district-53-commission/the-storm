-- Throws an item from position with random velocity
local function spawn_throw_item() return end


function fantasia.print_error(pl_name, msg)
	minetest.chat_send_player(pl_name, minetest.colorize("#e6482e", arena_lib.mods["fantasia_royale"].prefix .. msg))
end



function fantasia.print_msg(pl_name, msg)
	minetest.chat_send_player(pl_name, arena_lib.mods["fantasia_royale"].prefix .. msg)
end


-- ### CLEAR INVENTORY ###

-- clears a player's inventory, including armor. Should be called when they die.
-- INPUT:
-- player: player object reference
-- place_in_world: bool, default:false, puts the items in the world.


function fantasia.clear_inv(player,place_in_world)
    -- toggle for putting items in world
    place_in_world = place_in_world or false
    local pos = player:get_pos()
    local inv = player:get_inventory()
    local list = inv:get_list("main")

    for _, item in pairs(list) do
        if place_in_world then
            spawn_throw_item(pos,item)
        end
        inv:remove_item("main", item)
    end

    -- remove armor
    local _, armor_inv = armor:get_valid_player(player, "3d_armor")
    if place_in_world then
        for _, item in pairs(armor_inv) do
            spawn_throw_item(pos,item)
        end
    end

    -- use 3d_armor's function to remove armor (thereby updating model, etc)
    local elems = armor:get_weared_armor_elements(player)
    if elems then
        armor:remove_all(player)
    end
end



-- ### LOOK RAYCAST ###

-- casts a ray in the direction a player is looking.
-- INPUT:
-- player: player object reference
-- range: number, how far the ray will get
-- objects/liquids: bool, default: true/false. minetest.raycast arguments.

function fantasia.look_raycast(player, range, objects, liquids)
    objects = objects or true
    liquids = liquids or false

    local pl_pos = player:get_pos()
    local head_pos = {x = pl_pos.x, y = pl_pos.y+1.5, z = pl_pos.z}
    local looking_dir = player:get_look_dir()
    local shoot_offset = vector.multiply(looking_dir, range)

    local ray = minetest.raycast(
        vector.add(head_pos, vector.divide(looking_dir, 2)), -- to avoid the player
        vector.add(head_pos, shoot_offset),
        objects,
        liquids
    )

    return ray
end





--------------------------------------------------
--------------------------------------------------
---------LOCAL FUNCTION IMPLEMENTATIONS-----------
--------------------------------------------------
--------------------------------------------------

-- Throws an item from position with random velocity

-- pos: the position to spawn at
-- item: the itemstring to spawn

local function spawn_throw_item(pos,item)
    local obj = minetest.add_item(pos, item)
    if obj then
        obj:set_velocity({
            x = math.random(-2, 2),
            y = math.random(1, 2),
            z = math.random(-2, 2)
        })
    end
end
