
--[[
    
license for this file only:

Copyright (C)2022 Aaron Suen <warr1024@gmail.com>
Portions Copyright (C)2022 Wuzzy <Wuzzy@disroot.org>
Portions Copyright (C)2022 MisterE <MisterE123.coding@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject
to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


]]--



-- LUALOCALS < ---------------------------------------------------------
local ItemStack, getmetatable, minetest, setmetatable
    = ItemStack, getmetatable, minetest, setmetatable
-- LUALOCALS > ---------------------------------------------------------

local invplayer = {}
setmetatable(invplayer, {__mode = "k"})

local invgiving = {}
setmetatable(invgiving, {__mode = "k"})

local function newadd(inv, item, player)
    local p_name = player:get_player_name()
    local in_arena = arena_lib.is_player_in_arena(p_name, "fantasia_royale")
    if not in_arena then
        return inv:add_item("main", item)
    end
    -- first, copy the old list for comparison.
    local old_list = table.copy(inv:get_list("main"))
	-- Then, add item as normal
	local ret = inv:add_item("main", item)
    -- find the change
    local new_list = inv:get_list("main")
    local found = false
    local idx
    local stack
    for k,v in ipairs(old_list) do
        if new_list[k] ~= v then
            found = true
            idx = k
            stack = new_list[k]
        end
    end
    if found then
        -- then, perform the "put" action for sorting the inventory.
        fantasia.sections_on_player_inventory_action(player, "put", inv, {listname="main", index=idx, stack=stack})
    end
    return ret
end

local function wrapinv(inv, player)
	local meta = getmetatable(inv)
	meta = meta and meta.__index or meta
	local oldadd = meta.add_item
	function meta:add_item(listname, stack, ...)
		if invgiving[self] then return oldadd(self, listname, stack, ...) end
		invgiving[self] = true
		local function helper(...)
			invgiving[self] = nil
			return ...
		end
		local found = invplayer[self]
		if found then
			return helper(newadd(self, stack, player))
		else
			return helper(oldadd(self, listname, stack, ...))
		end
	end
	wrapinv = function(i, p)
		if i then invplayer[i] = p end
		return i
	end
	return wrapinv(inv, player)
end

local function patchplayers()
	local player = (minetest.get_connected_players())[1]
	if not player then
		return minetest.after(0, patchplayers)
	end

	local meta = getmetatable(player)
	meta = meta and meta.__index or meta
	if not meta.get_inventory then return end

	local getraw = meta.get_inventory
	function meta:get_inventory(...)
		return wrapinv(getraw(self, ...), self)
	end
end
minetest.after(0, patchplayers)


