-- NOTE FOR READING LEADERBOARDS: If the arena is in_game, use the
-- arena.leaderboard property. If the arena is not in_game, then use
-- retrieve_leaderboard_from_storage. safe_read_leaderboard does this for you.


local function sort_and_limit(leaderboard) end
local function get_record_by_p_name(leaderboard,p_name) end
local function get_leader_form() end
local function create_score_table() end

-- the leaderboard is a table of records:
--[[
{
    {p_name="jeff",xp=5}
    {p_name="sam",xp=3}
}
]]

function fantasia.add_xp(arena,p_name,xp)
    local r_idx,record = get_record_by_p_name(arena.leaderboard,p_name)
    if r_idx then
        arena.leaderboard[r_idx].xp = record.xp + xp
    else
        record = {p_name = p_name, xp = xp}
        table.insert(arena.leaderboard,record)
    end
    sort_and_limit(arena.leaderboard)
    fantasia.store_leaderboard(arena)
end

function fantasia.store_leaderboard(arena)
    local leaderboard = arena.leaderboard
    local arena_id = arena_lib.get_arena_by_name("fantasia_royale", arena.name)
    local tag = "leaderboard_"..arena_id
    -- json is used for easy reading by a web server
    fantasia.store_table(tag, leaderboard)

    local json_leaderboard = {}
    json_leaderboard.entries = table.copy(leaderboard)
    json_leaderboard.meta = {arena_name = arena.name, last_update = os.time()}
    fantasia.store_json(tag, json_leaderboard)
end


function fantasia.retrieve_leaderboard_from_storage(arena)
    local arena_id = arena_lib.get_arena_by_name("fantasia_royale", arena.name)
    local tag = "leaderboard_"..arena_id
    return fantasia.load_table(tag)
end


function fantasia.safe_read_leaderboard(arena)
    if arena.in_game or arena.in_celebration then
        return arena.leaderboard
    else
        return fantasia.retrieve_leaderboard_from_storage(arena)
    end
end


-- shows an arena's leaderboard to p_name
function fantasia.show_leaderboard(arena,p_name)
    minetest.show_formspec(p_name,"froyale_leaderboard", get_leader_form(arena, p_name))
end

-- LOCAL FUNCTIONS

function sort_and_limit(leaderboard)
    local sort = function(a,b)
        return a > b
    end
    local record_sorter = function(rec_a,rec_b)
        return sort(rec_a["xp"],rec_b["xp"])
    end
    table.sort(leaderboard,record_sorter)
    if #leaderboard > 100 then
        for i = #leaderboard, 101, -1 do
          table.remove(leaderboard,i)
        end
    end
end

-- returns record index and the record itself
function get_record_by_p_name(leaderboard,p_name)
    for i, record in ipairs(leaderboard) do
        if record.p_name == p_name then
            return i,record
        end
    end
end



local formspec_prefix = "formspec_version[5]"..
    "size[10.5,10.5]"..
    "background9[-.5,-.5;11.5,11.5;froyale_leader_bg.png;false;125]"..
    -- content_offset=0 to prevent button from moving the text when clicked
    "style_type[button;border=false;content_offset=0]"..
    "style_type[button,table;font=normal,bold]"..
    "style[hs_title;font_size=+3]"..
    "style[hs_title;textcolor=#e6482e]"..
    "style[arena_name;textcolor=#f4b41b]"..
    "tableoptions[color=#CFC6B8;highlight_text=#CFC6B8;highlight=#00000000;border=false;background=#00000010]"..
    "tablecolumns[color;text,align=right;text,padding=2;text,align=right,padding=4]"


function get_leader_form(arena, select_player)
    local p_names = "<no data>"
    local sel_idx


    local table_data, sel_idx = create_score_table(arena, select_player)
    if not sel_idx then
        sel_idx = 1
    end

    return formspec_prefix ..
    -- fake buttons to create centered text (clicking them does nothing)
    "button[0.6,0.6;9.3,0.8;hs_title;"..minetest.formspec_escape("Fantasia Royale Leaderboard").."]"..
    "button[0.6,1.6;9.3,0.8;arena_name;"..arena.name.."]"..

    "table[0.6,2.5;9.3,7.4;names;"..table_data..";"..sel_idx.."]"

end

function create_score_table(arena, select_player)
    local empty_text = "The leaderboard is empty."
    local content = ""
    local rank = 0
    local rank_str = "0"
    local sel_idx = 1
    local prev_xp = 0

    local leaderboard = fantasia.safe_read_leaderboard(arena)

    if #leaderboard > 0 then
        for idx,record in ipairs(leaderboard) do
            if prev_xp ~= record.xp then
                rank = rank + 1
                rank_str = tostring(rank)
            else
                rank_str = ""
            end
            local color = ""
            -- index for table selection
            if record.p_name == select_player then
                sel_idx = idx
                -- highlight row if this is the player we're looking for
                color = "#f47e1b"
            end

            -- write row: color, rank, player name, score
            content = content .. color .. "," .. rank_str ..","..minetest.formspec_escape(record.p_name)..","..record.xp

            -- next row
            if idx ~= #leaderboard then
                content = content .. ","
            end
            prev_xp = record.xp

        end
    else
        return ",,"..minetest.formspec_escape(empty_text), sel_idx
    end
    return content, sel_idx
end

minetest.register_chatcommand("fantasia_leaderboard",{
    func = function(p_name, param)
        if param == nil or param == "" then
            return "Usage: /fantasia_leaderboard <arena_name>; shows an arena's leaderboard."
        end
        local _, arena = arena_lib.get_arena_by_name("fantasia_royale",param)
        if arena then
            fantasia.show_leaderboard(arena,p_name)
        else
            return "That arena does not exist!"
        end
    end
})