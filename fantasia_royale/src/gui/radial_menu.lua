
local function show_radial_menu(p_name) end
local function close_radial_menu(p_name) end
local function get_radial_menu_tree(p_name) end
local main_fs_tree = {}
local flying_fs_tree = {}

minetest.register_on_player_receive_fields(function(player, formname, fields)
    local p_name = player:get_player_name()
    if not formname == "radial_menu" then return end

    if fields.waypoint_button then
        -- TODO: place waypoint at pointed location
    end

    if fields.map_button then
        minetest.close_formspec(p_name, "radial_map")
        local mod = arena_lib.get_mod_by_player(p_name)
        if mod ~= "fantasia_royale" then return end
        local arena = arena_lib.get_arena_by_player(p_name)
        if not arena then return end
        fantasia.show_minimap_fs(p_name, arena)
    end

    if fields.emoji_button then
        -- TODO: open emojis
    end

    if fields.blank1_button or fields.blank2_button or fields.blank3_button or fields.blank4_button or fields.center_exit then
        close_radial_menu(p_name)
    end

end)


-- close the radial menu when a player leaves the arena
arena_lib.register_on_quit(function(mod, arena, p_name, is_spectator, reason)
    if mod == "fantasia_royale" and not is_spectator then
        close_radial_menu(p_name)
    end
end)
arena_lib.register_on_celebration(function(mod, arena, winners)
    if mod == "fantasia_royale" then
        for pl_name, stats in pairs(arena.players) do
            close_radial_menu(pl_name)
        end
    end
end)
arena_lib.register_on_eliminate(function(mod, arena, p_name)
    if mod == "fantasia_royale" then
        close_radial_menu(p_name)
    end
end)



-- use controls to open the radial menu when aux1 is pressed and inside the minigame arena
controls.register_on_press(function(player, key)

    if not arena_lib.is_player_playing(player:get_player_name(), "fantasia_royale") then return end

    if key == "aux1" then

        show_radial_menu(player:get_player_name())
    end

end)




function show_radial_menu(p_name)
    if not arena_lib.is_player_playing(p_name, "fantasia_royale") then return end
    formspec_ast.show_formspec(p_name, "radial_menu", get_radial_menu_tree(p_name))
end

function close_radial_menu(p_name)
    minetest.close_formspec(p_name, "radial_menu")
    minetest.close_formspec(p_name, "radial_emojis")
    minetest.close_formspec(p_name, "fantasia_royale:minimap")
end



function get_radial_menu_tree(p_name)
    local main_fs_tree = {
        formspec_version = 7,
        { type = "size", w = 68, h = 68, fixed_size = false },
        { type = "no_prepend" },
        { type = "bgcolor", fullscreen = "neither", bgcolor = "#FFFFFFFF" },
        { type = "background", x = 0, y = 0, w = 68, h = 68, texture_name = "froyale_wheel_bg.png" },
        { type = "style", selectors = {"exit_btn"}, props = { bgcolor = "#FFFFFFFF", noclip = true, alpha = true, border = false} },
        { type = "style", selectors = {"exit_btn:hovered"}, props = {bgcolor = "#FFFFFFFF"} },
        { type = "style", selectors = {"exit_btn:pressed"}, props = {bgcolor = "#FFFFFFFF"} },
        { type = "button_exit", x = -100, y = -100, w = 500, h = 500, name = "exit_btn", label = "" },
    }





    -- table.insert(main_fs_tree, { type = "style", selectors = {"waypoint_button:hovered"}, props = {fgimg = "froyale_waypoint_button_hovered.png"} })
    table.insert(main_fs_tree,{
        type = "image_button",
        x = 2, y = 2,
        w = 32, h = 32,
        -- texture_name = "froyale_waypoint_button.png",

        texture_name = "froyale_blank1_button.png", -- blank for now since Waypoint selection by pointing is not yet implemented

        name = "waypoint_button",
        label = "",
        drawborder = false,
        -- pressed_texture_name = "froyale_waypoint_button_pressed.png",
    })



    -- the map button is always shown.
    table.insert(main_fs_tree, { type = "style", selectors = {"map_button:hovered"}, props = {fgimg = "froyale_map_button_hovered.png"} })
    table.insert(main_fs_tree,{
        type = "image_button",
        x = 34, y = 2,
        w = 32, h = 32,
        texture_name = "froyale_map_button.png",
        name = "map_button",
        label = "",
        drawborder = false,
        pressed_texture_name = "froyale_map_button_pressed.png",
    })

    -- the third button is blank for now
    -- table.insert(main_fs_tree, { type = "style", selectors = {"blank3_button:hovered"}, props = {fgimg = "froyale_blank3_button_hovered.png"} })
    table.insert(main_fs_tree,{
        type = "image_button",
        x = 34, y = 34,
        w = 32, h = 32,
        texture_name = "froyale_blank3_button.png",
        name = "blank3_button",
        label = "",
        drawborder = false,
        -- pressed_texture_name = "froyale_blank3_button_pressed.png"
    })




    -- table.insert(main_fs_tree, { type = "style", selectors = {"blank4_button:hovered"}, props = {fgimg = "froyale_blank4_button_hovered.png"} })
    table.insert(main_fs_tree,{
        type = "image_button",
        x = 2, y = 34,
        w = 32, h = 32,
        texture_name = "froyale_blank4_button.png",
        name = "blank4_button",
        label = "",
        drawborder = false,
        -- pressed_texture_name = "froyale_blank4_button_pressed.png"
    })


    -- after all that, we will add a center button to close the form

    table.insert(main_fs_tree, { type = "style", selectors = {"center_exit:hovered"}, props = {fgimg = "froyale_exit_button_hovered.png"} })
    table.insert(main_fs_tree,{
        type = "image_button",
        x = 29, y = 29,
        w = 10, h = 10,
        texture_name = "froyale_exit_button.png",
        name = "center_exit",
        label = "",
        drawborder = false,
        pressed_texture_name = "froyale_exit_button_pressed.png"
    })

    return main_fs_tree
end


