local S = minetest.get_translator("fantasia_royale")

local function get_formspec() end
local function get_thumbnails() end

local selected_class = {}                       -- KEY: p_name; VAL: c_name



function fantasia.GUI_show_class_selector(p_name)
    local chara = fantasia.get_class(p_name)

    selected_class[p_name] = chara
    minetest.show_formspec(p_name, "fantasia_royale:class_selector", get_formspec(chara))
end



function get_formspec(chara)
    local class =  classy.get_class_by_name(chara)
    local skill = skills.get_skill_def(class.starting_skills[1])
    local thumbnails = get_thumbnails(chara)

    local formspec = {
        "formspec_version[6]",
        "size[20.4,12]",
        "no_prepend[]",
        "bgcolor[;true]",
        "background[-1.4,-0.7;22.7,13.5;froyale_gui_classes_bg.png]",
        "style_type[image_button;border=false;font=bold]",
        "image[0.2,0.88;9.7,8.8;" .. class._splash_art .. ";]",
        "container[11,3]",
        "image[-0.83,-0.08;8.95,3.7;froyale_gui_classes_selector_infobox_bg.png]",
        "hypertext[0,0;7.26,0.8;pname_txt;<global size=34 halign=center valign=bottom><b>" .. class.name .. "</b>]",
        "hypertext[0,0.8;7.26,0.5;pname_txt;<global size=16 halign=center valign=top><b>" .. class.description .. "</b>]",
        "image[0.2,1.9;1,1;" .. skill._icon .. "]",
        "hypertext[1.5,1.75;5.5,0.5;pname_txt;<global size=15 halign=left valign=middle><b>" .. skill.name .. "</b>]",
        "hypertext[1.5,2.15;5.5,0.9;pname_txt;<global size=14 halign=left valign=top>" .. skill.description .. "]",
        "container_end[]",
        "container[2.7,9.26]",
        "image[-0.45,0;15.9,1.2;froyale_gui_classes_selector_char_bg.png]",
        "container[5.7,0]",
        thumbnails,
        "container_end[]",
        "container_end[]",
        "image_button[9,11.1;2.4,0.7;froyale_gui_misc_button1.png;equip;Select]"
    }

    return table.concat(formspec, "")
end



function get_thumbnails(chara)
    local thumbnails = {}
    -- as we can't tell the order in which classes have been stored in Classy, we
    -- manually determine the order here
    local thumb_x = {
        ["fantasia_royale:elara"] = 0,
        ["fantasia_royale:sol_arca"] = 1.2,
        ["fantasia_royale:oakblade"] = 2.4
    }

    for cl_name, cl in pairs(classy.get_classes("fantasia_royale", true)) do
        local greyed_out = cl_name == chara and "" or "^[colorizehsl:0:0:-30"

        thumbnails[#thumbnails + 1] = "image_button[" .. thumb_x[cl_name] .. ",0;1.2,1.2;" .. cl._thumbnail .. greyed_out .. ";" .. cl_name .. ";]"
    end

    return table.concat(thumbnails, "")
end





----------------------------------------------
--------------FIELDS MANAGEMENT---------------
----------------------------------------------

minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname ~= "fantasia_royale:class_selector" then return end

    local p_name = player:get_player_name()

    -- equip button
    if fields.equip then
        local curr_class = fantasia.get_class(p_name)
        local sel_class = selected_class[p_name]

        if curr_class == sel_class then return end

        if fantasia.is_class_unlocked(p_name, sel_class) then
            fantasia.set_class(p_name, sel_class)

            minetest.sound_play("froyale_ui_confirm", {to_player = p_name})
            minetest.close_formspec(p_name, "fantasia_royale:class_selector")
            return
        end
    end

    -- hero selection
    for k, _ in pairs(fields) do
        if k:find(":") then
            selected_class[p_name] = k
            minetest.show_formspec(p_name, "fantasia_royale:class_selector", get_formspec(k))
        end
    end
end)