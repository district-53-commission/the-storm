-- -- An entity to keep players still while the game loads. -- Sorry, we need
-- this anyways, to wait for the storm entity to load. All players have to sit
-- at the storm's mapblock or it may not load for a whole minute or more.

local dummy = {
    initial_properties = {
        visual = "sprite",
        physical = false,
        hp_max = 32,
        --textures = {"blank.png"},
        textures = {"logo.png"}, -- DEBUG
        collisionbox = {-.1,-.1,-.1,.1,.1,.1},
    },
    on_step = function(self,dtime)
        -- remove if there are no players left attached
        local children = self.object:get_children()
        if #children == 0 then
            self.object:remove()
        end
    end,
}

minetest.register_entity("fantasia_royale:dummy", dummy)




