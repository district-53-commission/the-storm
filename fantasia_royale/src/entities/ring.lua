minetest.register_entity("fantasia_royale:ring",{
    initial_properties = {
        visual = "mesh",
        mesh = "froyale_the_storm_cylinder.b3d",
        physical = false,
        hp_max = 32,
        glow = -1,
        textures = {"froyale_ring.png"},
        pointable = false,
        use_texture_alpha = true,
        shaded = false,
        backface_culling = false,
        static_save = false,
    },
    _arena_name = "",
    _matchID = 0,
    _selfID = 0, -- one ring to rule them all
    _update_properties = function(self,arena)
        -- set circle size
        local size = arena.storm_radius
        local oldy = self.object:get_properties().visual_size.y
        self.object:set_properties({visual_size={x=size,y=oldy,z=size}})
        -- set circle offset
        self.object:set_bone_position("cyl", vector.multiply(arena.storm_offset,-(1/size)*10), nil)
    end,

    on_step = function(self,dtime)
        local _, arena = arena_lib.get_arena_by_name("fantasia_royale", self._arena_name)
        if not arena or not arena.in_game or self._matchID ~= arena.matchID or
            (arena.storm_obj_ref and arena.storm_obj_ref:get_luaentity()._selfID ~= self._selfID) then

            self.object:remove()
            return
        end
    end,

    on_activate = function(self, staticdata, dtime_s)
        -- store arena reference
        local arena = arena_lib.get_arena_by_pos(self.object:get_pos(), "fantasia_royale")
        if arena then
            self._arena_name = arena.name
            self._matchID = arena.matchID
            self._selfID = math.random()
        else
            minetest.log("error", "The `fantasia_royale` storm entity was spawned without a valid arena. This is a bug. Removing it.")
            self.object:remove()
        end
    end,

})