

local glider = {
	initial_properties = {
		visual = "mesh",
		visual_size = {x = 12, y = 12},
		collisionbox = {0,0,0,0,0,0},
		mesh = "froyale_glider.obj",
		immortal = true,
		static_save = false,
		textures = {"wool_white.png", "default_wood.png"},
	},
	on_step = function(self, d_time, moveresult)
		local player = self.object:get_attach()
		-- if the player disconnects
		if not player then
            minetest.debug("removing glider because player disconnected")
			self.object:remove()
			return
		end
	end,
    on_deactivate = function(self, removal) 
        minetest.debug("removing glider in on_deactivate")
    end,
}

minetest.register_entity("fantasia_royale:glider", glider)