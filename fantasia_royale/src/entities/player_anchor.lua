local anchor = {
	initial_properties = {
		 hp_max = 999,
		 physical = true,
		 visual_size = {x = 0, y = 0},
		 textures = {"blank.png"},
		 pointable = false,
		 visual = "sprite",
		 static_save = false,
	},
	_p_name = ""
}



function anchor:on_activate(p_name, dtime_s)
	local player = minetest.get_player_by_name(p_name)

	if not arena_lib.is_player_playing(p_name, "fantasia_royale") then
		 self.object:remove()
		 return
	end

	self.object:set_acceleration({x=0, y=-9.6, z=0})
	self.object:set_properties({collisionbox = player:get_properties().collisionbox})
	self.object:set_rotation(vector.dir_to_rotation(player:get_look_dir()))

	self._p_name = p_name
	player:set_attach(self.object)
end



function anchor:on_step(dtime_s)
	local warcry = self._p_name:get_skill("fantasia_royale:warcry")

	if not arena_lib.is_player_playing(self._p_name, "fantasia_royale") then
		 self.object:remove()
		 return
	end

	if not warcry or not warcry.is_active then
		self.object:remove()
	end
end



minetest.register_entity("fantasia_royale:player_anchor", anchor)
