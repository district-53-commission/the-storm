local function jump_off() end

local vehicles = {}                                -- KEY: arena_name; VALUE: vehicle

local color1_texture = "wool_blue.png"
local color2_texture = "wool_yellow.png"

-- local fire_tex = "default_furnace_fire_fg.png"
local canvas_texture = "wool_white.png^[colorize:#f4e7c1:128"
local metal_texture = "default_clay.png^[colorize:#a3acac:128"
local black_texture = "default_clay.png^[colorize:#030303:200"
local wood_texture = "default_clay.png^[colorize:#3a270d:230"
local forno_texture = black_texture.."^[mask:froyale_steampunk_blimp_forno_mask.png"
local rotor_texture = "("..canvas_texture.."^[mask:froyale_steampunk_blimp_rotor_mask2.png)^(default_wood.png^[mask:froyale_steampunk_blimp_rotor_mask.png)"
local ladder_texture = "default_ladder_wood.png"

local initial_vehicle = {
    initial_properties = {
        visual = "mesh",
        backface_culling = false,
        physical = false,
        hp_max = 32,
        glow = -1, -- a soft glow that is not affected by the light level at the entity position, because the entity travels outside of the loaded map.
        mesh = "froyale_steampunk_blimp.b3d",
        textures = {
            black_texture, --alimentacao balao
            "default_wood.png", --asa
            canvas_texture, --asa
            canvas_texture, --balao
            color2_texture, --faixas brancas nariz
            color1_texture, --faixas azuis nariz
            metal_texture, --pontas do balão
            "blank.png",
            black_texture, --caldeira
            forno_texture, --caldeira
            "default_junglewood.png", --casco
            black_texture, -- corpo da bussola
            metal_texture, -- indicador bussola
            canvas_texture, --leme
            "default_junglewood.png", --leme
            wood_texture, --timao
            "blank.png",
            ladder_texture, --escada
            "default_wood.png", --mureta
            wood_texture, --mureta
            black_texture, --nacele rotores
            wood_texture, --quilha
            "default_wood.png", --rotores
            rotor_texture, --"steampunk_blimp_rotor.png", --rotores
            black_texture, --suportes rotores
            "default_junglewood.png", --suporte timao
            "froyale_steampunk_blimp_rope.png", --cordas
            color1_texture, --det azul
            color2_texture, --det branco
            wood_texture, --fixacao cordas
            "blank.png", --logo
            --"steampunk_blimp_metal.png",
            --"steampunk_blimp_red.png",
        },
        collisionbox = {-4, -2.5, -4, 4, 9, 4},
    },

    _arena_name = "",
  
    on_activate = function(self, staticdata, dtime_s)
        local vehicle = self.object

        if staticdata ~= "" and staticdata ~= nil then
            local data = minetest.parse_json(staticdata) or {}
            self.match_id = data.match_id
        else vehicle:remove() return end

        local arena = arena_lib.get_arena_by_pos(vehicle:get_pos(), "fantasia_royale")

        if not arena or not arena.in_game then vehicle:remove() return end

        if not arena.matchID == self.match_id then vehicle:remove() return end

        local a_name = arena.name

        self._arena_name = a_name

        self.object:set_animation({x = 1, y = 47}, 20, 0, true)
    end,
}

minetest.register_entity("fantasia_royale:initial_vehicle", initial_vehicle)







