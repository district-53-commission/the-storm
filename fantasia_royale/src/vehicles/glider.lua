local function is_too_close_to_ground(pos, arena) end
local function open_glider(player, p_name, arena) end
local function spawn_glider_object(player, p_name, arena) end
local function close_glider(player, p_name, arena) end
local function set_physics_overrides(player, overrides) end
local function remove_physics_overrides(player, p_name, arena) end
local function safe_node_below(pos) end
local function calc_speed(player) end
local function land(player, p_name, arena) end
local function check_for_and_respawn_glider(player, p_name, arena) end


local colours = {}

for _, col in pairs(arena_lib.PALETTE) do
	if col ~= "" then
		table.insert(colours, col)
	end
end


function fantasia.is_gliding(p_name, arena)
	return arena.players[p_name].is_gliding
end


-- timeouts for opening/closing the glider
local timeouts = {}
minetest.register_globalstep(function(dtime)
    for p_name, timeout in pairs(timeouts) do
        local timeout = timeout - dtime
        if timeout <= 0 then
            timeouts[p_name] = nil
        else
            timeouts[p_name] = timeout
        end
    end
end)

-- ignore_if_not_found is used for being sure to remove the glider object on
-- quit if it exists without worrying about the arena state
function fantasia.remove_glider_object(player, ignore_if_not_found)
    local children = player:get_children()
    local glider_object
    for _, child in pairs(children) do
        local luaent = child:get_luaentity()
        if luaent and luaent.name == "fantasia_royale:glider" then
            glider_object = child
        end
    end
    if not(ignore_if_not_found) then
        assert(glider_object, "glider object not found while trying to close glider")
    end
    if glider_object then
        minetest.debug("removing glider object")
        glider_object:remove()
    end
end



function fantasia.start_gliding(p_name, arena)
    timeouts[p_name] = .5
    if arena.players[p_name].is_gliding then return end
    local player = minetest.get_player_by_name(p_name)
    arena.players[p_name].is_gliding = true
    arena.players[p_name].is_glider_open = false
    arena.players[p_name].can_control_glider = true
    if not arena.players[p_name].glider_color then arena.players[p_name].glider_color = colours[math.random(1, #colours)] end

    arena_lib.HUD_send_msg("broadcast", p_name, "Press Space to open/close the glider")

    open_glider(player, p_name, arena)
    player:hud_set_flags({wielditem = false})
end


controls.register_on_press(function(player, key)
    if not arena_lib.is_player_playing(player:get_player_name(), "fantasia_royale") then return end
    local p_name = player:get_player_name()
    local arena = arena_lib.get_arena_by_player(p_name)

    if not arena.players[p_name].is_gliding then return end
    if not arena.players[p_name].can_control_glider then return end

    local is_open = arena.players[p_name].is_glider_open

    if key == "jump" then
        if is_open then
            if not timeouts[p_name] then
                close_glider(player, p_name, arena)
                timeouts[p_name] = 0.5
            end
        else
            if not timeouts[p_name] then
                open_glider(player, p_name, arena)
                timeouts[p_name] = 0.5
            end
        end
    end
end)


minetest.register_globalstep(function(dtime)
    -- change players's physics while gliding
    for _, player in pairs(minetest.get_connected_players()) do
        local p_name = player:get_player_name()
        if arena_lib.is_player_playing(p_name, "fantasia_royale") then
            local arena = arena_lib.get_arena_by_player(p_name)
            if arena.players[p_name].is_gliding then

                local pos = player:get_pos()
                local too_close, height_of_map = is_too_close_to_ground(pos, arena)

                check_for_and_respawn_glider(player, p_name, arena)

                if too_close then
                    
                    if arena.players[p_name].can_control_glider then
                        arena.players[p_name].can_control_glider = false
                        
                        arena_lib.HUD_hide("broadcast", p_name)

                        -- attach the player to a dummy object to stop their movement
                        local dummy_pos = player:get_pos()
                        dummy_pos.y = height_of_map
                        local dummy = minetest.add_entity(player:get_pos(), "fantasia_royale:dummy")
                        player:set_attach(dummy, "", vector.new(0, 0, 0), vector.new(0, 0, 0),true)
                        minetest.after(0, function()
                            if player and arena_lib.is_player_playing(p_name, "fantasia_royale") then
                                player:set_detach() 
                            end
                            if dummy then
                                dummy:remove()
                            end
                        end)

                    end

                    


                    if not arena.players[p_name].is_glider_open then
                        open_glider(player, p_name, arena)
                    end
                end

                local is_on_ground = safe_node_below(pos)
                
                if arena.players[p_name].is_glider_open then
                    calc_speed(player)
                end

                if is_on_ground then
                    if arena.players[p_name].is_glider_open then
                        close_glider(player, p_name, arena)
                    end
                    land(player, p_name, arena)
                end

            end

        end
    end
end)


-- why? because minetest loves deleting entities randomly: https://github.com/minetest/minetest/issues/14853
function check_for_and_respawn_glider(player, p_name, arena)
    if arena.players[p_name].is_glider_open then
        local children = player:get_children()
        local glider_object
        for _, child in pairs(children) do
            local luaent = child:get_luaentity()
            if luaent and luaent.name == "fantasia_royale:glider" then
                glider_object = child
            end
        end
        if not glider_object then
            -- minetest.debug("Glider object not found while glider was supposed to be open. This is a bug!")
            spawn_glider_object(player, p_name, arena)
        end
    end
end


local heightmap_lookup = {}
function is_too_close_to_ground(pos, arena)

    -- we will get an approximation of the heightmap at this position
    local pos_2d = vector.new(pos.x, 0, pos.z)
    local height_of_map

    for _, heightmap_lookup_pos in pairs(heightmap_lookup) do
        local hmap_pos_2d = vector.new(heightmap_lookup_pos.x, 0, heightmap_lookup_pos.z)
        if vector.distance(pos_2d, hmap_pos_2d) < 5 then
            height_of_map = heightmap_lookup_pos.y
            break
        end
    end

    if not height_of_map then
        local test_arena = arena_lib.get_arena_by_pos(pos, "fantasia_royale")
        if not test_arena then 
            height_of_map = arena.ground_level 
        else
            -- this uses storage so it's a bit expensive, that is why it's only done when needed and we use a lookup
            height_of_map = fantasia.get_height(arena, math.floor(pos.x), math.floor(pos.z))
            if height_of_map == -35000 then
                height_of_map = arena.ground_level
            end
            local heightmap_lookup_save = vector.new(pos.x, height_of_map, pos.z)
            table.insert(heightmap_lookup, heightmap_lookup_save)
        end
    end

    -- cull the lookup if it gets too big
    if #heightmap_lookup > #arena.players + 10 then
        table.remove(heightmap_lookup, 1)
    end

    if pos.y < height_of_map + 40 then return true, height_of_map end


	-- local p1 = pos
	-- local p2 = vector.subtract(p1, vector.new(0, 15, 0))
	-- local raycast = minetest.raycast(p1, p2, false, true)
	-- return raycast:next() ~= nil
end


function safe_node_below(pos)
	local node = minetest.get_node_or_nil(vector.new(pos.x, pos.y - 0.5, pos.z))

	if not node then return false end

	local def = minetest.registered_nodes[node.name]

	return def and (def.walkable or (def.liquidtype ~= "none" and def.damage_per_second <= 0))
end



function calc_speed(player)
	local vel = player:get_velocity().y
	if vel < 0 and vel > -3 then
		set_physics_overrides(player, { speed = math.abs(vel / 2.0) + 1.0, gravity = (vel + 3) / 20 })
	elseif vel <= -3 then
		set_physics_overrides(player, { speed = 2.5, gravity = -0.1 })
		if vel < -5 then
			-- Extra airbrake when falling too fast
			player:add_velocity(vector.new(0, math.min(5, math.abs(vel / 10.0)), 0))
		end
	else  -- vel > 0
		set_physics_overrides(player, { speed = 1.0, gravity = 0.25 })
	end
end



function open_glider(player, p_name, arena)
    arena.players[p_name].is_glider_open = true
    minetest.sound_play("froyale_glider_equip", {pos = player:get_pos(), max_hear_distance = 8}, true)
    set_physics_overrides(player, {jump = 0, gravity = 0.25})
    spawn_glider_object(player, p_name, arena)
end

function spawn_glider_object(player, p_name, arena)
    local glider_object = minetest.add_entity(player:get_pos(), "fantasia_royale:glider", p_name)
    glider_object:set_properties({textures = {"wool_white.png^[multiply:" .. arena.players[p_name].glider_color, "default_wood.png" }})
    glider_object:set_attach(player, "", vector.new(0, 10, 0), vector.new(0, 0, 0),true)
end

local remove_glider_object = fantasia.remove_glider_object

function close_glider(player, p_name, arena)
    minetest.sound_play("froyale_glider_equip", {pos = player:get_pos(), max_hear_distance = 8}, true)
    remove_physics_overrides(player, p_name, arena)
    arena.players[p_name].is_glider_open = false
    remove_glider_object(player)
end

function set_physics_overrides(player, overrides)
	player:set_physics_override(overrides)
end


function remove_physics_overrides(player, p_name, arena)
    local overrides = {}

    if arena_lib.mods["fantasia_royale"].in_game_physics then
        for _, name in pairs({"jump", "speed", "gravity"}) do
            overrides[name] = arena_lib.mods["fantasia_royale"].in_game_physics[name] or 1.0
        end
    else
        overrides = {speed = 1.0, jump = 1.0, gravity = 1}
    end

    local class_name = fantasia.get_class(p_name)
    local class_data = classy.get_class_by_name(class_name)

    assert(class_data, "Fantasia uses Classes, but the class was not found when trying to set physics overrides. This is a bug!")

    local c_overrides = class_data.physics_override

    -- physics overrides may or may not have been defined for the class.
    if c_overrides then
        for _, name in pairs({"jump", "speed", "gravity"}) do
            if c_overrides[name] then
                overrides[name] = c_overrides[name]
            end
        end
    end


    for _, name in pairs({"jump", "speed", "gravity"}) do
		player:set_physics_override({[name] = overrides[name]})
	end
end

function land(player, p_name, arena)
    if not arena.players[p_name].has_recieved_starting_items then
        local p_name = player:get_player_name()
        local starting_items = {
            [1]="fantasia_royale:wind_surfer",
            [2]="",
            [3]="",
            [4]="",
            [5]="",
            [6]="",
            [7]="",
            [8]="fantasia_royale:pickaxe_1",
        }

        player:get_inventory():set_list("main", starting_items)

        arena.players[p_name].has_recieved_starting_items = true

    end
    player:hud_set_flags({wielditem = true})
    if arena.players[p_name].can_control_glider then
	    arena_lib.HUD_hide("broadcast", p_name)
    end
    arena.players[p_name].is_gliding = false
    arena.players[p_name].is_glider_open = false
    arena.players[p_name].can_control_glider = false
end


-- admin command to start gliding
minetest.register_chatcommand("fantasia_glide", {
    params = "",
    privs = {server = true},
    description = "Start gliding",
    func = function(name)
        local player = minetest.get_player_by_name(name)
        local arena = arena_lib.get_arena_by_player(name)

        if not arena then return "You are not in an arena!" end

        local is_gliding = fantasia.is_gliding(name, arena)

        if is_gliding then return "You are already gliding!" end

        fantasia.start_gliding(name, arena)
    end
})