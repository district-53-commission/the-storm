local function spawn_vehicle(arena, pos, dir, vel) end
local function attach_players_to_vehicle(vehicle, arena) end
local function drop_all_players_from_vehicle(arena, vehicle) end
local function drop_off_player(arena, p_name) end



function fantasia.create_initial_vehicle(arena)
    local p1 = vector.add(arena.pos1, vector.new(2, -2, 2)) -- a bit of padding to avoid having the vehicle collide against the borders of the map
    local p2 = vector.subtract(arena.pos2, vector.new(2, 2, 2)) -- same
    local max_y = math.max(p1.y, p2.y) - 20 -- a bit of padding to avoid having the vehicle collide against the top of the arena

    -- put the vehicle onto the first corner and move it from p1 to p2 in DROP_OFF_TIME
    p1.y = max_y
    p2.y = max_y


    local loading_time = arena_lib.mods["fantasia_royale"].load_time
    local dir = vector.direction(p1, p2)
    local dist_per_sec = vector.distance(p1, p2) / (fantasia.DROP_OFF_TIME + loading_time)
    local vel = vector.multiply(dir, dist_per_sec)

    local vehicle = spawn_vehicle(arena, p1, dir, vel)

    attach_players_to_vehicle(vehicle, arena)

    minetest.after(loading_time, function()
        arena_lib.HUD_send_msg_all("broadcast", arena, "Press Space to jump off")
    end)


    arena.initial_vehicle_exists = true
    arena.initial_vehicle_obj_ref = vehicle
    arena.initial_vehicle_pos = p1
    arena.initial_vehicle_dir = dir
    arena.initial_vehicle_vel = vel
    arena.initial_vehicle_start_time = minetest.get_us_time()
end


-- globalstep to record the position of the initial vehicle, check that it still
-- exists, respawn if not, and drop players off after the drop off time.

minetest.register_globalstep(function(dtime)
    -- arena_lib.mods[mod].arenas[arenaID]
    for arenaID, arena in pairs(arena_lib.mods["fantasia_royale"].arenas) do
        if arena.in_game and arena.initial_vehicle_exists then
            local vehicle = arena.initial_vehicle_obj_ref
            if vehicle then
                local time_since_start_sec = (minetest.get_us_time() - arena.initial_vehicle_start_time) / 1000000
                if time_since_start_sec > arena.drop_off_time then
                    drop_all_players_from_vehicle(arena, vehicle)
                else
                    -- the vehicle still exists and we still need it. Record the
                    -- vehicle's position in case the vehicle is deleted in the
                    -- future
                    local pos = vehicle:get_pos()
                    arena.initial_vehicle_pos = pos
                end
            else
                -- so the initial vehicle has been deleted. Respawn it if we still need it.
                local num_players_attached = 0
                for p_name, stats in pairs(arena.players) do
                    if not stats.has_dropped_off then
                        num_players_attached = num_players_attached + 1
                    end
                end
                if num_players_attached > 0 then
                    -- Handle it when minetest randomly deletes entities
                    arena.initial_vehicle_obj_ref = spawn_vehicle(arena, arena.initial_vehicle_pos, arena.initial_vehicle_dir, arena.initial_vehicle_vel)
                    attach_players_to_vehicle(arena.initial_vehicle_obj_ref, arena)
                else
                    -- we don't need the initial vehicle anymore. Sets initial_vehicle_exists to false
                    drop_all_players_from_vehicle(arena, nil)
                end
            end
        end
    end
end)


-- let players jump off the initial vehicle
controls.register_on_press(function(player, key)
    local p_name = player:get_player_name()

    if not arena_lib.is_player_playing(p_name, "fantasia_royale") then return end

    if key == "jump" then
        local arena = arena_lib.get_arena_by_player(p_name)

        -- arena.current_time to see whether the game is in the playing phase (contrary to the loading phase)
        if arena.current_time and not arena.players[p_name].has_dropped_off then
            drop_off_player(arena, p_name)
        end
    end
end)




function spawn_vehicle(arena, pos, dir, vel)
    local staticdata = minetest.write_json{
        match_id = arena.matchID,
    }
    local vehicle = minetest.add_entity(pos, "fantasia_royale:initial_vehicle", staticdata)
    vehicle:set_velocity(vel)
    -- make it face the direction it is moving
    vehicle:set_yaw(minetest.dir_to_yaw(dir))

    return vehicle
end



function attach_players_to_vehicle(vehicle, arena)
    for p_name, stats in pairs(arena.players) do
        if not stats.has_dropped_off then
            local player = minetest.get_player_by_name(p_name)
            player:set_attach(vehicle, "", vector.new(10, 10, 10), vector.new(0, 0, 0))
        end
    end
end

-- vehicle: optional, if given will be removed. This is used when the initial
-- vehicle is no longer needed, e.g. after the drop off time or if minetest has
-- deleted the initial vehicle and all players have dropped off already.

-- Note: the initial vehicle will not be removed automatically when all players
-- have dropped off, so that even the last player can see the ship sailing away.

-- still, if the initial vehicle is deleted automatically and it is no longer
-- needed, it will not be respawned again, and this function will be called
-- early.

function drop_all_players_from_vehicle(arena, vehicle)
    for p_name, stats in pairs(arena.players) do
        if not stats.has_dropped_off then
            drop_off_player(arena, p_name)
        end
    end
    if vehicle then
        vehicle:remove()
    end
    arena.initial_vehicle_obj_ref = nil
    arena.initial_vehicle_exists = false
end




function drop_off_player(arena, p_name)
    local player = minetest.get_player_by_name(p_name)
    local class_name = fantasia.get_class(p_name)
    local class_data = classy.get_class_by_name(class_name)
    local aspect = class_data.aspect or {}
    local visual_size = aspect.visual_size or {x = 1, y = 1}
    player:set_properties({visual_size = visual_size})
    player:set_eye_offset(player_api.get_animation(player).eye_height)
    player:set_detach()
    arena.players[p_name].has_dropped_off = true
    -- clear the HUD about dropping off
    arena_lib.HUD_hide("broadcast", p_name)
    fantasia.start_gliding(p_name,arena)
end


