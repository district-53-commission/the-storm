fantasia.create_poi = function(arena, p_name, color, pos)
    -- TODO: create poi's for team members as well
    if not pos.y then
        pos = vector.new(pos.x, arena.ground_level, pos.z)
    end
    if not arena then return end
    if not arena.in_game then return end

    arena.players[p_name].pois[color] = pos
    fantasia.HUD_poi_create(p_name, arena, color, pos)
end

fantasia.remove_poi = function(arena, p_name, color)
    arena.players[p_name].pois[color] = nil
    fantasia.HUD_poi_remove(p_name, color)
end


