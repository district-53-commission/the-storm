local fill_texture = "froyale_poi_fill.png"
local border_texture = "froyale_poi_border.png"
local button_texture = "froyale_poi_button.png"
local button_hovered_texture = "froyale_poi_button_hovered.png"
local button_selected_texture = "froyale_poi_button_selected.png"
local dot_texture = "froyale_poi_dot.png"
local dot_overlay = "froyale_poi_dot_overlay.png"
local trans_dot_texture = "froyale_poi_trans_dot.png"
local trans_dot_overlay = "froyale_poi_trans_dot_overlay.png"

fantasia.poi_colors = {
    green = {hex = "#71aa34", number = 0x71aa34, tool_name = "minimap_poi_green"},
    blue = {hex = "#28ccdf", number = 0x28ccdf, tool_name = "minimap_poi_blue"},
    yellow = {hex = "#f4b41b", number = 0xf4b41b, tool_name = "minimap_poi_yellow"},
    red = {hex = "#e6482e", number = 0xe6482e, tool_name = "minimap_poi_red"},
}


local function get_poi_texture(color) 
    return "(" .. fill_texture .. "^[colorize:" .. fantasia.poi_colors[color].hex .. ")".. "^" .. border_texture
end

-- local function get_poi_escaped_texture(color) 
--     return "((" .. fill_texture .. "\\\\^[colorize\\\\:" .. fantasia.poi_colors[color].hex .. ")".. "\\\\^" .. border_texture .. ")"
-- end

local function get_poi_button_texture(color)
    return "(" .. fill_texture .. "^[colorize:" .. fantasia.poi_colors[color].hex .. ")".. "^" .. button_texture
end

local function get_poi_button_hovered_texture(color) 
    return "(" .. fill_texture .. "^[colorize:" .. fantasia.poi_colors[color].hex .. ")".. "^" .. button_hovered_texture
end

local function get_poi_button_selected_texture(color) 
    return "(" .. fill_texture .. "^[colorize:" .. fantasia.poi_colors[color].hex .. ")".. "^" .. button_selected_texture
end

local function get_poi_dot_texture(color) 
    return "((" .. dot_texture .. "\\\\^[colorize\\\\:" .. fantasia.poi_colors[color].hex .. ")\\\\^" .. dot_overlay .. ")"
end

local function get_poi_trans_dot_texture(color)
    return "((" .. trans_dot_texture .. "\\\\^[colorize\\\\:" .. fantasia.poi_colors[color].hex .. "88" .. ")\\\\^" .. trans_dot_overlay .. ")"
end

for color, _ in pairs(fantasia.poi_colors) do
    fantasia.poi_colors[color].poi_texture = get_poi_texture(color)
    -- fantasia.poi_colors[color].poi_escaped_texture = get_poi_escaped_texture(color)
    fantasia.poi_colors[color].poi_button_texture = get_poi_button_texture(color)
    fantasia.poi_colors[color].poi_button_hovered_texture = get_poi_button_hovered_texture(color)
    fantasia.poi_colors[color].poi_button_selected_texture = get_poi_button_selected_texture(color)
    fantasia.poi_colors[color].poi_dot_texture = get_poi_dot_texture(color)
    fantasia.poi_colors[color].poi_trans_dot_texture = get_poi_trans_dot_texture(color)
end



