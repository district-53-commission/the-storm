local function fill_chests() end



function fantasia.fill_loot(arena)
  local add_node = minetest.add_node

  for i=1, #arena.chests do
    add_node(arena.chests[i].pos, {name="default:chest"})
  end
  fill_chests(arena)
end



function fantasia.generate_chest_id(arena)
	local max_id = 1

	for i = 1, #arena.chests do
		if arena.chests[i].id > max_id then
			max_id = arena.chests[i].id
		end
	end

	return max_id+1
end



function fill_chests(arena)
  local get_meta = minetest.get_meta
  local select_random_treasures = fantasia.select_random_treasures

  for i, chest in pairs(arena.chests) do
    local treasures = select_random_treasures(chest, arena)
    local meta = get_meta(chest.pos)
    local inv = meta:get_inventory()

    inv:set_list("main", {})
    for i=1, #treasures do
      inv:set_stack("main", i, treasures[i])
    end
  end
end