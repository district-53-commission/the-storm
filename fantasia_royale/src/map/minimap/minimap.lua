local add_selection_grid = function(tree) end
local clamp = function(v, min, max) end
local show_minimap_fs = function(p_name, arena) end
local toggle_and_disable = function(arena,p_name,toggle,disable) end
local add_icons = function(arena, p_name, tree) end
local add_storm_circle = function(arena, p_name, tree) end
local get_world_coords_from_map_coords = function(arena, p_name, x, z) end
local get_map_coords_from_world_coords = function(arena, p_name, x, z) end

local form_width = 64
local form_height = 64

fantasia.load_new_minimap = function(path)
    minetest.dynamic_add_media({filepath = path}, function(name) end) -- not really dynamic media, hehe
end


fantasia.get_max_min_zoom = function(arena)

    local map_width = math.max(arena.pos1.x, arena.pos2.x) - math.min(arena.pos1.x, arena.pos2.x)
    local map_height = math.max(arena.pos1.z, arena.pos2.z) - math.min(arena.pos1.z, arena.pos2.z)

    local biggest = math.max(map_width, map_height)
    local smallest = 32

    return biggest, smallest
end


local gui = flow.widgets
function fantasia.show_minimap_fs(p_name, arena)

    local arena_top_left_x = math.min(arena.pos1.x, arena.pos2.x)
    local arena_top_left_z = math.max(arena.pos1.z, arena.pos2.z)



    -- each pixel on the map is a single node

    -- let's center the map on the player's position
    local player = minetest.get_player_by_name(p_name)
    local player_pos = player:get_pos()
    local player_x = (player_pos.x)
    local player_z = (player_pos.z)

    -- update player properties
    arena.players[p_name].minimap_shown = true

    if arena.players[p_name].minimap_centered then
        arena.players[p_name].minimap_center = vector.new(player_x, 0, player_z)
    end

    if not arena.players[p_name].minimap_view_size then
        arena.players[p_name].minimap_view_size = arena.default_view_size
    end

    local center_x = arena.players[p_name].minimap_center.x
    local center_z = arena.players[p_name].minimap_center.z

    local view_size = arena.players[p_name].minimap_view_size

    local player_view_top_left_x = center_x - view_size / 2
    local player_view_top_left_z = center_z + view_size / 2

    local x_offset = arena_top_left_x - player_view_top_left_x
    local z_offset = player_view_top_left_z - arena_top_left_z

    local texture = table.concat({"[combine:", tostring(view_size), "x", tostring(view_size),":", tostring(x_offset), ",",tostring(z_offset),"=","minimap_" .. arena.name .. ".png".."\\^[transformFY\\^[transformFX"})


    local tree = {
        { type = "formspec_version", version = 7 },
        { type = "size", w = form_width, h = form_height, fixed_size = false },
        { type = "position", x = 0.5, y = 0.5 },
        { type = "anchor", x = 0.5, y = 0.5 },
        { type = "padding", x = 0, y = 0 },
        { type = "no_prepend" },
        -- { type = "bgcolor", fullscreen = "neither", bgcolor = "#FFFFFFFF" },
        -- { type = "background", x = 0, y = 0, w = 16, h = 16, texture_name = "blank.png" },
        { type = "image", x = 0, y = 0, w = form_width, h = form_height, texture_name = texture}
    }

    -- controls
    if arena.players[p_name].minimap_pan or
         arena.players[p_name].minimap_zoom_in or
         arena.players[p_name].minimap_zoom_out or
         arena.players[p_name].minimap_poi_red or
         arena.players[p_name].minimap_poi_blue or
         arena.players[p_name].minimap_poi_green or
         arena.players[p_name].minimap_poi_yellow then

        add_selection_grid(tree)
        
    end

    -- add the storm circle

    add_storm_circle(arena, p_name, tree)
    
    add_icons(arena, p_name, tree)


    local controls_size = 4
    local controls_w = 1 * controls_size
    local controls_h = 1 * controls_size
    local controls_x = 16 * controls_size - controls_w * 1.5
    local controls_y = 1 * controls_size
    
    local controls_add = 1.25 * controls_size
    

    -- solar compass

    table.insert(tree,{
        type = "image_button",
        x = controls_x - controls_size, y = controls_y, 
        w = controls_w * 2, h = controls_h * 2,
        texture_name = "froyale_solar_compass.png",
        name = "solar_compass_button",
        drawborder = false,
        pressed_texture_name = "froyale_solar_compass.png"
    })
    controls_y = controls_y + controls_add*2

    -- view all button
    -- center map button hovered
    table.insert(tree,{
        type = "style",
        selectors = {"view_all_button:hovered"},
        props = {fgimg = "froyale_center_map_hovered.png"}
    })
    table.insert(tree,{
        type = "image_button", 
        x = controls_x, y = controls_y, 
        w = controls_w, h = controls_h, 
        texture_name = "froyale_center_map.png",
        name = "view_all_button",
        drawborder = false,
        pressed_texture_name = "froyale_center_map_pressed.png"
    })
    controls_y = controls_y + controls_add


    -- center player
    -- center player button hovered
    table.insert(tree,{
        type = "style",
        selectors = {"center_player_button:hovered"},
        props = {fgimg = "froyale_center_player_hovered.png"}
    })
    table.insert(tree,{
        type = "image_button",
        x = controls_x, y = controls_y, 
        w = controls_w, h = controls_h, 
        texture_name = "froyale_center_player.png",
        name = "center_player_button",
        drawborder = false,
        pressed_texture_name = "froyale_center_player_pressed.png"
    })
    controls_y = controls_y + controls_add


    -- -- poi placer NOTE: COMMENTED OUT UNTIL POI SYSTEM IS FINISHED AND TEAMS ARE ENABLED
    -- local poi_buttons = table.copy(fantasia.poi_colors)
    -- for _, button in pairs(poi_buttons) do
    --     button.w = controls_w/2
    --     button.h = controls_h/2
    -- end
    -- poi_buttons["red"].x = controls_x
    -- poi_buttons["red"].y = controls_y
    -- poi_buttons["blue"].x = controls_x + controls_w/2
    -- poi_buttons["blue"].y = controls_y
    -- poi_buttons["green"].x = controls_x
    -- poi_buttons["green"].y = controls_y + controls_h/2
    -- poi_buttons["yellow"].x = controls_x + controls_w/2
    -- poi_buttons["yellow"].y = controls_y + controls_h/2


    -- for color, button in pairs(poi_buttons) do
    --     local poi_button_texture = button.poi_button_texture

    --     if arena.players[p_name][button.tool_name] then
    --         poi_button_texture = button.poi_button_selected_texture
    --     end
    --     -- style for hovered

    --     table.insert(tree, {
    --         type = "style",
    --         selectors = {button.tool_name .. ":hovered"},
    --         props = {fgimg = button.poi_button_hovered_texture}
    --     })

    --     table.insert(tree, {
    --         type = "image_button",
    --         x = button.x, y = button.y, 
    --         w = button.w, h = button.h, 
    --         texture_name = poi_button_texture,
    --         name = button.tool_name,
    --         drawborder = false,
    --     })
    -- end

    -- controls_y = controls_y + controls_add

    -- local pan_texture = "froyale_pan_tool.png"
    -- if arena.players[p_name].minimap_pan then
    --     pan_texture = "froyale_pan_tool_selected.png"
    -- end
    -- -- style for hovered
    -- table.insert(tree, {
    --     type = "style",
    --     selectors = {"pan_button:hovered"},
    --     props = {fgimg = "froyale_pan_tool_hovered.png"}
    -- })
    -- table.insert(tree, {
    --     type = "image_button", 
    --     x = controls_x, y = controls_y, 
    --     w = controls_w, h = controls_h, 
    --     texture_name = pan_texture,
    --     name = "pan_button",
    --     drawborder = false,
    -- })
    -- controls_y = controls_y + controls_add

    -- local zoom_in_texture = "froyale_zoom_in.png"
    -- if arena.players[p_name].minimap_zoom_in then
    --     zoom_in_texture = "froyale_zoom_in_selected.png"
    -- end

    -- -- style for hovered
    -- table.insert(tree, {
    --     type = "style",
    --     selectors = {"zoom_in_button:hovered"},
    --     props = {fgimg = "froyale_zoom_in_hovered.png"}
    -- })
    -- table.insert(tree, {
    --     type = "image_button", 
    --     x = controls_x, y = controls_y, 
    --     w = controls_w, h = controls_h, 
    --     texture_name = zoom_in_texture,
    --     name = "zoom_in_button",
    --     drawborder = false,
    -- })
    -- controls_y = controls_y + controls_add

    -- local biggest, smallest = fantasia.get_max_min_zoom(arena)
    -- local step = (biggest - smallest) / 100
    -- table.insert(tree,{
    --     type = "scrollbaroptions",
    --     opts = {
    --         min = smallest,
    --         max = biggest,
    --         smallstep = step,
    --         largestep = step*2,
    --         thumbsize = 3,
    --         arrows = "show"
    --     }
    -- })

    -- table.insert(tree,{
    --     type = "scrollbar",
    --     x = controls_x+.33 * controls_size, y = controls_y,
    --     w = .33 * controls_size, h = 4 * controls_size,
    --     orientation = "vertical",
    --     name = "zoom_scrollbar",
    --     value = clamp(arena.players[p_name].minimap_view_size, smallest, biggest),
    -- })

    -- controls_y = controls_y + 4.25 * controls_size

    -- local zoom_out_texture = "froyale_zoom_out.png"
    -- if arena.players[p_name].minimap_zoom_out then
    --     zoom_out_texture = "froyale_zoom_out_selected.png"
    -- end
    -- -- style for hovered
    -- table.insert(tree, {
    --     type = "style",
    --     selectors = {"zoom_out_button:hovered"},
    --     props = {fgimg = "froyale_zoom_out_hovered.png"}
    -- })
    -- table.insert(tree, {
    --     type = "image_button", 
    --     x = controls_x, y = controls_y, 
    --     w = controls_w, h = controls_h, 
    --     texture_name = zoom_out_texture,
    --     name = "zoom_out_button",
    --     drawborder = false,
    -- })
    -- controls_y = controls_y + controls_add

    formspec_ast.show_formspec(p_name, "fantasia_royale:minimap", tree)
end





-- arena step to update center of the minimap when players have it shown and centered on the player
fantasia.register_on_time_tick("minimap_update", function(arena)
    for pl_name, stats in pairs(arena.players) do
        if stats.minimap_shown then
            show_minimap_fs(pl_name, arena)
        end
    end
end)



minetest.register_on_player_receive_fields(function(player, formname, fields) 
    local p_name = player:get_player_name()
    if not formname == "fantasia_royale:minimap" then return end
    local mod = arena_lib.get_mod_by_player(p_name)
    if mod ~= "fantasia_royale" then return end
    local arena = arena_lib.get_arena_by_player(p_name)
    if not arena then return end

    if fields.quit then
        if arena.players[p_name] then
            arena.players[p_name].minimap_shown = false
            minetest.close_formspec(p_name, "")
        end
        return
    end

    if fields.center_player_button then
        -- center the minimap on the player and set the view size to the default view size
        arena.players[p_name].minimap_centered = true
        arena.players[p_name].minimap_view_size = arena.default_view_size
        show_minimap_fs(p_name, arena)
    end

    if fields.view_all_button then
        -- center the minimap on the center of the arena and set the view size to the largest possible
        arena.players[p_name].minimap_centered = false
        local arena_center = vector.new(
            (arena.pos1.x + arena.pos2.x) / 2,
            0,
            (arena.pos1.z + arena.pos2.z) / 2
        )
        arena.players[p_name].minimap_center = arena_center
        local map_width = math.max(arena.pos1.x, arena.pos2.x) - math.min(arena.pos1.x, arena.pos2.x)
        local map_height = math.max(arena.pos1.z, arena.pos2.z) - math.min(arena.pos1.z, arena.pos2.z)
        arena.players[p_name].minimap_view_size = math.max(map_width, map_height)
        show_minimap_fs(p_name, arena)
    end

    local tools = { 
        "minimap_pan", 
        "minimap_zoom_in", 
        "minimap_zoom_out", 
        "minimap_poi_red", 
        "minimap_poi_green", 
        "minimap_poi_blue", 
        "minimap_poi_yellow" 
    }


    local get_disable_list = function(tool)
        local index_of = function (list, val) for k, v in pairs(list) do if v == val then return k end end end
        local list = table.copy(tools)
        table.remove(list, index_of(list, tool))
        return list
    end
    if fields.pan_button then
        -- toggle pan function
        toggle_and_disable(arena, p_name, "minimap_pan", get_disable_list("minimap_pan"))
        show_minimap_fs(p_name, arena)
    end

    if fields.zoom_in_button then
        -- toggle zoom in function
        toggle_and_disable(arena, p_name, "minimap_zoom_in", get_disable_list("minimap_zoom_in"))
        show_minimap_fs(p_name, arena)
    end

    if fields.zoom_out_button then
        -- toggle zoom out function
        toggle_and_disable(arena, p_name, "minimap_zoom_out", get_disable_list("minimap_zoom_out"))
        show_minimap_fs(p_name, arena)
    end

    if fields.zoom_scrollbar then
        local exp = minetest.explode_scrollbar_event(fields.zoom_scrollbar)
        if exp.type == "CHG" then
            arena.players[p_name].minimap_view_size = exp.value
            show_minimap_fs(p_name, arena)
        end
    end

    for i, v in pairs(fields) do
        if string.find(i, "minimap_poi_") then
            -- decode the color
            local parts = string.split(i, "_")
            local poi_color = parts[3]
            toggle_and_disable(arena, p_name, i, get_disable_list(i))
            show_minimap_fs(p_name, arena)
        end
    end

    for i, v in pairs(fields) do
        if string.find(i, "selectionGrid") then
            -- decode the coordinates
            local parts = string.split(i, "_")
            local button_x = tonumber(parts[2])
            local button_z = tonumber(parts[3])

            -- calculate the world coordinates
            local world_x, world_z = get_world_coords_from_map_coords(arena, p_name, button_x, button_z)
            local biggest, smallest = fantasia.get_max_min_zoom(arena)
            -- handle the grid selection action
            if arena.players[p_name].minimap_pan then
                -- center the view on the selected location
                arena.players[p_name].minimap_centered = false
                arena.players[p_name].minimap_center = vector.new(world_x, 0, world_z)

            elseif arena.players[p_name].minimap_zoom_in then
                -- handle zoom in action
                -- if players are using the zoom in tool then they want to center the view on the selected location and zoom in
                arena.players[p_name].minimap_centered = false
                arena.players[p_name].minimap_center = vector.new(world_x, 0, world_z)
                local minimap_view_size = math.floor(arena.players[p_name].minimap_view_size/2)
                arena.players[p_name].minimap_view_size = clamp(minimap_view_size, smallest, biggest)
            elseif arena.players[p_name].minimap_zoom_out then
                -- handle zoom out action
                -- if players are using the zoom out tool then they want to center the view on the selected location and zoom out
                arena.players[p_name].minimap_centered = false
                arena.players[p_name].minimap_center = vector.new(world_x, 0, world_z)
                local minimap_view_size = math.floor(arena.players[p_name].minimap_view_size*2)
                arena.players[p_name].minimap_view_size = clamp(minimap_view_size, smallest, biggest)
            elseif arena.players[p_name].minimap_poi_red or arena.players[p_name].minimap_poi_blue or arena.players[p_name].minimap_poi_green  or arena.players[p_name].minimap_poi_yellow then
                local color = ""
                if arena.players[p_name].minimap_poi_red then
                    color = "red"
                elseif arena.players[p_name].minimap_poi_blue then
                    color = "blue"
                elseif arena.players[p_name].minimap_poi_green then
                    color = "green"
                elseif arena.players[p_name].minimap_poi_yellow then
                    color = "yellow"
                end

                local world_x, world_z = get_world_coords_from_map_coords(arena, p_name, button_x, button_z)
                fantasia.create_poi(arena, p_name, color, vector.new(world_x, arena.ground_level, world_z))
                -- TODO, this will involve adding a new POI HUD. We need the POI API first
            end

            show_minimap_fs(p_name, arena)
        end
    end

    -- allow removing pois
    for i, v in pairs(fields) do
        if string.find(i, "poi_remove_") then
            local parts = string.split(i, "_")
            local poi_color = parts[3]
            fantasia.remove_poi(arena, p_name, poi_color)

            show_minimap_fs(p_name, arena)
        end
    end

end)





local path = minetest.get_worldpath() .. DIR_DELIM .. "fantasia_royale" .. DIR_DELIM .. "minimap"


-- load any images in the directory
local function load_minimaps()

    local content = minetest.get_dir_list(path)
  
    if not next(content) then
        minetest.mkdir(path)
    else
      local function load_dir(dir)
        for _, f_name in pairs(minetest.get_dir_list(dir, false)) do
            minetest.debug("Loading minimap: " .. dir .. "/" .. f_name)
            minetest.dynamic_add_media({filepath = dir .. "/" .. f_name, filename = f_name}, function(name) end) -- not really dynamic media, hehe
        end
      end
  
      -- dynamic media can't be added when the server starts
      minetest.after(0.1, function()
        load_dir(path)
      end)
    end
end

load_minimaps()

function add_selection_grid(tree)

    for x = 0,63 do
        for z = 0,63 do
            -- hovered: froyale_location_hovered.png
            -- normal: blank.png
            -- pressed: froyale_location_pressed.png
            local button_name = "selectionGrid_".. x .. "_" .. z
            table.insert(tree, {
                type = "style",
                selectors = {button_name .. ":hovered"},
                props = {fgimg = "froyale_location_hovered.png"}
            })
            table.insert(tree, {
                type = "style",
                selectors = {button_name .. ":pressed"},
                props = {fgimg = "froyale_location_pressed.png"}
            })
            table.insert(tree, {
                type = "image_button", 
                x = x, y = z, 
                w = 1, h = 1, 
                texture_name = "blank.png",
                name = button_name,
                drawborder = false,
            })

        end


    end
end
function add_icons(arena, p_name, tree)
    local function add_icon_to_tree(icon_pos, texture, name)
        local map_x, map_z = get_map_coords_from_world_coords(arena, p_name, icon_pos.x, icon_pos.z)
        if map_x < 0 or map_x > 63 or map_z < 0 or map_z > 63 then
            return
        end
        table.insert(tree, {
            type = "image_button",
            x = map_x, y = map_z, 
            w = 1, h = 1,
            texture_name = texture,
            name = name,
            drawborder = false,
        })
    end
    local player = minetest.get_player_by_name(p_name)
    if not player then return end
    local yaw = player:get_look_horizontal() -- angle in radians is counter-clockwise from north

    local pi = math.pi

    local dir = (16 - math.floor((yaw + pi / 16) / (pi / 8))) % 16

    local direction_sprite = "froyale_player_dir_sheet.png^[sheet:16x1:" .. dir .. ",0"

    local player_pos = vector.apply(player:get_pos(), function(x) return math.floor(x) end)
    
    add_icon_to_tree(player_pos, direction_sprite, "player_location_button")

    -- add icons for POIs
    for color, pos in pairs(arena.players[p_name].pois) do
        local button_name = "poi_remove_" .. color
        add_icon_to_tree(pos, fantasia.poi_colors[color].poi_texture, button_name)
    end

end

function get_map_info_for_storm_location(arena, p_name, center, radius)

    local ret = {}

    ret.storm_center = center
    ret.storm_radius = radius
    ret.storm_top_left_x = ret.storm_center.x - ret.storm_radius
    ret.storm_top_left_z = ret.storm_center.z + radius
    ret.storm_bottom_right_x = ret.storm_center.x + radius
    ret.storm_bottom_right_z = ret.storm_center.z - radius
    
    ret.storm_top_left_x_map, ret.storm_top_left_z_map = get_map_coords_from_world_coords(arena, p_name, ret.storm_top_left_x, ret.storm_top_left_z)
    ret.storm_bottom_right_x_map, ret.storm_bottom_right_z_map = get_map_coords_from_world_coords(arena, p_name, ret.storm_bottom_right_x, ret.storm_bottom_right_z)
    ret.storm_map_width = ret.storm_bottom_right_x_map - ret.storm_top_left_x_map
    ret.storm_map_height = ret.storm_bottom_right_z_map - ret.storm_top_left_z_map

    return ret
end

function add_storm_circle(arena, p_name, tree)
    

    local storm_info = get_map_info_for_storm_location(arena, p_name, arena.storm_center, arena.storm_radius)


    table.insert(tree,{
        type = "image",
        x = storm_info.storm_top_left_x_map, y = storm_info.storm_top_left_z_map,
        w = storm_info.storm_map_width, h = storm_info.storm_map_height,
        texture_name = "froyale_minimap_storm.png"
    })

    
    -- Add colored fill on all sides of the storm circle image
    local fills = {}
    
    if storm_info.storm_top_left_z_map > 0 then
        -- top fill
        table.insert(fills, {
            x = 0, y = 0,
            w = form_width, h = storm_info.storm_top_left_z_map,
            texture_name = "froyale_minimap_storm_fill.png"
        })
    end

    if storm_info.storm_top_left_x_map > 0 then
        -- left fill
        table.insert(fills, {
            x = 0, y = storm_info.storm_top_left_z_map,
            w = storm_info.storm_top_left_x_map, h = storm_info.storm_map_height,
            texture_name = "froyale_minimap_storm_fill.png"
        })
    end

    if storm_info.storm_bottom_right_z_map < form_height then
        -- bottom fill
        table.insert(fills, {
            x = 0, y = storm_info.storm_bottom_right_z_map,
            w = form_width, h = form_height - storm_info.storm_bottom_right_z_map,
            texture_name = "froyale_minimap_storm_fill.png"
        })
    end

    if storm_info.storm_bottom_right_x_map < form_width then
        -- right fill
        table.insert(fills, {
            x = storm_info.storm_bottom_right_x_map, y = storm_info.storm_top_left_z_map,
            w = form_width - storm_info.storm_bottom_right_x_map, h = storm_info.storm_map_height,
            texture_name = "froyale_minimap_storm_fill.png"
        })
    end

    for _, fill in ipairs(fills) do
        table.insert(tree, {
            type = "image",
            x = fill.x, y = fill.y,
            w = fill.w, h = fill.h,
            texture_name = fill.texture_name
        })
    end

    if arena.rounds_complete >= 1 and arena.players[p_name].can_view_storm_target_1 then
        local target_info = get_map_info_for_storm_location(arena, p_name, arena.next_center, arena.next_radius)

        table.insert(tree,{
            type = "image",
            x = target_info.storm_top_left_x_map, y = target_info.storm_top_left_z_map,
            w = target_info.storm_map_width, h = target_info.storm_map_height,
            texture_name = "froyale_minimap_storm_target_1.png"
        })
    
        if arena.players[p_name].can_view_storm_target_2 then
            local target_info2 = get_map_info_for_storm_location(arena, p_name, arena.next_center2, arena.next_radius2)

            table.insert(tree,{
                type = "image",
                x = target_info2.storm_top_left_x_map, y = target_info2.storm_top_left_z_map,
                w = target_info2.storm_map_width, h = target_info2.storm_map_height,
                texture_name = "froyale_minimap_storm_target_2.png"
            })

        end
    
    end
    
end

    

function clamp(v, min, max)
    return math.max(min, math.min(v, max))
end

function toggle_and_disable(arena,p_name,toggle,disable) 
    arena.players[p_name][toggle] = not arena.players[p_name][toggle]
    for k,v in pairs(disable) do
        arena.players[p_name][v] = false
    end
end

show_minimap_fs = fantasia.show_minimap_fs

get_world_coords_from_map_coords = function(arena, p_name, map_x, map_z) 
    -- calculate the world coordinates
    local center_x = arena.players[p_name].minimap_center.x
    local center_z = arena.players[p_name].minimap_center.z

    local view_size = arena.players[p_name].minimap_view_size

    local player_view_top_left_x = center_x - view_size / 2
    local player_view_top_left_z = center_z + view_size / 2

    -- each button x _ z is 1/64 of the view size
    local step_x = view_size / 64
    local step_z = view_size / 64

    local world_x = player_view_top_left_x + map_x * step_x
    local world_z = player_view_top_left_z - map_z * step_z

    return world_x, world_z
end

get_map_coords_from_world_coords = function(arena, p_name, world_x, world_z)
    -- retrieve player-specific data
    local center_x = arena.players[p_name].minimap_center.x
    local center_z = arena.players[p_name].minimap_center.z

    local view_size = arena.players[p_name].minimap_view_size

    local player_view_top_left_x = center_x - view_size / 2
    local player_view_top_left_z = center_z + view_size / 2

    -- each button x _ z is 1/64 of the view size
    local step_x = view_size / 64
    local step_z = view_size / 64

    -- calculate the map coordinates
    local map_x = (world_x - player_view_top_left_x) / step_x
    local map_z = (player_view_top_left_z - world_z) / step_z

    return map_x, map_z
end
