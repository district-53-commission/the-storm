local function on_section_use_give_tools(itemstack, user, pointed_thing) end
local function set_num_property(itemstack, user, pointed_thing, property) end

local tool_sections = {
    -- there can be up to 9 tools per section. The 10th is the return button. Commented out are some tools that should be implemented.

    ["fantasia_royale:chests_editor_section"] = {
        -- this section should show the locations of all chests when it is opened
        "fantasia_royale:treasures_editor_section",
        "fantasia_royale:show_chest_locations",
        "fantasia_royale:add_chest",
        "fantasia_royale:edit_chest",
        "fantasia_royale:remove_chest",
    },



    ["fantasia_royale:map_editor_section"] = {
        "fantasia_royale:save_minimap",
        "fantasia_royale:set_kill_level",
        "fantasia_royale:set_ground_level",
        "fantasia_royale:set_starting_storm_radius",
        "fantasia_royale:save_heightmap",
    }


}

arena_lib.register_editor_section("fantasia_royale", {
    name = "Fantasia Properties",
    icon = "froyale_icon.png",
    give_items = function(itemstack, user, arena)
        local tools = {}
        for itemname, _ in pairs(tool_sections) do
            table.insert(tools, itemname)
        end
        return tools
    end
})



-- Section tools
minetest.register_tool("fantasia_royale:chests_editor_section", {
    description = "Edit Chests and Treasures",
    inventory_image = "froyale_chests_editor.png",
    wield_image = "froyale_chests_editor.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        on_section_use_give_tools(itemstack, user, pointed_thing)
    end
})


minetest.register_tool("fantasia_royale:map_editor_section", {
    description = "Edit Fantasia Map Properties",
    inventory_image = "froyale_editor_map.png",
    wield_image = "froyale_editor_map.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        on_section_use_give_tools(itemstack, user, pointed_thing)
    end
})

-- action tools

minetest.register_tool("fantasia_royale:show_chest_locations", {
    description = "Show Chest Locations",
    inventory_image = "froyale_show_chest_locations.png",
    wield_image = "froyale_show_chest_locations.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(user)
        if not success then return end

        local p_name = user:get_player_name()
        fantasia.show_chests(p_name, arena)

        -- switch back to hide_chest_locations
        return ItemStack("fantasia_royale:hide_chest_locations")
    end
})


minetest.register_tool("fantasia_royale:hide_chest_locations", {
    description = "Show Chest Locations",
    inventory_image = "froyale_hide_chest_locations.png",
    wield_image = "froyale_hide_chest_locations.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(user)
        if not success then return end

        local p_name = user:get_player_name()
        fantasia.hide_chests(p_name, arena)

        -- switch back to show_chest_locations
        return ItemStack("fantasia_royale:show_chest_locations")
    end
})



minetest.register_tool("fantasia_royale:add_chest", {
    description = "Add Chest",
    inventory_image = "froyale_add_chest.png",
    wield_image = "froyale_add_chest.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(user)
        if not success then return end

        if pointed_thing and pointed_thing.type == "node" then
            local pos = pointed_thing.above
            local i, chest_def = fantasia.get_chest_by_pos(arena, pos)
            if i then
                fantasia.print_error(user:get_player_name(), "There is already a chest here!")
                return
            end
            -- see editor_gui.lua
            fantasia.add_chest_form(user, pos)
        end
    end
})



minetest.register_tool("fantasia_royale:edit_chest", {
    description = "Edit Chest",
    inventory_image = "froyale_edit_chest.png",
    wield_image = "froyale_edit_chest.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(user)
        if not success then return end

        if pointed_thing and pointed_thing.type ~= "object" then
            fantasia.print_error(user:get_player_name(), "You must point to a chest marker entity! If you can't see one, try showing using the show chests tool.")
            return
        end
        if pointed_thing and pointed_thing.type == "object" then
            local chest_obj = pointed_thing.ref
            if chest_obj:get_luaentity() and chest_obj:get_luaentity().name == "fantasia_royale:chest_pos_ent" then
                local pos = chest_obj:get_pos()
                local i, chest_def = fantasia.get_chest_by_pos(arena, pos)
                local chest_id = chest_def.id
                -- see editor_gui.lua
                fantasia.edit_chest_form(user, chest_id)
            else
                fantasia.print_error(user:get_player_name(), "That is not a chest marker entity!")
            end
        end
    end
})



minetest.register_tool("fantasia_royale:treasures_editor_section", {
    description = "Edit Treasures",
    inventory_image = "froyale_treasures_editor_section.png",
    wield_image = "froyale_treasures_editor_section.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(user)
        if not success then return end
        -- see editor_gui.lua
        fantasia.edit_treasures(user, arena)
    end
})


minetest.register_tool("fantasia_royale:remove_chest", {
    description = "Remove Chest",
    inventory_image = "froyale_remove_chest.png",
    wield_image = "froyale_remove_chest.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(user)
        if not success then return end
        if pointed_thing and pointed_thing.type == "object" then
            local chest_obj = pointed_thing.ref
            if chest_obj:get_luaentity() and chest_obj:get_luaentity().name == "fantasia_royale:chest_pos_ent" then
                local pos = chest_obj:get_pos()
                local i, chest_def = fantasia.get_chest_by_pos(arena, pos)
                if i then
                    fantasia.hide_chest_location(user, arena, chest_def.id)
                    table.remove(arena.chests, i)
                    arena_lib.change_arena_property(user:get_player_name(), "fantasia_royale", arena.name, "chests", dump(arena.chests), true)
                else
                    fantasia.print_error(user:get_player_name(), "Chest not found in arena! (this is a bug)")
                end
            else
                fantasia.print_error(user:get_player_name(), "That is not a chest marker entity!")
            end
        end
    end
})


minetest.register_tool("fantasia_royale:save_minimap", {
    description = "Save Minimap",
    inventory_image = "froyale_save_minimap.png",
    wield_image = "froyale_save_minimap.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(user)
        if not success then return end
        fantasia.save_minimap(user:get_player_name(), arena)
    end
})



minetest.register_tool("fantasia_royale:set_starting_storm_radius", {
    description = "Set starting storm radius (0 for automatic calculation)",
    inventory_image = "froyale_set_starting_storm_radius.png",
    wield_image = "froyale_set_starting_storm_radius.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        fantasia.set_num_property( user, "starting_storm_radius")
    end
})


minetest.register_tool("fantasia_royale:set_ground_level", {
    description = "Set Ground Level (REQUIRED)",
    inventory_image = "froyale_set_ground_level.png",
    wield_image = "froyale_set_ground_level.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(user)
        if not success then return end
        local y = math.floor(user:get_pos().y)
        arena_lib.change_arena_property(user:get_player_name(), mod, arena_name, "ground_level", math.floor(y), true)
        minetest.chat_send_player(user:get_player_name(), minetest.colorize("#f47e1b", ("Ground level set to " .. y)))
    end
})




minetest.register_tool("fantasia_royale:save_heightmap", {
    description = "Save Heightmap (REQUIRED)",
    inventory_image = "froyale_editor_save_heightmap.png",
    wield_image = "froyale_editor_save_heightmap.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(user)
        if not success then return end
        local error = fantasia.save_heightmap(arena)
        if error then
            minetest.chat_send_player(user:get_player_name(), minetest.colorize("#f47e1b", error))
            return
        end
        minetest.chat_send_player(user:get_player_name(), minetest.colorize("#f47e1b", ("Heightmap saved!")))
    end
})



minetest.register_tool("fantasia_royale:set_kill_level", {
    description = "Set Kill Level (REQUIRED)",
    inventory_image = "froyale_set_kill_level.png",
    wield_image = "froyale_set_kill_level.png",
    groups = {not_in_creative_inventory = 1},
    on_place = function() end,
    on_drop = function() end,
    on_use = function(itemstack, user, pointed_thing)
        local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(user)
        if not success then return end
        local y = math.floor(user:get_pos().y)
        arena_lib.change_arena_property(user:get_player_name(), mod, arena_name, "kill_level", math.floor(y), true)
        minetest.chat_send_player(user:get_player_name(), minetest.colorize("#f47e1b", ("Kill level set to " .. y)))
    end
})


function on_section_use_give_tools(itemstack, user, pointed_thing)
    local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(user)
    if not success then return end
    local p_name = user:get_player_name()
    local tools = {"","","","","","","","","","arena_lib:editor_return"}
    local i_name = itemstack:get_name()
    local section_tools = tool_sections[i_name]
    for i = 1, #section_tools do
        tools[i] = section_tools[i]

        -- special case for hide_chests
        if i_name == "fantasia_royale:chests_editor_section" then
            if i == 2 then
                if fantasia.editor_chests_shown[p_name] then
                    tools[i] = "fantasia_royale:hide_chest_locations"
                end
            end
        end
    end

    user:get_inventory():set_list("main", tools)
end






minetest.register_on_joinplayer(function(player)
    -- remove any control items
    local inv = player:get_inventory()
    local list = inv:get_list("main")
    for _, item in pairs(list) do
        for section_item, section_list in pairs(tool_sections) do
            if item:get_name() == section_item then
                inv:remove_item("main", item)
            end
            for i = 1, #section_list do
                if item:get_name() == section_list[i] then
                    inv:remove_item("main", item)
                end
            end
            if item:get_name() == "fantasia_royale:hide_chest_locations" then
                inv:remove_item("main", item)
            end
        end
    end
end)