
local chest_location_HUD_ids = {} -- p_name indexed table of tables offset chest location HUD ids

-- returns the index of the chest in the chest list and the chest definition

function fantasia.show_chest_location(player, arena, chest_id)
    local p_name = player:get_player_name()
    local chests = arena.chests

    local i, chest_def = fantasia.get_chest_by_id(arena, chest_id)

    if not i then return end

    -- add HUD
    local chest_pos = chest_def.pos

    if not chest_location_HUD_ids[p_name] then chest_location_HUD_ids[p_name] = {} end

    if chest_location_HUD_ids[p_name][chest_id] then
        player:hud_remove(chest_location_HUD_ids[p_name][chest_id])
    end

    local color = fantasia.chest_colors[chest_def.color]
    -- black color if not defined
    if not color then
        color = 0x302c2e
    else
        color = color.number
    end
    -- add HUD
    chest_location_HUD_ids[p_name][chest_id] = player:hud_add({
        name = "Chest ".. chest_id,
        hud_elem_type = "waypoint",
        precision = 0,
        world_pos = chest_pos,
        number = color,
    })

    -- add entity marker
    local staticdata = minetest.write_json{
        _arena = arena.name,
        _chest_id = chest_id,
        _player = p_name
    }
    local obj = minetest.add_entity(chest_pos, "fantasia_royale:chest_pos_ent", staticdata)

end

function fantasia.hide_chest_location(player, arena, chest_id)
    local p_name = player:get_player_name()
    if chest_location_HUD_ids[p_name] then
        local i, chest_def = fantasia.get_chest_by_id(arena, chest_id)
        if not i then return end
        player:hud_remove(chest_location_HUD_ids[p_name][chest_id])
        -- remove entity marker
        local pos = chest_def.pos

        chest_location_HUD_ids[p_name][chest_id] = nil
    end
end

function fantasia.hide_all_chest_locations(player, arena)
    local p_name = player:get_player_name()
    if chest_location_HUD_ids[p_name] then
        for chest_id, _ in pairs(chest_location_HUD_ids[p_name]) do
            fantasia.hide_chest_location(player, arena, chest_id)
        end
        chest_location_HUD_ids[p_name] = nil
    end
end



