
-- returns boolean success, mod, arena_name, id, arena
function fantasia.get_editor_arena_info(user)
    local mod = user:get_meta():get_string("arena_lib_editor.mod")
    if not mod then return false end
    local arena_name = user:get_meta():get_string("arena_lib_editor.arena")
    if not arena_name then return false end
    local id, arena = arena_lib.get_arena_by_name(mod, arena_name)
    if not arena then return false end
    return true, mod, arena_name, id, arena
end



function fantasia.get_chest_by_id(arena, chest_id) 
    for i, chest_def in pairs(arena.chests) do
        if chest_def.id == chest_id then
            return i, chest_def
        end
    end
end

function fantasia.get_chest_by_pos(arena, pos)
    pos = vector.round(pos)
    for i, chest_def in pairs(arena.chests) do
        local c_pos = vector.round(vector.new(chest_def.pos.x, chest_def.pos.y, chest_def.pos.z))
        if c_pos.x == pos.x and c_pos.y == pos.y and c_pos.z == pos.z then
            return i, chest_def
        end
    end
end


fantasia.editor_chests_shown = {}

function fantasia.hide_chests(p_name, arena)
    local player = minetest.get_player_by_name(p_name)
    if not player then return end
    if not fantasia.editor_chests_shown[p_name] then
        return
    end
    fantasia.hide_all_chest_locations(player, arena)
    fantasia.editor_chests_shown[p_name] = nil
end


function fantasia.show_chests(p_name, arena)
    local player = minetest.get_player_by_name(p_name)
    if not player then return end

    if fantasia.editor_chests_shown[p_name] then
        fantasia.hide_chests(p_name, arena)
    end

    for i, chest_def in pairs(arena.chests) do
        fantasia.show_chest_location(player, arena, chest_def.id)
    end
    fantasia.editor_chests_shown[p_name] = true
end

fantasia.save_minimap = function(p_name, arena)
    if not arena.pos1 or not arena.pos2 then
        fantasia.print_error(p_name, "Arena region is not set!")
    end
    minetest.chat_send_player(p_name, "Saving minimap, this may take a while...")
    local start_time = os.clock()
    -- render and save to world-directory
    local png = isogen.draw_map(arena.pos1, arena.pos2)
    local path = minetest.get_worldpath() .. DIR_DELIM .. "fantasia_royale" .. DIR_DELIM .. "minimap" .. DIR_DELIM .. "minimap_" .. arena.name .. ".png"
    
    
    minetest.safe_file_write(path, png)
    minetest.after(1, function()
        fantasia.load_new_minimap(path)
    end)
    
    -- show that we are finished and how long it took in sec
    minetest.chat_send_player(p_name, "Minimap saved! " .."[" .. os.clock() - start_time .. " " .. "sec" .. "]")
    minetest.chat_send_player(p_name, "If you are overwriting the minimap, you need to restart the server!")
end