-- p_name - indexed table of booleans. true if the chests are currently shown to the editor p_name

local function check_chest_data_validity(ctx) end
local function get_color_idx(color) end

local gui = flow.widgets

-- inventory for adding a treasure

local add_invs = {}

function fantasia.edit_treasures(player, arena)
    -- this should open up a form to edit the treasures
    local edit_treasures_gui = flow.make_gui(function(player, ctx)
        
        -- create or get the add_inv for the player.
        local p_name = player:get_player_name()
        local inv = minetest.create_detached_inventory("fantasia_add_inv_" .. p_name)
        inv:set_size("input", 1)

        local function get_treasure_hbox(treasure_def)

            return gui.HBox{
                gui.ItemImage({w =.75, h=.75, item_name = treasure_def.name}),

                gui.label{label = "Count: " .. treasure_def.count},
                gui.label{label = "Preciousness: " .. treasure_def.preciousness},
                gui.label{label = "Rarity: " .. treasure_def.rarity},

                gui.Button{
                    label = "X",
                    on_event = function(player, ctx)
                        local new_treasures = arena.treasures
                        for i=1, #new_treasures do
                            if new_treasures[i].id == treasure_def.id then
                                table.remove(new_treasures, i)
                                break
                            end
                        end
                        arena.treasures = new_treasures
                        fantasia.reorder_treasures(arena)
                        --saves the changes to mod storage
                        arena_lib.change_arena_property(p_name, "fantasia_royale", arena.name, "treasures", dump(arena.treasures), true)
                        return true
                    end
                }
                
            }
        end
        -- make the list of treasures
        local scrollable_treasures = {
            name = "treasures_list_scroll",
            h = 5,
            -- content here
        }
        for i=1, #arena.treasures do
            table.insert(scrollable_treasures, get_treasure_hbox(arena.treasures[i]))
        end
        return gui.VBox{
            gui.Label{label = "Edit Treasures"},
            gui.ScrollableVBox(scrollable_treasures),
            gui.Label{label = "Add treasure: "},
            gui.Hbox{

                gui.Vbox{
                    gui.Label{label = "Item"},
                    gui.List{
                        inventory_location = "detached:fantasia_add_inv_"..p_name,
                        list_name = "input",
                        w = 1,
                        h = 1,
                    },
                },
                
                gui.Field{w=2, h=.6, name="count_field", label = "Count (1-99)", default="1"},
                gui.Field{w=2, h=.6, name="preciousness_field", label = "Preciousness", default="1"},
                gui.Field{w=2, h=.6, name="rarity_field", label = "Rarity (1-10)", default="1"},
                gui.Button{
                    label = "Add",
                    on_event = function(player, ctx)
                        -- add the treasure
                        local fantasia_inv = minetest.get_inventory({type = "detached", name = "fantasia_add_inv_"..p_name})
                        local treasure_stack = fantasia_inv:get_stack("input", 1)
                        if treasure_stack:is_empty() then
                            fantasia.print_error(p_name, "No item added!")
                            return
                        end
                        if not tonumber(ctx.form.count_field) or not tonumber(ctx.form.preciousness_field) or not tonumber(ctx.form.rarity_field) then
                            fantasia.print_error(p_name, "All fields have to be numbers!")
                            return
                        end

                        local treasure_name = fantasia_inv:get_stack("input", 1):get_name()
                        local count = tonumber(ctx.form.count_field)
                        local preciousness = tonumber(ctx.form.preciousness_field)
                        local rarity = tonumber(ctx.form.rarity_field)
                        if count < 1 then
                            fantasia.print_error(p_name, "Count has to be greater than 0!")
                            return
                        elseif rarity < 1 or rarity > 10 then
                            fantasia.print_error(p_name, "Rarity has to be between 1 and 10!")
                            return
                        end
                
                        local item_id = fantasia.generate_treasure_id(arena)
                        local treasure = {
                            name = treasure_name,
                            rarity = rarity,
                            count = count,
                            preciousness = preciousness,
                            id = item_id
                        }
                        table.insert(arena.treasures, treasure)
                
                        fantasia.reorder_treasures(arena)

                        arena_lib.change_arena_property(p_name, "fantasia_royale", arena.name, "treasures", dump(arena.treasures), true)
                        return true
                        
                    end
                },
            },
            gui.Vbox{
                gui.Label{label = "Your Inventory: add things below your hotbar to drop them above"},
                gui.List{
                    inventory_location = "current_player",
                    list_name = "main",
                    w = 10,
                    h = 2,
                    starting_item_index = 10
                },
            },
        }
    end)
    edit_treasures_gui:show(player)
end





-- gui for setting a numeric property
function fantasia.set_num_property(user, property)
    local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(user)
    if not success then return end
    local edit_num_property_gui = flow.make_gui(function(player, ctx)
        ctx.property = ctx.property or property
        return gui.VBox{
            gui.Hbox{
                gui.Label{label = "Set " .. ctx.property .. " to: "},
                gui.Field{w = 1, h = .6, name = "set_num_property", label = "", default = tostring(arena[ctx.property] or 0)},
                gui.ButtonExit{
                    name = "exit_btn",
                    w = 1, h = .6,
                    label = "Set",
                    on_activate = function()
                        local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(player)
                        if not success then return true end
                        
                        arena_lib.change_arena_property(player:get_player_name(), mod, arena_name, ctx.property, tonumber(ctx.form.set_num_property), true)
                        if arena[ctx.property] ~= tonumber(ctx.form.set_num_property) then
                            return false
                        else
                            return true
                        end
                    end
                },
            },
        }
    end)
    edit_num_property_gui:show(user)
end


-- these defaults update when a new chest is added, so you can add multiple
-- chests with the same settings easily.
local default_min_treasures = 1
local default_max_treasures = 5
local default_min_preciousness = 0
local default_max_preciousness = 10
local default_color = "green"

local function update_add_chest_defaults(min_treasures, max_treasures, min_preciousness, max_preciousness, color)
    default_min_treasures = min_treasures
    default_max_treasures = max_treasures
    default_min_preciousness = min_preciousness
    default_max_preciousness = max_preciousness
    default_color = color
end




function fantasia.add_chest_form(player, pos)
    
    local add_chest_form = flow.make_gui(function(player, ctx)
        local p_name = player:get_player_name()
        local color_idx = get_color_idx(default_color)


        return gui.VBox{
            gui.Label{label = "Add Chest settings:"},
            gui.Hbox{
                gui.Field{w=2, h=.6, name="min_treasures", label = "min_treasures", default= default_min_treasures},
                gui.Field{w=2, h=.6, name="max_treasures", label = "max_treasures", default= default_max_treasures},
            },
            gui.Hbox{
                gui.Field{w=2, h=.6, name="min_preciousness", label = "min_preciousness", default= default_min_preciousness},
                gui.Field{w=2, h=.6, name="max_preciousness", label = "max_preciousness", default= default_max_preciousness},
            },

            gui.Dropdown {
                w = 4, -- Optional
                h = .6, -- Optional
                name = "color", -- Optional
                items = {"green", "blue", "yellow"},
                selected_idx = color_idx,
                index_event = false, -- use value not idx
            },
            

            gui.HBox{
                gui.ButtonExit{
                    label = "Cancel",
                    on_event = function(player, ctx)
                        return
                    end
                },
                gui.ButtonExit{
                    label = "Add",
                    align_h = "right",
                    on_event = function(player, ctx)
                        local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(player)
                        if not success then return end

                        local success, min_treasures, max_treasures, min_preciousness, max_preciousness, color = check_chest_data_validity(ctx)
                        if not success then return end

                        local chest_id = fantasia.generate_chest_id(arena)
                        local chest = {
                            pos = pos,
                            min_preciousness = min_preciousness,
                            max_preciousness = max_preciousness,
                            min_treasures = min_treasures,
                            max_treasures = max_treasures,
                            color = color,
                            id = chest_id
                        }
                        table.insert(arena.chests, chest)
                        
                        arena_lib.change_arena_property(p_name, "fantasia_royale", arena.name, "chests", dump(arena.chests), true)

                        -- show the location of the new chest on the HUD
                        if fantasia.editor_chests_shown[p_name] then
                            fantasia.show_chest_location(player, arena, chest_id)
                        end

                        update_add_chest_defaults(min_treasures, max_treasures, min_preciousness, max_preciousness, color)

                        return true
                    end
                }
            }
        }
    end)
    add_chest_form:show(player)
end



fantasia.edit_chest_form = function(player, chest_id)
    local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(player)
    if not success then return end
    local i, chest_data = fantasia.get_chest_by_id(arena, chest_id)

    if not i then return end

    local max_treasures = chest_data.max_treasures
    local min_treasures = chest_data.min_treasures
    local max_preciousness = chest_data.max_preciousness
    local min_preciousness = chest_data.min_preciousness
    local original_color = chest_data.color
    if not original_color then original_color = "green" end
    local color_idx = get_color_idx(original_color)

    local edit_chest_gui = flow.make_gui(function(player, ctx)
        
        return gui.VBox{
            gui.Label{label = "Edit Chest id:" .. chest_id},

            gui.Hbox{
                gui.Field{w=2, h=.6, name="min_treasures", label = "min_treasures", default=min_treasures},
                gui.Field{w=2, h=.6, name="max_treasures", label = "max_treasures", default=max_treasures},
            },
            gui.Hbox{
                gui.Field{w=2, h=.6, name="min_preciousness", label = "min_preciousness", default=min_preciousness},
                gui.Field{w=2, h=.6, name="max_preciousness", label = "max_preciousness", default=max_preciousness},
            },

            gui.Dropdown {
                w = 4, -- Optional
                h = .6, -- Optional
                name = "color", -- Optional
                items = {"green", "blue", "yellow"},
                selected_idx = color_idx,
                index_event = false, -- use value not idx
            },
            

            gui.HBox{
                gui.ButtonExit{
                    label = "Cancel",
                    on_event = function(player, ctx)
                        return
                    end
                },
                gui.ButtonExit{
                    label = "Edit",
                    align_h = "right",
                    on_event = function(player, ctx)
                        local p_name = player:get_player_name()
                        local success, mod, arena_name, id, arena = fantasia.get_editor_arena_info(player)
                        if not success then return end

                        local success, min_treasures, max_treasures, min_preciousness, max_preciousness, color = check_chest_data_validity(ctx)
                        if not success then return end

                        -- hide the chest
                        if fantasia.editor_chests_shown[p_name] then
                            fantasia.hide_chest_location(player, arena, chest_id)
                        end

                        arena.chests[i] = {
                            pos = chest_data.pos,
                            min_preciousness = min_preciousness,
                            max_preciousness = max_preciousness,
                            min_treasures = min_treasures,
                            max_treasures = max_treasures,
                            color = color,
                            id = chest_id
                        }
                        arena_lib.change_arena_property(p_name, mod, arena_name, "chests", dump(arena.chests), true)

                        update_add_chest_defaults(min_treasures, max_treasures, min_preciousness, max_preciousness, color)

                        -- show the location of the new chest on the HUD
                        if fantasia.editor_chests_shown[p_name] then
                            fantasia.show_chest_location(player, arena, chest_id)
                        end
                        
                    end
                }
            }
        }
    end)
    edit_chest_gui:show(player)
end


                        
function check_chest_data_validity(ctx) 
    local min_treasures = tonumber(ctx.form.min_treasures)
    local max_treasures = tonumber(ctx.form.max_treasures)
    local min_preciousness = tonumber(ctx.form.min_preciousness)
    local max_preciousness = tonumber(ctx.form.max_preciousness)
    local color = ctx.form.color
    if not min_treasures or not max_treasures or not min_preciousness or not max_preciousness then
        fantasia.print_error(p_name, "All fields have to be numbers!")
        return false
    end
    if min_treasures <= 0 or max_treasures <= 0 then
        fantasia.print_error(user:get_player_name(), "The minimum or maximum amount of treasures has to be greater than 0!")
        return false
    end
    return true, min_treasures, max_treasures, min_preciousness, max_preciousness, color
end



function get_color_idx(color)
    local color_idx
    
    if color == "green" then
        color_idx = 1
    elseif color == "blue" then
        color_idx = 2
    elseif color == "yellow" then
        color_idx = 3
    end

    return color_idx
end


-- hide the editor chest markers when players leave
arena_lib.register_on_leave_editor(function(mod, arena, p_name)
    if mod ~= "fantasia_royale" then return end

    if fantasia.editor_chests_shown[p_name] then
        fantasia.hide_chests(p_name, arena)
    end
end)