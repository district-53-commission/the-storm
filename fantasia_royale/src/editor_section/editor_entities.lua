-- cjest marker for editor

local tex = {}
for i=1, 6 do
    tex[i] = "froyale_chests_editor.png"
end

minetest.register_entity("fantasia_royale:chest_pos_ent",{
    initial_properties = {
        physical = false,
        use_texture_alpha = true,
        collisionbox = {-.6,-.6,-.6,.6,.6,.6},
        visual = "cube",
        textures = tex,
        visual_size = {x = 1.3, y = 1.3, z = 1.3},
        static_save = false,
    },

    on_activate = function(self, staticdata, dtime_s)
        if staticdata ~= "" and staticdata ~= nil then
            local data = minetest.parse_json(staticdata) or {}
            if data._arena then self._arena = data._arena end
            if data._chest_id then self._chest_id = data._chest_id end
            if data._player then self._player = data._player end
        end
    end,

    get_staticdata = function(self)
        return minetest.write_json{
            _arena = self._arena,
            _chest_id = self._chest_id,
            _player = self._player
        }
    end,

    on_step = function(self, dtime, moveresult)

        self._timer = self._timer + dtime

        if self._timer > .1 then

            if self._arena == "" then
                self.object:remove()
                return
            end

            if not arena_lib.get_arena_by_name('fantasia_royale', self._arena ) then
                self.object:remove()
                return
            end

            local arena_id, arena = arena_lib.get_arena_by_name('fantasia_royale', self._arena )

            if arena.enabled == true then
                self.object:remove()
                return
            end

            local p_name = self._player
            if not fantasia.editor_chests_shown[p_name] then
                self.object:remove()
                return
            end

            local chest_id = tonumber(self._chest_id)

            local objpos = vector.floor(self.object:get_pos())
            local i, chest_def = fantasia.get_chest_by_pos(arena, objpos)

            if not i then
                self.object:remove()
                return
            end

            if chest_def.id ~= chest_id then
                self.object:remove()
                return
            end


        end

    end,

    _arena = "",
    _chest_id = "",
    _timer = 0.0,
    _player = "",

})
