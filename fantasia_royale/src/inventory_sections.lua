
-- this file manages the inventory and allows or disallows moving items to different sections
local function room_for_item(inv, item, allowed_slots) end
local function move_item(inv, from_idx, to_idx) end
local function throw_item(player, inv, from_idx) end

local is_weapon = function(stack) return weapons_lib.is_weapon(stack:get_name()) end
local is_movement = function(stack) return stack:get_name() == "fantasia_royale:wind_surfer" end
local is_pick = function(stack) return minetest.get_item_group(stack:get_name(), "froyale_pickaxe") ~= 0 end
local is_attachment = function(stack) return minetest.get_item_group(stack:get_name(), "weapon_attachment") ~= 0 end
local is_other_item = function(stack) return not(is_weapon(stack) or is_movement(stack) or is_pick(stack) or is_attachment(stack)) end



local function combine(t1, t2)
    for k, v in pairs(t2) do
        t1[k] = v
    end
    return t1
end

local other_inv_slots = {} for i = 9, 32 do other_inv_slots[i] = true end
local weapons_allowed_slots = {[2] = true, [3] = true}
local movement_allowed_slots = {[1] = true}
local items_allowed_slots = combine({[4] = true, [5] = true, [6] = true, [7] = true}, other_inv_slots)
local pick_allowed_slots = {[8] = true}



-- Data Structure
-- =====================
-- allows: function(ItemStack) - return true if item allowed in these slots

-- allowed_slots - table, idx=true if allowed to be placed there in that index..
-- In other words, include any inventory idx that is allowed to place it there

-- item_handler: function(player, action, inv, inv_info) -- called if allows is true.
-- Return true to mark as handled. For custom logic such as equipping better tools automatically.

local inventory_sections = {
    weapons = {
        allows = is_weapon,
        allowed_slots = weapons_allowed_slots,
    },
    movement = {
        allows = is_movement,
        allowed_slots = movement_allowed_slots,
        locked = true
    },
    items = {
        allows = is_other_item,
        allowed_slots = items_allowed_slots
    },
    pick = {
        allows = is_pick,
        allowed_slots = pick_allowed_slots,
        locked = true,
        -- auto equips a better pick if available
        item_handler = function (player, action, inv, inv_info)
            local pckx_slot = inv:get_list("main")[8]

            if not pckx_slot:is_empty() then
                local stack = inv_info.stack or inv:get_stack(inv_info.to_list, inv_info.to_index)
                local pckx_group_no = minetest.get_item_group(pckx_slot:get_name(), "froyale_pickaxe")
                local new_pckx_group_no = minetest.get_item_group(stack:get_name(), "froyale_pickaxe")

                if pckx_group_no < new_pckx_group_no then
                    throw_item(player, inv, 8)
                    move_item(inv, inv_info.index, 8)
                    return true
                end
            end
        end,
    },
}


-- enforce the rules: if you put an item into your inventory, then after you do
-- it, we will re-arrange the inventory to enforce the rules.
-- that way, you can still use listring to shift-click items into your inventory.

minetest.register_allow_player_inventory_action(function(player, action, inv, inv_info)
    if not arena_lib.is_player_in_arena(player:get_player_name(), "fantasia_royale") then return end

    -- disallow moving locked items into other inventories (such as pickaxe or movement tool)
    if action == "take" then
        local stack = inv_info.stack

        for sctn_name, section in pairs(inventory_sections) do
            local belongs_to_section = section.allows(stack)
            if belongs_to_section then
                if section.locked then return 0 end
            end
        end
    end
end)



-- handling weapons and attachments slots
minetest.register_allow_player_inventory_action(function(player, action, inv, inv_info)
    if not arena_lib.is_player_in_arena(player:get_player_name(), "fantasia_royale") then return end

    if action == "move" then
        -- equipping attachments
        if inv_info.to_list:match("attachments") then
            local stack = inv:get_stack(inv_info.from_list, inv_info.from_index)
            if not is_attachment(stack) then return 0 end

            local weap_slot = inv_info.to_list:find("1") and 2 or 3
            local weap = inv:get_stack("main", weap_slot)

            if minetest.get_item_group(weap:get_name(), stack:get_name()) == 0 then return 0 end

            local attchmt_def = stack:get_definition()

            if attchmt_def._on_equip then
                attchmt_def._on_equip(inv, weap, weap_slot)
            end

        -- removing attachments
        elseif inv_info.from_list:match("attachments") then
            local stack = inv:get_stack(inv_info.from_list, inv_info.from_index)
            local attchmt_def = stack:get_definition()

            if attchmt_def._on_remove then
                local weap_slot = inv_info.from_list:find("1") and 2 or 3
                local weap = inv:get_stack("main", weap_slot)

                stack:get_definition()._on_remove(inv, weap, weap_slot)
            end
        end
    end
end)



function fantasia.sections_on_player_inventory_action(player, action, inv, inv_info)
    local p_name = player:get_player_name()
    local in_arena = arena_lib.is_player_in_arena(p_name, "fantasia_royale")
    if not in_arena then return end

    -- don't run if the item was not put into the main inventory
    if inv_info.listname and inv_info.listname ~= "main" then return end

    -- don't run if an item is being put from the player main inventory in a chest
    -- or if it's moving in a list that's different from the main one
    if
        inv_info.from_list
        and (
            (inv_info.from_list == "main" and inv_info.to_list ~= "main")
            or (inv_info.from_list ~= "main" and inv_info.to_list ~= "main")
        )
    then
        return
    end

    fantasia.update_inventory(player)

    -- rearrange the inventory
    if action == "put" or action == "move" then -- we will override inv:on_add so as to handle digging and collecting items
        local to_idx = inv_info.to_index or inv_info.index
        local stack = inv_info.stack or inv:get_stack(inv_info.to_list, inv_info.to_index)

        for sctn_name, section in pairs(inventory_sections) do
            local belongs_to_section = section.allows(stack)

            if belongs_to_section then
                local handled = false

                if section.item_handler then
                    handled = section.item_handler(player, action, inv, inv_info)
                end

                if not handled then
                    local is_allowed = section.allowed_slots[to_idx]

                    if not is_allowed then
                        -- move it to valid slot
                        local first_valid_idx = room_for_item(inv, stack, section.allowed_slots)
                        if first_valid_idx then
                            move_item(inv, to_idx, first_valid_idx)
                        else
                            throw_item(player, inv, to_idx)
                        end
                    end
                end
            end
        end
    end
end



minetest.register_on_player_inventory_action(fantasia.sections_on_player_inventory_action)




-- returns the first allowed index for that item, or nil
function room_for_item(inv, item, allowed_slots)
    for idx, bool in pairs(allowed_slots) do
        local slot = inv:get_stack("main", idx)
        if bool and slot:item_fits(item) then
            return idx
        end
    end
end



function move_item(inv, from_idx, to_idx)
    local list = inv:get_list("main")

    list[to_idx]:add_item(list[from_idx])
    list[from_idx] = ItemStack("")

    inv:set_list("main",list)
end



function throw_item(player, inv, from_idx)
    local list = inv:get_list("main")
    local stack = list[from_idx]
    minetest.item_drop(stack, player, vector.add(player:get_pos(),vector.new(0,.5,0)))
    list[from_idx] = ItemStack("") -- clear the item
    inv:set_list("main",list) -- set the new list
end
