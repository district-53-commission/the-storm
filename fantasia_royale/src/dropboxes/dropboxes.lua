local function count_players(arena) end


-- Also see: /storm/storm_control.lua

--[[
relavant arena properties:

arena.dropboxes = { {pos = vector.new(x,0,z), color = "#xxxxxx", treasure =
    "default:dirt"}, }

these entries are chosen when the ring shrinks. Then the particle animation
starts, and each dropbox is spawned using minetest.add_node. Finally, the
arena.dropboxes table is cleared.

arena.num_dropbox_rounds = 3 -- the number of dropbox rounds that will occur in a game


arena.dropbox_rounds_left = 3 -- the number of times left to spawn dropboxes. It
decrements every time and afterward, no more dropboxes will spawn
]]


-- there are three categories of dropboxes: green, gold, and purple in increasing rarity.
-- A raffle is used to pick one of the three. Then another raffle is used to pick the treasure.

-- these are the items that will be spawned in the dropboxes.
-- use the format "treasure_item_name" = commonality, where commonality is the number of times the item is entered into the raffle.

-- note that the commonality of green treasures is 3, gold is 2, purple is 1.

-- The category of treasure is chosen first, then the treasure itself is chosen.
fantasia.dropbox_treasures = {
    green = { 
        ["fantasia_royale:pickaxe_2"] = 3,
        ["fantasia_royale:pickaxe_3"] = 2,
        ["fantasia_royale:nailedclub"] = 1,
    },

    gold = {
        ["fantasia_royale:mana_vial_large"] = 2,
        ["fantasia_royale:health_potion_large"] = 1,
        ["fantasia_royale:crossbow"] = 1,
    },

    purple = {
        ["fantasia_royale:firestaff"] = 1,
    }
}

-- let's make the raffle table for the dropbox categories
local dropbox_category_raffle = {"purple", "gold", "gold", "green", "green", "green"}
-- and the raffle table for the dropbox treasures for each category
local dropbox_raffle = {}

for _, category in ipairs({"purple", "gold", "green"}) do
    dropbox_raffle[category] = {}
    for item, commonality in pairs(fantasia.dropbox_treasures[category]) do
        for i = 1, commonality do
            table.insert(dropbox_raffle[category], item)
        end
    end
end

local particle_colors = {
    ["green"] = "#71aa34",
    ["gold"] = "#f47e1b",
    ["purple"] = "#564064"
}


-- dropbox positions, colors, and treasures are chosen all at once and then
-- saved in arena.dropboxes. Later, they are placed and added to
-- arena.permanent_dropboxes which records diggable dropboxes so that players
-- can actually dig them. The reason is that in the future, we may make the
-- placement of dropboxes take some time and use an animation of some sort.
-- MisterE tried implementing a time-delayed animation and the first
-- implementation was too complex... a second attempt can be made after better
-- lore and better graphical abilities are available.

fantasia.choose_dropbox_spawns = function(arena)
    local num_players = count_players(arena)


    local num_dropboxes = math.floor(num_players * ( math.random(0,4)/2 + .75))

    if num_dropboxes == 0 then
        num_dropboxes = 1
    end

    local dropboxes = {}

    local center = arena.storm_center
    local storm_radius = arena.storm_radius

    for i = 1, num_dropboxes do
        local angle, radius, offset, pos, test_arena, height
        local tries = 0
        local num_tries = 5

        repeat
            -- Try to find a valid position within the arena
            angle = math.random() * 2 * math.pi
            radius = math.sqrt(math.random()) * storm_radius
            offset = vector.new(radius * math.cos(angle), 0, radius * math.sin(angle))
            pos = vector.apply(vector.add(center, offset), math.floor)
            pos.y = arena.ground_level
            test_arena = arena_lib.get_arena_by_pos(pos, "fantasia_royale")

            -- Check if the position is within the arena and has a valid height
            if test_arena ~= nil then
                height = fantasia.get_height(arena, pos.x, pos.z)
                if height == -35000 then
                    height = nil
                else
                    pos.y = height + 1
                end
            end

            tries = tries + 1
        until (test_arena ~= nil and height ~= nil) or tries >= num_tries

        if test_arena ~= nil and height ~= nil then
            local category = dropbox_category_raffle[math.random(#dropbox_category_raffle)]
            local color = particle_colors[category]
            local treasure = dropbox_raffle[category][math.random(#dropbox_raffle[category])]
            table.insert(dropboxes, {pos = pos, color = color, treasure = treasure, category = category})
        end
    end
    arena.dropboxes = dropboxes
end



-- NEXT, we will create a particle effect to show the dropboxes being spawned.

fantasia.place_dropboxes = function(arena)
    for _, dropbox in ipairs(arena.dropboxes) do
        local nodename = "fantasia_royale:dropbox_" .. dropbox.category
        minetest.add_node(dropbox.pos, {name = nodename})

        -- add the dropbox to the permanent dropboxes table so players can get rewards when they dig it.
        arena.permanent_dropboxes[minetest.pos_to_string(dropbox.pos,0)] = dropbox

        -- add a particle effect to every player in the arena
        for p_name, stats in pairs(arena.players_and_spectators) do
            minetest.add_particlespawner({
                amount = 500,
                -- Number of particles spawned over the time period `time`.
                size = 5,
                exptime = 20,
                time = 10,
                -- Lifespan of spawner in seconds.
                -- If time is 0 spawner has infinite lifespan and spawns the `amount` on
                -- a per-second basis.            
                collisiondetection = false,
                -- If true collide with `walkable` nodes and, depending on the
                -- `object_collision` field, objects too.            
                collision_removal = false,
                -- If true particles are removed when they collide.
                -- Requires collisiondetection = true to have any effect.            
                object_collision = false,
                -- If true particles collide with objects that are defined as
                -- `physical = true,` and `collide_with_objects = true,`.
                -- Requires collisiondetection = true to have any effect.                                    
                texture = "froyale_dropbox_effect_particle.png^[colorize:" .. dropbox.color,
                -- The texture of the particle
                -- v5.6.0 and later: also supports the table format described in the
                -- following section.
                playername = p_name,
                -- Optional, if specified spawns particles only on the player's client
                glow = 3,
                -- Optional, specify particle self-luminescence in darkness.
                -- Values 0-14.
                pos = dropbox.pos,
                radius_tween = {
                    style = "pulse",
                    reps = 10,
                    {min = 1, max = 2,},
                    {min = 2, max = 5,},
                },
                vel = {x = 0, y = 10, z = 0},
            })
        end
    end
            
end





function count_players(arena)
    local cnt = 0
    for pl_name, stats in pairs(arena.players) do
        cnt = cnt + 1
    end
    return cnt
end