## Coding guidelines

* snake_case variables and functions
* uppercase SNAKE_CASE for globals
* 1 tab indentation
* variables starting with `p_` are player variables (e.g. `p_name`). When inside a for, use `pl_`