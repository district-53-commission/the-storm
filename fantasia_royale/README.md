## Fantasia Royale

A Battle Royale for Minetest (WIP)

### Planning

See milestones

## Game Loop:

1) Queue
2) Map reset, time allotted for intro/explanation HUDs
- attach players to dummy entities to prevent movement
3) loot crates:
- fill loot crates
4) player spawn
- move players' dummy entities to spawn locations. Give them a glider.
- make them press space to detach from the dummy entity, which lets them enter freefall.
- with the glider, pressing jump equips the glider to stop freefall
- with the glider, pressing jump un-equips the glider to start freefall

- the glider automatically deploys a small distance from the ground (use raycast?) and cannot be disabled

- when a player hits the ground, remove the glider.

5) spawn timeout
- after a timeout (displayed on a hotbar HUD), detach any remaining players from their dummy entities

6) storm circle
- the game has phases during which the storm circle shrinks/moves and during which it is constant. Implement such that the next movement is determined in the previous phase, so a forecast mechanic can be implemented later.
- players outside the storm circle are damaged by an amount that increases with time.

7) eliminations
- show a kill hud, where who killed who with what is shown as in block league. Add a sound?
- throw players' items into the world

8) winning condition:
last player standing, there is only one winner, use default arena_lib functionality


## Instructions

### 1. Install the dependencies
listed in [mod.conf](mod.conf)

### 2. Create an arena
with `/arenas create fantasia_royale <arena name>`, edit it with `/arenas edit fantasia_royale <arena name>`

### 3. Set an arena region
![Arena region](doc_textures/arenalib_editor_map.png) -> ![Arena region](doc_textures/arenalib_tool_map_region.png) or it won't work

### 4. Use the map section of the custom editor section
![Arena editor section](textures/froyale_icon.png) -> ![Map section](textures/froyale_editor_map.png) to set the ground level ![Ground level](textures/froyale_set_ground_level.png) and the kill level ![Kill level](textures/froyale_set_kill_level.png). If desired, set the storm starting radius ![Storm radius](textures/froyale_set_starting_storm_radius.png) or leave/set it at 0 to let it be calculated automatically to the largest possible size.

### 5. Set the arena treasures
(the items that can spawn in the chests). ![Arena editor section](textures/froyale_icon.png) -> ![Chests section](textures/froyale_chests_editor.png) -> ![Treasures editor](textures/froyale_treasures_editor_section.png). Before using the tool, put the items you wish to add into the lower section of your inventory. After using the tool, you will see the lower section of your inventory. Drag the item into the slot and set the count, preciousness and rarity, and then click `[Add]`.

- count: the amount of the items that will be placed together in a slot
- preciousness: in which chests it can spawn, it's like the treasure tier (e.g. a chest with a preciousness range 2-4 can just spawn items with a preciousness between 2 and 4)
- rarity: 1-10, a variable that determines how often it will spawn in chests

The editor also allows you to scroll through all the treasures that are set up for the arena, and click the X button to remove them.

You can also copy treasures from another arena using the command:
```/froyale copytreasures <(from) arena name> <(to) arena name>```

### 6. Place the chests in the arena.
![Arena editor section](textures/froyale_icon.png) -> ![Chests section](textures/froyale_chests_editor.png)

Your tools are:

- Show Chest Locations ![Show Chest Locations](textures/froyale_show_chest_locations.png). This shows the location of all the chests in the arena on the HUD and spawns marker entities at their locations. You must click on a marker entity to edit or remove chests. If you move into an unloaded area, the marker entities might not have spawned: simply hide and show them again to spawn the entities.

When used, the tool changes into hide chest locations ![Hide Chest Locations](textures/froyale_hide_chest_locations.png).

- Add chest ![Add Chest](textures/froyale_add_chest.png)

Click on a place to add a chest, and then set the following options in the popup that appears:

`min_treasures`: the minimum amount of treasures that will spawn in the chest

`max_treasures`: the maximum amount of treasures that will spawn in the chest

`min_preciousness`: treasures with at least this preciousness will spawn in the chest

`max_preciousness`: treasures with at most this preciousness will spawn in the chest

`color`: choose from 3 color tiers: green, blue, yellow. For now, this just helps you keep track of the value of chests that you have placed around the arena while in the editor because it affects the chest HUD color. In the future, colored particles will spawn around the chests during gameplay.


Whenever you add or edit a chest, the parameters of the chest are saved for the defaults of the next chest you add. To copy a chest, edit it and then just use the add tool without changing anything.

- Edit chest ![Edit Chest](textures/froyale_edit_chest.png). Click on a chest marker entity. It will pop up with the same form to change the options.

- Remove chest ![Remove Chest](textures/froyale_remove_chest.png). Click on a chest marker entity to delete it.


### DO NOT REMOVE

Troubleshooting;
If the storm is disappearing and the game is crashing, then your max_forceloaded_blocks setting is too low.

set it to:
```
max_forceloaded_blocks = -1
```
in minetest.conf to disable the limit or increase the limit.

