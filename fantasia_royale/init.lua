local modname = "fantasia_royale"
local modpath = minetest.get_modpath(modname)
local srcpath = modpath .. "/src"


fantasia = {}
fantasia.settings = {}
fantasia.modname = modname
fantasia.srcpath = srcpath

fantasia.chest_colors = {
    green = {hex = "#71aa34", number = 0x71aa34},
    blue = {hex = "#28ccdf", number = 0x28ccdf},
    yellow = {hex = "#f4b41b", number = 0xf4b41b},
}

dofile(modpath .. "/GLOBALS.lua")
dofile(srcpath .. "/_load.lua")

--====================================================
--====================================================
--               Minigame registration
--====================================================
--====================================================

local min_players, load_time

if fantasia.settings.DEBUG_MODE then
    min_players = 1
    load_time = 1
else
    min_players = 3
    load_time = 10
end

arena_lib.register_minigame(modname, {
    name = "Fantasia Royale",
    icon = "froyale_icon.png",
    min_players = min_players,
    can_build = true,
    regenerate_map = true,
    spectate_mode = "blind",

    time_mode = "incremental",
    load_time = load_time,
    celebration_time = 10,

    damage_modifiers = {fall_damage_add_percent = -50},

    in_game_physics = {
        speed = 1.5
    },

    hotbar = {
        slots = 8, -- One for movement, two for weapons, 4 for items, 1 for pick
        background_image = "froyale_hud_hotbar8.png"
    },

    -- MT doesn't allow to move the breath bar (https://github.com/minetest/minetest/issues/14125),
    -- so for now we just disable the breathing system entirely (so to avoid the
    -- mana bar overlapping with the mana one). See https://gitlab.com/district-53-commission/fantasia-royale/-/issues/59
    disabled_damage_types = {"drown"},
    hud_flags = {
        breathbar = false,
        wielditem = false,
        minimap = false,
        basic_debug = false,

    },

    -- players start by being invisible so that:
    -- 1. there's no need to se it manually when attaching them to the initial vehicle
    -- 2. when they leave, their visual size is always restored (e.g. if done whilst attached to the vehicle)
    player_aspect = {
        visual_size = {x=0, y=0},
    },
    camera_offset = {vector.new(0, 30, -40),vector.new(0, 30, -40)},

    -- persisted props
    properties = {
        chests = {},
        treasures = {}, -- items to put in chests
        ground_level = -34000, -- an impossible value to detect if it was not changed.
        starting_storm_radius = 0,
        storm_center_start = vector.new(0,0,0), -- the starting location of the storm center
        first_round_time = 180,
        kill_level = -50,
        default_view_size = 512,
        drop_off_time = fantasia.DROP_OFF_TIME,
        num_dropbox_rounds = 6, -- the number of times dropboxes will be spawned when the ring shrinks
    },

    -- unpersisted props
    temp_properties = {
        time_since_start = 0, -- sec, updated in on_time_tick
        -- the current center and radius
        storm_radius = 0,
        storm_offset = vector.new(0,0,0),

        -- the next target offest and radius
        next_radius = 0,
        next_offset = vector.new(0,0,0),

        -- the next next target offset and radius (implemented this way so that
        -- it is predetermined and can be used for a powerup/strategy)
        next_radius2 = nil,
        next_offset2 = nil,

        rounds_complete = 0, -- how many storm cycles have been completed
        round_timer = 0, -- a counter for timing until the next round
        stop_time = 0, -- the time of round_timer to stop movement
        storm_obj_ref = nil, -- MisterE: I know I'm not supposed to do this. I'm doing it anyways.
        storm_loaded = false, -- sets to true when the storm entity has been spawned. DO NOT commence the game until true!

        round_start_timestamp = 0, -- us time of the start of the last round
        round_start_offset = vector.new(0,0,0), -- the offset of the last round
        round_start_size = 0, -- the size of the last round

        -- leaderboard, gets updated from storage when the game starts
        leaderboard = {},

        -- helper positions, updated on the globalstep
        storm_center = vector.new(0,0,0),
        next_center = vector.new(0,0,0),
        next_center2 = vector.new(0,0,0),

        initial_vehicle_exists = false,
        initial_vehicle_obj_ref = nil, -- MisterE: I know I'm not supposed to do this. I'm doing it anyways.
        initial_vehivle_dir = vector.new(0,0,0),
        initial_vehicle_pos = vector.new(0,0,0),
        initial_vehicle_vel = vector.new(0,0,0),
        initial_vehicle_start_time = 0,

        dropbox_rounds_left = 0, -- the number of times left to spawn dropboxes. Set to num_dropbox_rounds when the arena is loaded
        dropboxes = {}, -- temporary dropboxes that are in the process of being spawned. See dropboxes/dropboxes.lua
        permanent_dropboxes = {}, -- dropboxes that have been spawned in the arena, and are waiting for a player to pick them up. the key is the minetest.pos_to_string(pos,0) of the dropbox position, and the value is a table with keys pos, color, treasure, and category. See dropboxes/dropboxes.lua
    },

    player_properties = {
        -- whether the player is still inside the initial vehicle
        has_dropped_off = false,
        is_gliding = false,
        can_control_glider = true,
        is_glider_open = false,
        glider_color = nil,

        -- see mana_system.lua
        mana = 10,
        mana_reserve = 0,
        minimap_view_size = nil,
        minimap_centered = true,
        minimap_center = vector.new(0,0,0),
        minimap_pan = false,
        minimap_zoom_in = false,
        minimap_zoom_out = false,
        minimap_poi_green = false,
        minimap_poi_blue = false,
        minimap_poi_yellow = false,
        minimap_poi_red = false,

        pois = {},

        can_view_storm_target_1 = true, -- let everyone see the current target
        can_view_storm_target_2 = false, -- use this as a powerup to know the next target in advance, unlocked with an item

        has_recieved_starting_items = false,
        -- visual_size = {x=0, y=0}, -- this is to save the visual size of the player when they have been assigned a class but still need to be invisible while attached to the starting vehicle
      },
})



weapons_lib.register_mod("fantasia_royale", {
    RELOAD_KEY = "",
})



--====================================================
--====================================================
--            Calling the other files
--====================================================
--====================================================

-- keep the root > sub / alphabetical order please, unless required otherwise
dofile(srcpath .. "/storage.lua")
dofile(srcpath .. "/heightmap/heightmap.lua")
dofile(srcpath .. "/minigame_manager/globalstep.lua")
dofile(srcpath .. "/minigame_manager/editor.lua")
dofile(srcpath .. "/minigame_manager/on_enable.lua")
dofile(srcpath .. "/minigame_manager/on_celebration.lua")
dofile(srcpath .. "/minigame_manager/on_death.lua")
dofile(srcpath .. "/minigame_manager/on_eliminate.lua")
dofile(srcpath .. "/minigame_manager/on_end.lua")
dofile(srcpath .. "/minigame_manager/on_load.lua")
dofile(srcpath .. "/minigame_manager/on_prejoin_queue.lua")
dofile(srcpath .. "/minigame_manager/on_quit.lua")
dofile(srcpath .. "/minigame_manager/on_start.lua")
dofile(srcpath .. "/minigame_manager/on_time_tick.lua")
dofile(srcpath .. "/minigame_manager/on_change_spectated_target.lua")
dofile(srcpath .. "/commands.lua")
dofile(srcpath .. "/invadd_override.lua")
dofile(srcpath .. "/inventory_sections.lua")
dofile(srcpath .. "/items.lua")
dofile(srcpath .. "/nodes.lua")
dofile(srcpath .. "/player_manager.lua")
dofile(srcpath .. "/utils.lua")
dofile(srcpath .. "/classes/elara/elara.lua")
dofile(srcpath .. "/classes/oakblade/oakblade.lua")
dofile(srcpath .. "/classes/sol_arca/sol_arca.lua")
dofile(srcpath .. "/editor_section/editor_lib.lua")
dofile(srcpath .. "/editor_section/editor_gui.lua")
dofile(srcpath .. "/editor_section/editor_hud.lua")
dofile(srcpath .. "/editor_section/editor_entities.lua")
dofile(srcpath .. "/editor_section/editor.lua")
dofile(srcpath .. "/entities/player_anchor.lua")
dofile(srcpath .. "/entities/dummy.lua")
dofile(srcpath .. "/entities/glider.lua")
dofile(srcpath .. "/entities/initial_vehicle.lua")
dofile(srcpath .. "/entities/ring.lua")
dofile(srcpath .. "/gui/gui_classes_selector.lua")
dofile(srcpath .. "/gui/radial_menu.lua")
dofile(srcpath .. "/gui/leaderboard.lua")
dofile(srcpath .. "/mana/mana_system.lua")
dofile(srcpath .. "/map/poi/poi_colors.lua")
dofile(srcpath .. "/map/poi/poi.lua")
dofile(srcpath .. "/map/minimap/minimap.lua")
dofile(srcpath .. "/map/chest_treasures/chest_setter.lua")
dofile(srcpath .. "/map/chest_treasures/treasures.lua")
dofile(srcpath .. "/overrides/zoom_overrides.lua")
dofile(srcpath .. "/storm/storm_control.lua")
dofile(srcpath .. "/storm/storm_player_control.lua")
dofile(srcpath .. "/storm/storm_sounds.lua")
dofile(srcpath .. "/hud/hud_log.lua")
dofile(srcpath .. "/hud/hud_mana.lua")
dofile(srcpath .. "/hud/hud_skill.lua")
dofile(srcpath .. "/hud/hud_players_counter.lua")
dofile(srcpath .. "/hud/hud_storm_overlay.lua")
dofile(srcpath .. "/hud/hud_minimap.lua")
dofile(srcpath .. "/hud/hud_ring_shrink_timer.lua")
dofile(srcpath .. "/hud/hud_poi.lua")
dofile(srcpath .. "/hud/utils.lua")
dofile(srcpath .. "/items/wind_surfer.lua")
dofile(srcpath .. "/items/mana_vials/vials_system.lua")
dofile(srcpath .. "/items/mana_vials/vials.lua")
dofile(srcpath .. "/items/picks/picks.lua")
dofile(srcpath .. "/items/potions/health_potions.lua")
dofile(srcpath .. "/dropboxes/dropboxes.lua")
dofile(srcpath .. "/vehicles/glider.lua")
dofile(srcpath .. "/vehicles/initial_vehicle.lua")
dofile(srcpath .. "/player/input_manager.lua")
dofile(srcpath .. "/player/inventory.lua")
dofile(srcpath .. "/player/player_data.lua")
dofile(srcpath .. "/player/status/regeneration.lua")
dofile(srcpath .. "/weapons/crossbow.lua")
dofile(srcpath .. "/weapons/falchion.lua")
dofile(srcpath .. "/weapons/fire_staff.lua")
dofile(srcpath .. "/weapons/nailed_club.lua")
dofile(srcpath .. "/weapons/weapon_utils.lua")
dofile(srcpath .. "/weapons/attachments/attchmt_melee.lua")
dofile(srcpath .. "/weapons/attachments/attchmt_ranged.lua")

dofile(srcpath .. "/unit_tests/test_hud.lua")
