# Fantasia Royale DOCS

(WIP)

## Classes
Classes are `classy` objects, adding a few custom fields:
* `_splash_art`: (string) a high-res pixel art representing the character
* `_thumbnail`: (string) a thumbnail representing the face of the splash art

## Weapons

Fire weapons consume mana, melee weapons don't.  

Weapons can have attachments. In order for a weapon to be able to equip a certain attachment, its technical name must be added into the weapon's `groups`, e.g.

```lua
weapons_lib.register_weapon("fantasia_royale:crossbow", {
  groups = {["fantasia_royale:attchmt_zoom"] = 1}`		-- this weapon can equip the zoom attachment
  --stuff
})
```

## Initial Vehicle

Players start on a steampunk blimp and may skydive off of it at any time after the arena starts.

`arena.players[p_name].has_dropped_off` indicates whether the player has dropped off the initial vehicle yet


## Hang Glider

The hang glider is used to drop off from the initial vehicle. It can be opened and closed through the jump key and, if closed, it opens automatically a few nodes before entering in contact with a solid node.

`arena.players[p_name].is_gliding` indicates whether the player may glide.
`arena.players[p_name].can_control_glider` indicates whether the player may open or close the glider .. they may not after they get too close to the ground.
`arena.players[p_name].is_glider_open` indicates if the player currently has the glider open.
`arena.players[p_name].glider_color` the color of the glider is nil until the first time the player opens the glider. Then it remains the same for that match.

These should be read-only. To let a player start gliding, 

`fantasia.start_gliding(p_name, arena)` starts the gliding process. It is called when players first drop off the initial vehicle, but may be called by other game features later. Note that it *DOES NOT* automatically open the glider. It does display a message that the glider may be opened. Once this is called, the player will automatically equip the glider when they return to the ground.

## Heightmap

When the arena is created, a heightmap should be saved using the tool in the editor section. After the heightmap is saved, and when an arena is in game, you can get the height at an x and z coordinate inside the arena using:

`fantasia.get_height(arena_name_or_arena, x, z)`

It should not be called for x,z outside the arena area (It will crash). It should not be called before the heightmap has been saved in the arena editor. If there is no terrain at the x,z (sky in a sky islands map), it will return `-35000`


## Debug mode
By activating this option located in the `SETTINGS.lua` file inside the world folder, players will be allowed to:
* set 1 player arenas
* set 1 second loading time

This option also enables unit tests, that can be run via `/froyale unittests`
