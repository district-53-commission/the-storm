-- For params we don't really want to edit, but that are handy to keep track within
-- a single file.

-- the largest arena the minigame supports. We have to modify the
-- "active_object_send_range_blocks" setting to allow clients to see the boundary
-- for this distance, so set it higher than the actual maximum arena size
fantasia.MAX_STORM_STARTING_RADIUS = 2000

-- the maximum time players can remain inside the initial vehicle before they're
-- automatically dropped off
fantasia.DROP_OFF_TIME = 60