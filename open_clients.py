
"""
Minetest Server and Clients Launcher Script

This script is used to start a Minetest server and multiple clients, each logging in with specified credentials.
It first terminates any existing Minetest processes and then starts the server and clients.
Use case: (re)start Minetest server and clients.

Assumptions:
- The Minetest executable is located in the '../../bin/' directory relative to the script's location.
- The world directory path, player names, and passwords are correctly specified in the script variables.
- `psutil` library is installed for managing processes.

Dependencies:
- psutil: Install using `pip install psutil`

Variables:
- `executable_path`: Path to the Minetest executable.
- `world_path`: Path to the world directory (replace '/path/to/world' with the actual path).
- `worldname`: Name of the world to load.
- `gameid`: ID of the game to use.
- `server_address`: Address where the server will run (default is '0.0.0.0').
- `server_port`: Port on which the server will run (default is '30000').
- `players`: List of dictionaries, each containing a player's name and password.

Functions:
- `detect_os()`: Detects the operating system.
- `kill_existing_minetest_processes()`: Terminates any running Minetest processes.
- `start_minetest_clients(executable_path, world_path, worldname, gameid, server_address, server_port, players)`: Starts the server and clients with the specified parameters.

Usage:
1. Ensure `psutil` is installed: `pip install psutil`.
2. Place this script in your Minetest mod directory.
3. Replace `/path/to/world` with the actual path to your world directory.
4. Update the `players` list with the actual player names and passwords.
5. Run the script to start the Minetest server and all specified clients, ensuring old instances are terminated first.

Example:
    python start_minetest.py
"""

import os
import subprocess
import platform
import signal
import psutil

# Variables
executable_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../bin'))
world_path = '/path/to/world'  # Replace with your actual world path
worldname = 'storm_map' # or use this
gameid = 'minetest'

local = True

server_port = '30000'
server_address = '95.217.191.120'

if local == True:
    server_address = '0.0.0.0'



players = [
    {'name': 'player1', 'password': 'password1'},
    {'name': 'player2', 'password': 'password2'},
    {'name': 'player3', 'password': 'password3'},
    # Add more players as needed
    # {'name': 'player4', 'password': 'password4'},
    # {'name': 'player5', 'password': 'password5'}
]

def detect_os():
    return platform.system()

def kill_existing_minetest_processes():
    for proc in psutil.process_iter(['pid', 'name']):
        if 'minetest' in proc.info['name']:
            os.kill(proc.info['pid'], signal.SIGTERM)

def start_minetest_clients(executable_path, world_path, worldname, gameid, server_address, server_port, players):
    os_type = detect_os()
    
    if os_type == 'Windows':
        minetest_exe = os.path.join(executable_path, 'minetest.exe')
    else:
        minetest_exe = os.path.join(executable_path, 'minetest')

    # Start server
    if local == True:
        server_command = [minetest_exe, '--server', '--worldname', worldname, '--world', world_path, '--gameid', gameid]
        subprocess.Popen(server_command)
        
    # Start clients
    for player in players:
        client_command = [
            minetest_exe, '--worldname', worldname, '--world', world_path, '--gameid', gameid, 
            '--address', server_address, '--port', server_port, '--name', player['name'], '--password', player['password'], '--go'
        ]
        subprocess.Popen(client_command)

if __name__ == "__main__":
    kill_existing_minetest_processes()
    start_minetest_clients(executable_path, world_path, worldname, gameid, server_address, server_port, players)
